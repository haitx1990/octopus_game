﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using TMPro;


public class SceneManager : MonoBehaviour
{
    public static SceneManager ins;
    public AsyncOperation async;
    public static FormUI formLast;//Lưu Form trước đó

    [Header("--------------- Trạng thái của Scene ---------------")]
    private bool isChangeForm = false;
    private bool isSkipCheck_RemoteConfig = false;
    private bool isOpenForm_NoShowAOA = false;

    [Header("--------------- Pause ---------------")]
    public bool isPause = false;
    [SerializeField] private bool isPauseF5 = false;
    [SerializeField] private float timeBlock = 0;//Block Input theo thời gian
    [SerializeField] private bool isBlockInput;//Block Input theo sự kiện
    public GameObject obj_BlockInput;

    [Header("--------------- Popup đã đc tạo trong Scene ---------------")]
    [HideInInspector] public Popup_Rate popup_Rate;
    [HideInInspector] public Popup_EndGame popup_EndGame;
    [HideInInspector] public Popup_Revive popup_Revive;
    [HideInInspector] public Popup_Settings popup_Settings;
    [HideInInspector] public Popup_Skin popup_Skin;
    [HideInInspector] public Popup_Offer popup_Offer;

    [Header("--------------- UI trong Scene ---------------")]
    public Form_Loading form_Loading;
    public Form_Home form_Home;
    public Form_GamePlay form_GamePlay;
    //public Canvas indicatorCanvas;
    //public Canvas playerCanvas;
    public Canvas popupCanvas;
    public FormBase formCurrent;

    //[Header("-------- Other --------")]
    //public camFollower camFollower;
    //public CharacterBase[] listAllChar;
    //public Indicator[] listIndicator;

#region Main
    private void Awake() {
        ins = this;
    }

    protected void Start()
    {
        if (GameManager.ins == null) return;
        isChangeForm = false;
        BlockInput(true);
        
        //Trong Session hiện tại đã từng vào màn Home rồi thì ko cần đợi RemoteConfig nữa -> Vào Form luôn
        if (GameManager.ins.isFirstOpen_FormHome) {
            isSkipCheck_RemoteConfig = true;
            BlockInput(false);
            formCurrent.Show();
        } else{//Trong Session hiện tại chưa từng vào màn Home -> Chỉ đợi 5s. Nếu RemoteConfig ko trả về kịp thì vẫn vào Home
            StartCoroutine(WaitRemoteConfig());
            Timer.Schedule(this, 5, () => {
                isSkipCheck_RemoteConfig = true;
            });
        }
    }

    IEnumerator WaitRemoteConfig()
    {
        //Đợi giá trị trả về từ RemoteConfig
        yield return new WaitUntil(() => DataManager.ins.gameSave != null && DataManager.ins.isLoaded && (FirebaseManager.ins.is_remote_config_success || isSkipCheck_RemoteConfig));
        //Nếu trong lần mở game thứ 2, Xong Tut, đang bật AppOpenAd -> Đợi AOA load xong mới bật
        if(DataManager.ins.gameSave.isTutCompleted && DataManager.ins.gameSave.sessionCount > 0  && DataManager.ins.enable_AppOpenAds) {
            //Nếu load AppOpenAd quá lâu thì sẽ hủy 
            Timer.Schedule(this, DataManager.ins.time_Loading, () => {
                isOpenForm_NoShowAOA = true;
            });
            yield return new WaitUntil(() => (AppOpenAdManager.Instance.IsAdAvailable || isOpenForm_NoShowAOA));
            if(AppOpenAdManager.Instance.IsAdAvailable) AppOpenAdManager.Instance.ShowAdIfAvailable();
        }
        //Mở Form lên
        BlockInput(false);
        formCurrent.Show();
    }

    void Update()
    {
        if (async != null) return;
        //Pause game
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F5)) isPauseF5 = !isPauseF5;
#endif
        if (GameManager.ins != null) Time.timeScale = (isPause || isPauseF5) ? 0 : 1;
        //User bấm nút Back trên điện thoại
        if (Input.GetKeyDown(KeyCode.Escape) && !obj_BlockInput.activeSelf)
        {
            //Đóng các Popup lại
            PopupBase[] listPopup =  popupCanvas.GetComponentsInChildren<PopupBase>();
            if (listPopup.Length > 0)
            {
                //Nếu popup cuối cùng (Ở trên cùng) có thể tắt đc -> Tắt popup
                if (listPopup[listPopup.Length - 1].isCloseByEscape)
                {
                    listPopup[listPopup.Length - 1].Close();
                }
            }
        }
    }
    #endregion


#region Block Input
    public void BlockInput(bool isBlock)
    {//BlockInput theo sự kiện
        isBlockInput = isBlock;
        if ((timeBlock <= 0 && !isBlock) || isBlock) obj_BlockInput.SetActive(isBlock);
    }

    public void BlockInput(float second)
    {//BlockInput theo thời gian
        obj_BlockInput.SetActive(true);
        if (second > timeBlock) timeBlock = second;
        StartCoroutine(CountTimeBlockInput());
    }

    private IEnumerator CountTimeBlockInput()
    {
        yield return new WaitForSeconds(0.5f);
        timeBlock -= 0.5f;
        if (timeBlock <= 0)
        {
            timeBlock = 0;
            if (!isBlockInput) obj_BlockInput.SetActive(false);//Nếu ko block theo even thì mới mở Block
        }
        else
        {
            StartCoroutine(CountTimeBlockInput());//Tiếp tục đếm thời gian Block Input
        }
    }
#endregion

#region Form
    //Chuyển sang 1 Form mới
    public void ChangeForm(FormUI formNext, float time = 0)
    {
        if (isChangeForm || async != null) return;//Nếu đang ChangeForm thì ko change Form nữa
        isChangeForm = true;
        formLast = formCurrent.idForm;

        BlockInput(time + 0.5f);
        //Chuyển Scene
        Timer.Schedule(this, time, () => { async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(formNext.ToString()); });
    }

    //Thụt ra thụt vào các Popup ở màn Home
    public void HideForm(bool isHide = true, bool includeGold = true)
    {
        if (isHide){//Ẩn Popup
            for (int i = 0; i < formCurrent.twn_Pos.Length; i++) formCurrent.twn_Pos[i].DOPlayBackwards();
        }else {//Hiện Popup
            for (int i = 0; i < formCurrent.twn_Pos.Length; i++) formCurrent.twn_Pos[i].DOPlayForward();
        }
    }
    #endregion

    #region Popup

    public void ShowPopup_Rate()
    {
        if (popup_Rate == null)
        {
            popup_Rate = Instantiate(GameManager.ins.popup_Rate, popupCanvas.transform);
        }
        popup_Rate.transform.localScale = Vector3.one;
        popup_Rate.transform.SetAsLastSibling();
        popup_Rate.gameObject.SetActive(false);
        popup_Rate.Show();
    }

    public void ShowPopup_EndGame(bool isWin = false)
    {
        if (popup_EndGame == null)
        {
            popup_EndGame = Instantiate(GameManager.ins.popup_EndGame, popupCanvas.transform);
        }
        popup_EndGame.transform.localScale = Vector3.one;
        popup_EndGame.transform.SetAsLastSibling();
        popup_EndGame.gameObject.SetActive(false);
        popup_EndGame.isWin = isWin;
        popup_EndGame.Show();
    }
    public void ShowPopup_Revive()
    {
        if (popup_Revive == null)
        {
            popup_Revive = Instantiate(GameManager.ins.popup_Revive, popupCanvas.transform);
        }
        popup_Revive.transform.localScale = Vector3.one;
        popup_Revive.transform.SetAsLastSibling();
        popup_Revive.gameObject.SetActive(false);
        popup_Revive.Show();
    }

    public void ShowPopup_Revive(System.Action c)
    {
        if (popup_Revive == null)
        {
            popup_Revive = Instantiate(GameManager.ins.popup_Revive, popupCanvas.transform);
        }
        popup_Revive.SetReborn(c);
        popup_Revive.transform.localScale = Vector3.one;
        popup_Revive.transform.SetAsLastSibling();
        popup_Revive.gameObject.SetActive(false);
        popup_Revive.Show();
    }

    public void ShowPopup_Settings()
    {
        if (popup_Settings == null)
        {
            popup_Settings = Instantiate(GameManager.ins.popup_Settings, popupCanvas.transform);
        }
        popup_Settings.transform.localScale = Vector3.one;
        popup_Settings.transform.SetAsLastSibling();
        popup_Settings.gameObject.SetActive(false);
        popup_Settings.Show();
    }
    public void ShowPopup_Skin()
    {
        if (popup_Skin == null)
        {
            popup_Skin = Instantiate(GameManager.ins.popup_Skin, popupCanvas.transform);
        }
        popup_Skin.transform.localScale = Vector3.one;
        popup_Skin.transform.SetAsLastSibling();
        popup_Skin.gameObject.SetActive(false);
        popup_Skin.Show();
    }

    public void ShowPopup_Offer()
    {
        if (popup_Offer == null)
        {
            popup_Offer = Instantiate(GameManager.ins.popup_Offer, popupCanvas.transform);
        }
        popup_Offer.transform.localScale = Vector3.one;
        popup_Offer.transform.SetAsLastSibling();
        popup_Offer.gameObject.SetActive(false);
        popup_Offer.Show();
    }
    #endregion
}

