﻿public class Constant 
{
    public const int frameRate = 90;
    public const int timeCountdown = 60;
    public const int moneyEndgame = 100;
    public const int costRevive = 200;
    public const int totalPlayer = 20;
    public const int moneyPlayerDie = 100;//Giá tiền 1 Player bị chết

}

public enum MiniGame
{
    None,
    RedLightGreenLight,
    Gameplay_Sut_Dat,
}

public enum FormUI
{
    None,
    Form_Loading,
    Form_Home,
    Form_RedLightGreenLight,
    MoveStopMove,
    Gameplay_Sut_Dat,
    Form_Loading2,
    Form_Tutorial,
}

public enum PopupUI
{
    None,
    Popup_Rate,
    Popup_EndGame,
    Popup_Revive,
    Popup_Settings,
    Popup_Skin,
    Popup_Offer,
}

public enum Ani_State
{
    None,
    Idle,
    Run,
    Dead,
    Win,
    Dance,
    Attack,
    Ulti,
    Walk_Up,
    Idle_InGame,
    Pose_InGame,
    Idle_Army,
    Attack_Army_Pistol,
    Attack_Army_AK,
}

public enum Char_State {
    None,
    Idle,
    Run,
    Dead,
}

public enum Doll_State {
    None,
    TurnOff,
    TurnOn,
}