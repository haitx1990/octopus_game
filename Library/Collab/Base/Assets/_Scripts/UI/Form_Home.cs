﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Form_Home : FormBase
{
    private int piggyBank_Case;

    public GameObject obj_Gold;
    public TextMeshProUGUI txt_Gold;

    public TextMeshProUGUI txt_Level;
    public TextMeshProUGUI txt_Day;
    public TextMeshProUGUI txt_PiggyBank;

    public PiggyBankControl piggyBankControl;
    public MyPlayer myPlayer;

    private void Awake()
    {
        if (GameManager.ins == null)
        {
            SceneManager.ins.ChangeForm(FormUI.Form_Loading);
            return;
        }
    }

    public override void Show()
    {
        base.Show();
        if (GameManager.ins == null || DataManager.ins == null) return;
        txt_Level.text = "Level " + (DataManager.ins.gameSave.level + 1);
        txt_Gold.text = DataManager.ins.gameSave.gold + "";
        txt_Day.text = "DAY " + (DataManager.ins.gameSave.level + 1);
        //Bật lại nhạc nền
        GameManager.ins.soundManager.Enable();

        //Nếu Player bị reset về Day 1 thì xóa hết tiền trong PiggyBank
        if (DataManager.ins.gameSave.level == 0)
        {
            piggyBankControl.DeleteData();
        }else{ //Load lại lượng tiền trong PiggyBank
            piggyBankControl.load_and_init();
            //Nếu có tiền thưởng thêm thì bật hiệu ứng tiền rơi
            if (DataManager.ins.gameSave.piggyBank_Case > 0) {
                piggyBank_Case = DataManager.ins.gameSave.piggyBank_Case;
                piggyBankControl.SpawnMoney(piggyBank_Case/Constant.moneyPlayerDie*2 * 0.1f, 0.1f);
            }
        }
        DataManager.ins.gameSave.piggyBank = DataManager.ins.gameSave.piggyBank + DataManager.ins.gameSave.piggyBank_Case;
        DataManager.ins.gameSave.piggyBank_Case = 0;
        txt_PiggyBank.text = (DataManager.ins.gameSave.piggyBank) + "$";
    }

    #region Button
    public void Btn_Play()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();
        DataManager.ins.gameSave.playTime++;
        DataManager.ins.gameSave.playTime_Session++;
        DataManager.ins.SaveGame();
        FirebaseManager.ins.level_start(DataManager.ins.gameSave.level + 1);
        if (piggyBankControl.coroutine_Spawn != null) StopCoroutine(piggyBankControl.coroutine_Spawn);
        //
        GameManager.ins.formGamePlay = GameManager.ins.levelCheat != FormUI.None ? GameManager.ins.levelCheat : FormUI.Form_RedLightGreenLight;
        if(GameManager.ins.isLoading2) {//Nếu bật Loading 2
            SceneManager.ins.ChangeForm(FormUI.Form_Loading2);
        } else {
            SceneManager.ins.ChangeForm(GameManager.ins.formGamePlay, 0);
        }
    }

    public void Btn_Skin()
    {

        GameManager.ins.soundManager.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Skin();
    }

    public void Btn_Settings()
    {

        GameManager.ins.soundManager.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Settings();
    }
    #endregion
}
