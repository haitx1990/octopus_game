﻿
[System.Serializable]
public class GameSave
{
    //-------- State --------
    public bool isNew = true;
    public int volumeMusic = 50;
    public int volumeSound = 50;
    public bool isVibrate = true;
    public bool isNoAds = false;
    public bool isTutCompleted = false;
    public int starRate = -1;

    //-------- Value --------
    public int gold = 0;
    public int level = 0;//Level hiện tại
    public int idSkin = 0;
    public int user_number = 100;
    public string user_name = "Player";


    //-------- Firebase --------
    public string timeInstall;//Thời điểm cài game
    public string timeLastOpen;//Thời điểm cuối cùng mở game
    public int daysPlayed = 0;//Số ngày đã User có mở game lên
    public int sessionCount = 0;//Tống số session
    public int playTime = 0;//Tổng số lần nhấn play game
    public int playTime_Session = 0;//Số lần nhấn play game trong 1 session
    public int seconds_levelCur = 0;//Số giây tại level hiện tại
    public int failcount_levelCur = 0;//Số lần fail tại level hiện tại

    //-------- Other --------
    public Skin_Save[] listSkins;

    public GameSave()
    {
        isNew = true;
        volumeMusic = 50;
        volumeSound = 50;
        isVibrate = true;
        isNoAds = false;
        isTutCompleted = false;
        starRate = -1;

        gold = 0;
        level = 0;
        idSkin = 0;
        user_number = 100;
        user_name = "Player";

        //Time
        timeInstall = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        timeLastOpen = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        daysPlayed = 0;
        sessionCount = 0;
        playTime = 0;
        playTime_Session = 0;
        seconds_levelCur = 0;
        failcount_levelCur = 0;

        
    }
}

[System.Serializable]
public class Skin_Save
{
    public int id;
    public string keyID;//ID để sau này đổi thứ tự vũ khí thì vẫn lấy đc data cũ

    //Những data sẽ thay đổi trong game
    public bool isUnlock;
    public int amountVideAds;//Số VideoAds đã xem để unlock

    public Skin_Save(int id_Skin, string key_Skin)
    {
        id = id_Skin;
        isUnlock = false;
        amountVideAds = 0;
        keyID = key_Skin;
        if (id == 0)
        {
            isUnlock = true;
        }
    }
}
