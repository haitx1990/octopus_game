﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Form_GamePlay : FormBase
{
    private void Awake()
    {
        if (GameManager.ins == null)
        {
            SceneManager.ins.ChangeForm(FormUI.Form_Loading);
            return;
        }
    }

    public override void Show()
    {
        base.Show();
        if (GameManager.ins == null) return;
        SceneManager.ins.myPlayer.isCanControl = true;
    }
}
