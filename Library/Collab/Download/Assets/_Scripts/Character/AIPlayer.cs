﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AIPlayer : PlayerBase {
    [Header("--------------- AIPlayer ---------------")]
    public Char_State charState;

    public float timeRun_AI = 0;
    public float timeIdle_AI = 0;

    public float timeMove = 0;//Để tính gia tốc

    public int randomForwardMax;//Random vị trí AIPlayer tiến lên trc 1 đoạn
    //public CharacterController controller;
    private Vector3 moveDirection = Vector3.zero;

    public override void Awake() {
        base.Awake();
    }

    public override void Start() {
        base.Start();
        idChar = 1;
        //idMaterial = DataManager.ins.gameSave.idMaterial;
        //color = GameManager.ins.listColors[idMaterial];
        namePlayer = DataManager.ins.gameSave.user_name;
        idSkin = DataManager.ins.gameSave.idSkin;
        timeMove = 0;
        //Random ra 1 con số thứ tự
        stt = Random.Range(2, 1000);
        //Nếu số thứ tự trùng Player thì sẽ là số 1
        if(stt == DataManager.ins.gameSave.user_number) stt = 1;
        //Viết thêm số 0 vào trước stt cho đủ 3 ký tự
        if(stt < 10) {
            txt_STT_Font.text = txt_STT_Back.text = "00" + stt;
        } else if(stt < 100) {
            txt_STT_Font.text = txt_STT_Back.text = "0" + stt;
        } else {
            txt_STT_Font.text = txt_STT_Back.text = "" + stt;
        }
        
    }

    public void FixedUpdate() {
        if (SceneManager.ins.formCurrent.idForm != FormUI.Form_RedLightGreenLight) return;
        if(ani_Apply == Ani_State.Dead) return;
        isWin = !isDie && ani != Ani_State.Dead &&  ani_Apply != Ani_State.Dead && transform.localPosition.z > SceneManager.ins.form_GamePlay.redLightGreenLight.zMax;

        //Load lại Animation
        ReloadAnimation();
    }

    public void ReloadAnimation() {
        if(ani_Apply != ani) {
            if(ani == Ani_State.Run) {
                audio_Run.Enable();
            } else if(ani_Apply == Ani_State.Run) {
                Timer.Schedule(this, 0.5f, () => { audio_Run.Disable(); });
            }
            //if(ani_Apply == Ani_State.Idle_InGame && ani == Ani_State.Pose_InGame) ani = Ani_State.Idle_InGame;
            ani_Apply = ani;
            animator.SetBool(Ani_State.Idle.ToString(), ani_Apply == Ani_State.Idle);
            animator.SetBool(Ani_State.Run.ToString(), ani_Apply == Ani_State.Run);
            animator.SetBool(Ani_State.Dead.ToString(), ani_Apply == Ani_State.Dead);
            animator.SetBool(Ani_State.Win.ToString(), ani_Apply == Ani_State.Win);
            animator.SetBool(Ani_State.Dance.ToString(), ani_Apply == Ani_State.Dance);
            animator.SetBool(Ani_State.Attack.ToString(), ani_Apply == Ani_State.Attack);
            animator.SetBool(Ani_State.Ulti.ToString(), ani_Apply == Ani_State.Ulti);
            animator.SetBool(Ani_State.Walk_Up.ToString(), ani_Apply == Ani_State.Walk_Up);
            animator.SetBool(Ani_State.Idle_InGame.ToString(), ani_Apply == Ani_State.Idle_InGame);
            /*if(ani_Apply == Ani_State.Pose_InGame) {
                animator.SetInteger(Ani_State.Pose_InGame.ToString(), SceneManager.ins.form_GamePlay.redLightGreenLight.doll.turn % Constant.maxPoseIdle + 1);
            } else {
                animator.SetInteger(Ani_State.Pose_InGame.ToString(), 0);
            }*/
        }
    }

    public void Move()
    {
        timeMove += Time.fixedDeltaTime;
        //if(ani_Apply != Ani_State.Run) tran_Rotate.localEulerAngles = new Vector3(tran_Rotate.localEulerAngles.x, Random.Range(-10,10), tran_Rotate.localEulerAngles.z);
        moveDirection = new Vector3(0.0f, 0.0f, 1);
        moveDirection = tran_Rotate.TransformDirection(moveDirection);//Di chuyển theo hướng quay của nhân vật
        rigid.velocity = moveDirection * Time.fixedDeltaTime * (175 + Mathf.Min(timeMove * 420, 420));

      
        //Di chuyển thẳng đều
        //moveDirection = moveDirection * 9f;
        //listAllPlayer[i].AIPlayer.controller.Move(moveDirection * Time.deltaTime);
    }

    public override void Dead() {
        ani = Ani_State.Dead;
        isWin = false;
        audio_Run.Disable();
        audio_Die.PlaySound();
        Dead_Ragdoll();
        effectBlood.SetActive(true);
        rigid.isKinematic = true;
        //dieCollider.gameObject.SetActive(true);
        myCollider.enabled = false;
    }
}
