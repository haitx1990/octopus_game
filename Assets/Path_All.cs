﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path_All : MonoBehaviour
{
    public GameObject[] obj_pathPoints;

    public List<Vector3> posPoints;
    public GameObject minionPrefabs;
    public int number_Minions;

    private void Awake()
    {
        TurnObjPathPointsToPosPoints();
        InitMinionOnThePath(number_Minions);
    }

    private void InitMinionOnThePath(int number_minions)
    {
        //Vector3 posInit = 
        //Instantiate(minionPrefabs,posInit,Quaternion.identity,transform);
    }

    private void TurnObjPathPointsToPosPoints()
    {
        foreach (GameObject opp in obj_pathPoints)
        {
            posPoints.Add(opp.transform.position);
        }
    }

}
