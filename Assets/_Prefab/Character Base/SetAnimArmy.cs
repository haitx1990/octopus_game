﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAnimArmy : MonoBehaviour
{
    public Animator anim;
    public string animStart;
    public void Start()
    {
        anim.Play(animStart);
    }
}
