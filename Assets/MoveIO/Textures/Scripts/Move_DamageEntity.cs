﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Move_DamageEntity : MonoBehaviour
{
    public GameObject Origin;

    public bool playerWeapon;
    public bool enemyWeapon;

    public float lifeTime;
    public float speed;
    public int speedRotate;

    public AudioSource Au;

    private Vector3 _dir;
    private bool _isMove = false;
    public void OnMove(Transform attacker, Transform targeter)
    {
        if (Move_GameManager.Ins.listEnemy.Count <= 1) return;
        Au.Stop();
        Au.clip = Move_SoundController.Instance.attack.Clip;
        Au.loop = false;
        Au.Play();

        Origin = attacker.gameObject;

        var v1 = attacker.position;
        v1.y = 0;

        var v2 = targeter.position;
        v2.y = 0;

        _dir = (v2 - v1);
        _dir = _dir / (Mathf.Abs(_dir.x) + Mathf.Abs(_dir.z));

        _isMove = true;
        transform.LookAt(targeter);
        StartCoroutine(autoDestroy());

    }

    IEnumerator autoDestroy()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

    private void Update()
    {
        if (!_isMove) return;
        var angles = transform.rotation.eulerAngles;
        angles.y += Time.deltaTime * speedRotate;
        transform.rotation = Quaternion.Euler(angles);
        transform.position += _dir * Time.deltaTime * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Origin || !_isMove) return;

        var player = other.GetComponent<Move_PlayerController>();
        if (player != null && playerWeapon) return;
        if(player != null) player.SetDied();

        var enemy = other.GetComponent<Move_EnemyController>();
        if(enemy != null) enemy.SetDied();

        if(player != null || enemy != null)
        {
            var tran = Instantiate(Move_GameManager.Ins.EfxBlood);
            tran.transform.position = transform.position;
        }

        _isMove = false;
        Destroy(gameObject);
    }
}
