﻿using System.Collections;
using TMPro;
using UnityEngine;

public class Move_PlayerController : MonoBehaviour
{
    public static Move_PlayerController Ins;
    private void Awake()
    {
        Ins = this;
    }

    public AudioSource Die;
    public AudioSource Run;

    public Transform tran_Rotate;
    public CharacterController controller;
    public Animator anim;
    public GameObject transWeapon;
    public GameObject knife;

    public float range;
    public int Hp;
    public float speed;
    public bool isCanMove = false;
    public bool isPlayingAttackAnim { get; set; }
    public Coroutine AttackCoroutine;

    public TextMeshPro txt_front;
    public TextMeshPro txt_back;


    private Vector3 moveDirection = Vector3.zero;
    private Transform victim;
    private RaycastHit[] hit;
    [HideInInspector] public bool _isDead;

    private GameObject _weapon;
    public void SetWeapon(GameObject weap, GameObject weapThrow)
    {
        _weapon = weap;
        for (var i = transWeapon.transform.childCount - 1; i > 0; i--)
            //transWeapon.transform.GetChild(i).gameObject.SetActive(false);
            Destroy(transWeapon.transform.GetChild(i).gameObject);
        transWeapon.transform.GetChild(0).gameObject.SetActive(false);

        var r = 456;
        txt_front.text = r + "";
        txt_back.text = r + "";

        if (weap == null || weapThrow == null) return;

        //clone transform
        var obj = Instantiate(_weapon, transWeapon.transform).transform;
        obj.localPosition = transWeapon.transform.GetChild(0).localPosition;
        obj.localScale = transWeapon.transform.GetChild(0).localScale;
        obj.rotation = Quaternion.Euler(transWeapon.transform.GetChild(0).rotation.eulerAngles);

        knife = weapThrow;
        knife.GetComponent<Move_DamageEntity>().playerWeapon = true;
        knife.GetComponent<Move_DamageEntity>().enemyWeapon = false;
    }

    public void FixedUpdate()
    {
        if (_isDead || !Move_GameManager.Ins.isGamePlaying) return;
        if (Hp > 0)
        {
            // Đi thẳng về phía trước mặt
            if (Input.GetMouseButton(0) && isCanMove)
            {
                ani = Move_Ani_State.Run;
                //Dừng tấn công lại
                if (isPlayingAttackAnim)
                {
                    isPlayingAttackAnim = false;
                    StopCoroutine(AttackCoroutine);
                    AttackCoroutine = null;
                }

                //Tính vận tốc và hướng của MainChar
                moveDirection = new Vector3(0.0f, 0.0f, 1);
                moveDirection = tran_Rotate.TransformDirection(moveDirection);//Di chuyển theo hướng quay của nhân vật
                moveDirection = moveDirection * speed;

                // Đi thẳng về phía trước mặt
                if (Input.GetMouseButton(0) && isCanMove && !isPlayingAttackAnim)
                {
                    controller.Move(moveDirection * Time.deltaTime);
                }
                //Khi ko trong chế độ tự động tấn công thì Quay mặt theo hướng controll đang điều khiển
                if (Move_TouchRotateSingle.eulerRotation != Vector3.zero)
                {
                    tran_Rotate.localRotation = Quaternion.LookRotation(Move_TouchRotateSingle.eulerRotation);
                }
            }
            else
            {
                if (isPlayingAttackAnim) return;
                //Trạng thái đứng đợi hoặc tấn công Enemy
                if (ani == Move_Ani_State.Run) ani = Move_Ani_State.Idle;

                //Tìm kiếm mục tiêu
                victim = null;
                for (var i = 1; i < Move_GameManager.Ins.listEnemy.Count; i++)
                {
                    if (Vector3.Distance(transform.position, Move_GameManager.Ins.listEnemy[i].transform.position) < range)
                    {
                        victim = Move_GameManager.Ins.listEnemy[i].transform;
                        break;
                    }
                }

                //Nếu chưa tấn công và vũ khí đã hồi lại -> Attack
                if (transWeapon.activeSelf && !isPlayingAttackAnim && victim != null)
                {
                    tran_Rotate.LookAt(victim);//Xoay mặt về phía Enemy
                    tran_Rotate.localRotation = Quaternion.Euler(0, tran_Rotate.localEulerAngles.y, 0);
                    AttackCoroutine = StartCoroutine(AttackRoutine());
                }
            }
            if (transform.position.y > 0.1f) transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
        else
        {
            isCanMove = false;
        }

    }

    public void SetDied()
    {
        if (Move_GameManager.Ins.listEnemy.Count == 1 && !_isDead) return;
        Move_GameManager.Ins.isGamePlaying = false;
        //Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.pDie);
        Die.Play();
        ani = Move_Ani_State.Dead;
        _isDead = true;
        ReloadAnimation();
        StartCoroutine(ie_Die());
    }

    IEnumerator ie_Die()
    {
        yield return new WaitForSeconds(2f);
        Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.loseBg);
        //Move_GameManager.Ins.Endgame(false);
        Move_GameManager.Ins.ShowPopup_Revive(Revive);
        //yield return new WaitForSeconds(1f);
        //ani = Ani_State.Idle;
        //ReloadAnimation();
    }

    public void Revive()
    {
        Move_GameManager.Ins.isGamePlaying = true;
        ani = Move_Ani_State.Idle;
        _isDead = false;
        ReloadAnimation();
        Move_GameManager.Ins.SetSlient();
        Move_GameManager.Ins.CheckWin();
    }

    void LateUpdate()
    {
        if (ani == Move_Ani_State.Run)
        {
            Run.enabled = true;
        }
        else
        {
            Timer.Schedule(this, 0.5f, () => { Run.enabled = false; });
        }
        if (_isDead) return;
        ReloadAnimation();
    }

    public Move_Ani_State ani;
    public Move_Ani_State ani_Apply;
    public void ReloadAnimation()
    {

        //Nếu trạng thái animation thay đổi
        if (ani_Apply == ani)
            return;
        //Bật animation sau mỗi frame
        ani_Apply = ani;
        switch (ani)
        {
            case Move_Ani_State.Idle:
                anim.SetBool("IsIdle", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                anim.SetBool("IsDance", false);
                break;
            case Move_Ani_State.Dance:
                anim.SetBool("IsIdle", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                anim.SetBool("IsDance", true);
                break;
            case Move_Ani_State.Run:
                anim.SetBool("IsIdle", false);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                break;
            case Move_Ani_State.Attack:
                anim.SetBool("IsAttack", true);
                anim.SetBool("IsUlti", false);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", false);
                break;
            case Move_Ani_State.Ulti:
                anim.SetBool("IsAttack", true);
                anim.SetBool("IsUlti", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", false);
                break;
            case Move_Ani_State.Win:
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", true);
                break;
            case Move_Ani_State.Dead:
                anim.SetBool("IsDead", true);
                break;
            default:
                Debug.LogError("Lỗi animationState Player" + ani.ToString());
                break;
        }
    }

    public GameObject follower;
    public Transform followerParent;
    public void SpawnFollower(Move_EnemyController e, Material mat)
    {
        //var f = Instantiate(follower).GetComponent<TargetFollower>();
        //f.transform.SetParent(followerParent);
        //f.Init(e, mat);
    }

    public IEnumerator AttackRoutine()
    {
        //Chạy Animation Attack
        isPlayingAttackAnim = true;
        ani = Move_Ani_State.Attack;

        //Thời điểm viên đạn đc bắn ra
        yield return new WaitForSeconds(0.23f);//+ 0.25f
        SpawnBullet(victim);
        transWeapon.SetActive(false);
        StartCoroutine(EnableWeapon());//Sẽ hiển thị lại vũ khí sau 1 lúc
        //Hết Animation Attack -> Trở về Idle
        yield return new WaitForSeconds(0.26f);
        ani = Move_Ani_State.Idle;//Tạm thời set Idle
        //Delay giữa các lần AutoAttack
        yield return new WaitForSeconds(1f);
        isPlayingAttackAnim = false;
        AttackCoroutine = null;
    }

    public IEnumerator EnableWeapon()
    {
        yield return new WaitForSeconds(0.4f);
        transWeapon.SetActive(true);
        //yield return new WaitForSeconds(0.6f);
        //isAutoAttack = false;
    }

    public void SpawnBullet(Transform target)
    {
        Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.attack);
        var b = Instantiate(knife).transform;
        b.transform.position = transWeapon.transform.position;
        b.transform.rotation = Quaternion.Euler(0, 0, 90);
        b.transform.localScale = Vector3.one;
        var scr = b.GetComponent<Move_DamageEntity>();
        scr.playerWeapon = true;
        scr.enemyWeapon = false;
        scr.OnMove(tran_Rotate, target);
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}

public enum Move_Ani_State
{
    None,
    Idle,
    Walk,
    Run,
    Attack,
    Dance,
    Dead,
    Win,
    OutGame_Dead,
    OutGame_Idle,
    Free_Run,
    Free_Idle,
    Attack_RunToEnemy,
    Attack_IdleStartAutoAttack,
    Attack_IdleDelayAttack,
    Attack_IdleWaitAttack,
    Attack_Attack,
    Attack_IdleAfterAttack,
    Attack_RunToDodge,
    Dead_Dead,
    Ulti,
    Attack_Ulti,
    Attack_Zombie
}