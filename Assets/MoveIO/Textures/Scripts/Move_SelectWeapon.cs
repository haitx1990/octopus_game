﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_SelectWeapon : MonoBehaviour
{
    public List<Transform> listSlot;
    public Transform playerStartPos;

    public void Setup(int id1, int id2, int id3)
    {
        weaponIndx = -1;
        SpawnWeapon(0, id1);
        SpawnWeapon(1, id2);
        SpawnWeapon(2, id3);
    }

    public void SpawnWeapon(int slotId, int id)
    {
        var o = Instantiate(Move_GameManager.Ins.WeaponThrow[id]).transform;
        o.SetParent(listSlot[slotId].GetChild(0));
        o.localPosition = Vector3.forward * 0.3f;
        o.localRotation = Quaternion.Euler(180, 0, 270);
        o.localScale = Vector3.one;
        o.gameObject.tag = "Element";
        o.gameObject.layer = LayerMask.NameToLayer("Element");
        var scr = o.gameObject.AddComponent<Element>();
        scr.Init(slotId, id, this);
    }

    public int weaponIndx = -1;
    public void ChooseWeapon(int indx, int offset)
    {
        weaponIndx = offset;
        ShowAll();
        listSlot[offset].GetChild(0).gameObject.SetActive(false);
        //spawn weapon in player
        Move_PlayerController.Ins.SetWeapon(Move_GameManager.Ins.Weapon[indx], Move_GameManager.Ins.WeaponThrow[indx]);
    }

    public void ShowAll()
    {
        for(var i = 0; i < listSlot.Count; i++)
        {
            listSlot[i].GetChild(0).gameObject.SetActive(true);
        }
    }
}
