﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Move_GameManager : MonoBehaviour
{
    public static Move_GameManager Ins;

    public bool isFinalGame = false;

    public List<GameObject> listEnemy;
    public GameObject EnemyObj;
    public GameObject EfxBlood;

    [Header("Game Status")]
    bool isStartGame;
    public bool isGamePlaying;
    public List<Move_AI_Config> AILevel;
    public List<GameObject> Weapon;
    public List<GameObject> WeaponThrow;

    [Header("UI")]
    public CanvasGroup TutObj;
    public Text txtAlive;
    public Text txtTime;
    public int maxTime;
    public Move_SelectWeapon selectWeaponObj;

    [Header("Map")]
    [SerializeField] private List<Transform> List_Start = new List<Transform>();
    [SerializeField] private List<Transform> List_AI = new List<Transform>();
    [SerializeField] private List<Transform> List_PlayerStart = new List<Transform>();
    [SerializeField] private List<PosRegister> listPosUsed = new List<PosRegister>();
    private object scenemanager;
    private bool canReady;
    private void Awake()
    {
        Ins = this;
        TutObj.alpha = 0;
    }

    #region StartGame
    public void Start()
    {
        canReady = false;
        Move_CameraFollow.Ins.state = 2;
        List_Start = DeepClone(Move_MapConfig.Ins.List_Start);
        List_AI = DeepClone(Move_MapConfig.Ins.List_AI);
        List_PlayerStart = DeepClone(Move_MapConfig.Ins.List_PlayerStart);

        #region Player Setting
        //Move_PlayerController.Ins.transform.position = List_PlayerStart[Random.Range(0, List_PlayerStart.Count)].position;
        Move_PlayerController.Ins.transform.position = selectWeaponObj.playerStartPos.position;
        var r = Random.Range(0, Weapon.Count - 5);
        var r2 = Random.Range(Weapon.Count - 4, Weapon.Count);
        Move_PlayerController.Ins.SetWeapon(null, null);
        selectWeaponObj.Setup(r, r + 1, r2);
        #endregion

        if (!isFinalGame) OnStart(20);
        else OnStart(10);
    }

    public void GetReadyGo()
    {
        canReady = true;
        Move_CameraFollow.Ins.state = 0;
        Move_PlayerController.Ins.transform.position = List_PlayerStart[Random.Range(0, List_PlayerStart.Count)].position;
        TutObj.alpha = 1;
        TutObj.interactable = true;
    }


    void OnStart(int enemyCount)
    {
        if (!isFinalGame)
        {
            txtAlive.text = "Alive: " + enemyCount;
            txtTime.text = GetTime(maxTime);
        }
        listEnemy.Clear();
        listEnemy.Add(Move_PlayerController.Ins.gameObject);

        for (var i = 0; i < enemyCount; i++) SpawnEnemy(i);
    }

    public void StartGame()
    {
        if (isStartGame) return;
        if (!canReady) return;
        SceneManager.ins.form_GamePlay.alivePlayer = listEnemy.Count;
        if (!isFinalGame) CoolDown();
        SetSlient();
        isStartGame = true;
        isGamePlaying = true;
        Move_CameraFollow.Ins.state = 1;
        Move_PlayerController.Ins.ani = Move_Ani_State.Idle;
        HideTut();
        RefreshHunterPlayer();
    }

    public void SpawnEnemy(int indx)
    {
        var e = Instantiate(EnemyObj).GetComponent<Move_EnemyController>();
        var et = e.transform;
        var ran = Random.Range(0, Weapon.Count);
        e.SetWeapon(Weapon[ran], WeaponThrow[ran]);

        var r = new PosRegister();
        r.target = e.gameObject;
        r.pos = List_Start[indx];
        listPosUsed.Add(r);

        var pos = r.pos;
        et.position = pos.position;
        et.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
        e.Init(AILevel[Random.Range(0, AILevel.Count)]);
        listEnemy.Add(e.gameObject);
    }
    #endregion

    #region Output
    public void CheckWin()
    {
        txtAlive.text = "Alive: " + listEnemy.Count;
        SceneManager.ins.form_GamePlay.alivePlayer = listEnemy.Count;
        if (listEnemy.Count == 1 && !Move_PlayerController.Ins._isDead)
        {
            SetWin();
        }
    }

    public void SetWin()
    {
        Move_CameraFollow.Ins.state = 0;
        StopAllCoroutines();
        isGamePlaying = false;
        Move_PlayerController.Ins.StopAllCoroutines();
        Move_PlayerController.Ins.ani = Move_Ani_State.Win;
        Move_PlayerController.Ins.ReloadAnimation();
        Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.victory);
        StartCoroutine(Endgame(true));
    }

    //Die 
    IEnumerator Endgame(bool isWin)
    {
        yield return Yielders.Get(3f);
        //PanelEndgame.SetActive(true);
        ShowPopup_EndGame(isWin);
    }

    public void Restart()
    {
        //SceneManager.LoadScene("MoveStopMove");
    }

    public int GetCountAlive()
    {
        return listEnemy.Count;
    }

    #endregion

    #region Support
    #region Tutorial
    public void ShowTut()
    {
        StartCoroutine(ie_ShowTut());
    }

    public void HideTut()
    {
        Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.btnClick);
        StartCoroutine(ie_HideTut());
    }

    IEnumerator ie_HideTut()
    {
        var t = 1f;
        TutObj.alpha = t;
        while (t >= 0)
        {
            t -= Time.deltaTime * 2;
            TutObj.alpha = t;
            yield return Yielders.FixedUpdate;
        }
    }

    IEnumerator ie_ShowTut()
    {
        var t = 0f;
        TutObj.alpha = t;
        while (t >= 0)
        {
            t -= Time.deltaTime * 2;
            TutObj.alpha = t;
            yield return Yielders.FixedUpdate;
        }
    }
    #endregion

    public void RefreshHunterPlayer()
    {
        StartCoroutine(Ie_RefreshHunterPlayer());
    }

    IEnumerator Ie_RefreshHunterPlayer()
    {
        while (listEnemy.Count != 1 && !Move_PlayerController.Ins._isDead)
        {
            RefreshPlayerHunter();
            yield return Yielders.Get(5);
        }
    }

    public void RefreshPlayerHunter()
    {
        for (var i = 1; i < listEnemy.Count; i++)
        {
            listEnemy[i].GetComponent<Move_EnemyController>().playerHunter = false;
        }
        var ran = Random.Range(3, 4);
        ran = Mathf.Min(ran, listEnemy.Count);
        for (var i = 0; i < ran; i++)
        {
            var r = Random.Range(1, listEnemy.Count);
            listEnemy[r].GetComponent<Move_EnemyController>().playerHunter = true;
        }
    }

    public void CoolDown()
    {
        StartCoroutine(Ie_Cooldown());
    }

    public string GetTime(int t)
    {
        var s = "";
        var sec = t % 60;
        var min = t / 60;
        s = ((min < 10 ? "0" : "") + min) + ":" + ((sec < 10 ? "0" : "") + sec);
        return s;
    }

    IEnumerator Ie_Cooldown()
    {
        var time = maxTime;
        while (time > 0)
        {
            if (isGamePlaying) time--;
            txtTime.text = GetTime(time);
            yield return Yielders.Get(1f);
        }
        if (!Move_PlayerController.Ins._isDead) SetWin();
    }

    [HideInInspector] public bool isSlient;
    public void SetSlient()
    {
        isSlient = true;
        StartCoroutine(ie_Slient());
    }

    IEnumerator ie_Slient()
    {
        var t = 3;
        while (t >= 0)
        {
            t--;
            yield return Yielders.Get(1f);
        }
        isSlient = false;
    }

    public Transform GetRandomPos()
    {
        var t = Move_MapConfig.Ins.List_AI.Count;
        return Move_MapConfig.Ins.List_AI[Random.Range(0, t)];
    }

    public void FreePos(GameObject target)
    {
        var pos = listPosUsed.Find(x => x.target == target);
        if (pos == null) return;
        listPosUsed.Remove(pos);
    }

    public void RefreshPos()
    {
        for (var i = 0; i < listPosUsed.Count; i++)
        {
            if (listPosUsed[i].target == null)
            {
                listPosUsed.RemoveAt(i);
                i--;
            }
        }
    }

    public List<Transform> DeepClone(List<Transform> l)
    {
        var res = new List<Transform>();
        foreach (var i in l) res.Add(i);
        return res;
    }

    [HideInInspector] public Popup_EndGame popup_EndGame;
    [HideInInspector] public Popup_Revive popup_Revive;
    public Canvas popupCanvas;
    public void ShowPopup_EndGame(bool isWin = false)
    {
        if (isWin)
        {
            //Nếu đã chơi đủ số ngày rồi thì mới show WinAll
            if (DataManager.ins.gameSave.level >= GameManager.ins.listMiniGame.Length - 1)
            {
                SceneManager.ins.ShowPopup_WinAll();
            }
            else
            {//Nếu ko thì chỉ show WinMinigame
                SceneManager.ins.ShowPopup_EndGame(true);
            }
        }
        else SceneManager.ins.ShowPopup_EndGame(isWin);
    }
    public void ShowPopup_Revive(System.Action c)
    {
        SceneManager.ins.ShowPopup_Revive(c);
    }
    #endregion

}

public class PosRegister
{
    public Transform pos;
    public GameObject target;
}