﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_MapConfig : MonoBehaviour
{
    public static Move_MapConfig Ins;
    
    public Transform Node_Start;
    public Transform Node_AI;
    public Transform Node_Player;
    public List<Transform> List_Start = new List<Transform>();
    public List<Transform> List_AI = new List<Transform>();
    public List<Transform> List_PlayerStart = new List<Transform>();

    private void Awake()
    {
        Ins = this;
        RandomNode();
    }

    public void RandomNode()
    {
        //Node start
        var c = Node_Start.childCount;
        for(var i = 0; i < c; i++)
        {
            Node_Start.GetChild(i).SetSiblingIndex(Random.Range(0, c));
        }
        List_Start.Clear();
        for (var i = 0; i < c; i++) List_Start.Add(Node_Start.GetChild(i));

        //Node AI
        c = Node_AI.childCount;
        for (var i = 0; i < c; i++)
        {
            Node_AI.GetChild(i).SetSiblingIndex(Random.Range(0, c));
        }
        List_AI.Clear();
        for (var i = 0; i < c; i++) List_AI.Add(Node_AI.GetChild(i));

        //Node Player Start
        c = Node_Player.childCount;
        for (var i = 0; i < c; i++)
        {
            Node_Player.GetChild(i).SetSiblingIndex(Random.Range(0, c));
        }
        List_PlayerStart.Clear();
        for (var i = 0; i < c; i++) List_PlayerStart.Add(Node_Player.GetChild(i));
    }

}
