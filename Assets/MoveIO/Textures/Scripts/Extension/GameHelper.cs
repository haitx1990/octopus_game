﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public static class GameHelper
{
    public static Vector2 WorldToCanvas(this Canvas canvas, Vector3 world_position, Camera camera = null)
    {
        if (camera == null)
        {
            camera = Camera.main;
        }

        var viewport_position = camera.WorldToViewportPoint(world_position);
        var canvas_rect = canvas.GetComponent<RectTransform>();

        return new Vector2((viewport_position.x * canvas_rect.sizeDelta.x) - (canvas_rect.sizeDelta.x * 0.5f),
            (viewport_position.y * canvas_rect.sizeDelta.y) - (canvas_rect.sizeDelta.y * 0.5f));
    }

    //public static Vector2 vTemp;
    public static void MoveXY(this Transform t, Vector2 d)
    {
        d.x += t.position.x;
        d.y += t.position.y;
        t.position = d;
    }
	
	/// <summary>
    /// Extension method to return an enum value of type T for the given string.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this string value)
    {
        return (T)System.Enum.Parse(typeof(T), value, true);
    }

    /// <summary>
    /// Extension method to return an enum value of type T for the given int.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    public static T ToEnum<T>(this int value)
    {
        var name = System.Enum.GetName(typeof(T), value);
        return name.ToEnum<T>();
    }

    public static void SetBool(this PlayerPrefs p, string key, bool value)
    {

    }

    public static void RemoveAllChildOnEditor(this Transform t)
    {
        int c = t.childCount;
        for (int i = 0; i < c; i++)
            Object.DestroyImmediate(t.GetChild(0).gameObject);
    }
    public static List<GameObject> GetAllChilds(this Transform t)
    {
        List<GameObject> childs = new List<GameObject>();
        int c = t.childCount;
        for (int i = 0; i < c; i++)
            childs.Add(t.GetChild(i).gameObject);
        return childs;
    }

    public static void RemoveAllChild2(this Transform t, int ortherIndex = -1)
    {
        List<Transform> lst = new List<Transform>();
        for (int i = 0; i < t.childCount; i++)
            if(i != ortherIndex) lst.Add(t.GetChild(i));
        for (int i = 0; i < lst.Count; i++)
            GameObject.Destroy(lst[i].gameObject);
    }

    public static void RemoveAllChild(this Transform t)
    {
        int c = t.childCount;
        for (int i = 0; i < c; i++)
            Object.Destroy(t.GetChild(0).gameObject);
    }

    public static void RemoveAllChildOther(this Transform t, Transform obj)
    {
        int c = t.childCount;
        int c1 = 0;
        for (int i = 0; i < c; i++) {
            if (t.GetChild(0) != obj) Object.Destroy(t.GetChild(c1).gameObject);
            else
            {
                c1++;
            }
        }
    }

    public static void SetSizeFollowWidth(this Image img, int maxWidth)
    {
        if (img.sprite == null)
            return;
        float aspect = img.sprite.bounds.size.y / img.sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(maxWidth, maxWidth * aspect);
    }

    public static void SetSizeFollowHeight(this Image img, int maxHeight)
    {
        if (img.sprite == null)
            return;
        float aspect = img.sprite.bounds.size.y / img.sprite.bounds.size.x;
        img.GetComponent<RectTransform>().sizeDelta = new Vector2(maxHeight / aspect, maxHeight);
    }

    public static void DeleteAllChilds(this Transform t)
    {
        int count = t.childCount;
        for (int i = 0; i < count; i++)
            Object.Destroy(t.GetChild(0).gameObject);
    }

    public static void DeleteImmediateAllChilds(this Transform t)
    {
        int count = t.childCount;
        for (int i = 0; i < count; i++)
            Object.DestroyImmediate(t.GetChild(0).gameObject);
    }

    public static void AlignWithPivot(this Image i)
    {
        if (i.sprite == null)
            return;
        Vector2 size = i.GetComponent<RectTransform>().rect.size;
        Vector2 pixelPivot = i.sprite.pivot;
        Vector2 percentPivot = new Vector2(pixelPivot.x / size.x, pixelPivot.y / size.y);
        i.GetComponent<RectTransform>().pivot = percentPivot;
    }
    private static System.Random rng = new System.Random();
    public static void Shuffle<T>(this T[] array)
    {
        int i = array.Length;
        while (i > 1)
        {
            int num = rng.Next(i--);
            T t = array[i];
            array[i] = array[num];
            array[num] = t;
        }
    }
    public static void Shuffle<T>(this List<T> array)
    {
        int i = array.Count;
        while (i > 1)
        {
            int num = rng.Next(i--);
            T t = array[i];
            array[i] = array[num];
            array[num] = t;
        }
    }

    private const float DegToRad = Mathf.PI / 180;

    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        return v.RotateRadians(degrees * DegToRad);
    }

    public static Vector2 RotateRadians(this Vector2 v, float radians)
    {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }

    public static Vector3 Truncate(this Vector3 original, float max)
    {
        original.Normalize();
        original *= max;
        return original;
    }

    public static float GetAngle(this Transform t, Vector3 target)
    {
        return (float)Mathf.Atan2(target.y - t.position.y, target.x - t.position.x) * Mathf.Rad2Deg - 90;
    }

    public static float GetAngle(this Vector3 t, Vector3 target)
    {
        return (float)Mathf.Atan2(target.y - t.y, target.x - t.x) * Mathf.Rad2Deg - 90;
    }
    public static float GetAngle(this Vector3 t)
    {
        return (float)Mathf.Atan2(t.y, t.x) * Mathf.Rad2Deg;
    }
    public static float GetAngle(this Vector2 t)
    {
        return (float)Mathf.Atan2(t.y, t.x) * Mathf.Rad2Deg;
    }

    public static float GetAngleZero(this Vector3 t, Vector3 target)
    {
        return (float)Mathf.Atan2(target.y - t.y, target.x - t.x) * Mathf.Rad2Deg + 90;
    }

    public static List<Vector3> FlipList(this List<Vector3> l)
    {
        return l.Select(s => new Vector3(-s.x, s.y)).ToList();
    }

    public static T DeepClone<T>(T obj)
    {
        using (var ms = new MemoryStream())
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            ms.Position = 0;

            return (T)formatter.Deserialize(ms);
        }
    }

    public static void SetSizeX(this SpriteRenderer render, int size)
    {
        float scale = size / render.sprite.rect.width;
        render.transform.localScale = new Vector3(scale, scale);
    }

    public static void SetSizeY(this SpriteRenderer render, int size)
    {
        float scale = size / render.sprite.rect.height;
        render.transform.localScale = new Vector3(scale, scale);
    }

    public static Vector3 Rotate(this Vector3 v, float degrees)
    {
        return v.RotateRadians(degrees * DegToRad);
    }

    public static Vector3 RotateRadians(this Vector3 v, float radians)
    {
        var ca = Mathf.Cos(radians);
        var sa = Mathf.Sin(radians);
        return new Vector2(ca * v.x - sa * v.y, sa * v.x + ca * v.y);
    }

    public static Color SetColorAlpha(Color c, float a)
    {
        c.a = a;
        return c;
    }

    public static int CurrentTimeInSecond
    {
        get
        {
            return (int)(System.DateTime.Now - new System.DateTime(1970, 1, 1)).TotalSeconds;
        }
    }

    public static string FormatTimeMMSS(int second)
    {
        string s = "";
        var min = second / 60;
        s += min < 10 ? "0" + min : "" + min;
        s += ":";
        var sec = second % 60;
        s += sec < 10 ? "0" + sec : "" + sec;
        return s;
    }

    public static string FormatTimeHHMMSS(int second)
    {
        string s = "";
        var hour = second / 3600;
        s += hour < 10 ? "0" + hour : "" + hour;
        s += ":";

        var min = (second - hour * 3600) / 60;
        s += min < 10 ? "0" + min : "" + min;
        s += ":";

        var sec = second % 60;
        s += sec < 10 ? "0" + sec : "" + sec;
        return s;
    }

    public static void FlipX(this Transform t)
    {
        t.localScale = new Vector3(t.localScale.x * -1, t.localScale.y, t.localScale.z);
    }

    public static void FlipX(this Transform t, int dir)
    {
        if(dir < 0) t.localScale = new Vector3( -Mathf.Abs(t.localScale.x), t.localScale.y, t.localScale.z);
        else t.localScale = new Vector3(Mathf.Abs(t.localScale.x), t.localScale.y, t.localScale.z);
    }

#if UNITY_EDITOR

    public static List<Object> GetAllAssets(string path)
    {
        string[] paths = { path };
        var assets = UnityEditor.AssetDatabase.FindAssets(null, paths);
        var assetsObj = assets.Select(s => UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(UnityEditor.AssetDatabase.GUIDToAssetPath(s))).ToList();
        return assetsObj;
    }

#endif
}