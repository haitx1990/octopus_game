﻿using System;
using System.Collections.Generic;
using UnityEngine;

public partial class Move_SoundController : MonoBehaviour
{
    public static Move_SoundController Instance;
    public bool OnSound = true;
    public bool OnMusic = true;
    [HideInInspector]
    public AudioSource BackgroundSound;
    [HideInInspector]
    public List<AudioSource> LoopSound = new List<AudioSource>();
    [HideInInspector]
    public List<AudioSource> Pools;
    [HideInInspector]
    public List<AudioSource> Spawns;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        BackgroundSound = gameObject.AddComponent<AudioSource>();
        BackgroundSound.volume = DataManager.ins.gameSave.volumeMusic;
    }

    public void PlayBg(AudioClip clip, float volume = 1f, bool loop = true)
    {
        if (OnMusic)
        {
            BackgroundSound.Stop();
            BackgroundSound.clip = CurrentBgClip;
            BackgroundSound.volume = DataManager.ins.gameSave.volumeMusic;
            BackgroundSound.loop = loop;
            BackgroundSound.Play();
        }
    }

    public int PlayLoop(AudioClip clip)
    {
        for (var i = 0; i < LoopSound.Count; i++)
        {
            if(LoopSound[i].isPlaying) continue;
            LoopSound[i].clip = clip;
            LoopSound[i].Play();
            return i;
        }
        var au = gameObject.AddComponent<AudioSource>();
        au.clip = clip;
        au.loop = true;
        LoopSound.Add(au);
        LoopSound[LoopSound.Count - 1].Play();
        return LoopSound.Count - 1;
    }

    public static int PlayLoopSound(SoundInfor clip)
    {
        return Instance.PlayLoop(clip.Clip);
    }

    public void StopLoop(int id)
    {
        if (LoopSound[id] != null)
        {
            LoopSound[id].Stop();
        }
    }

    public static void StopLoopSound(int id)
    {
        Instance.StopLoop(id);
    }

    public static AudioClip CurrentBgClip;
    public static void PlayBackgroundSound(AudioClip clip, float volume = 1, bool loop = true)
    {
        CurrentBgClip = clip;
        Instance.PlayBg(CurrentBgClip, volume, loop);
    }

    public static void PlayBackgroundSound(SoundInfor infor = null, bool loop = true)
    {
        if (infor == null)
            Instance.BackgroundSound.volume = 1;
        else
        {
            CurrentBgClip = infor.Clip;
            Instance.PlayBg(CurrentBgClip, Instance.OnMusic ? infor.Volume : 0, loop);
        }
            
    }

    public static void StopBackgroundSound()
    {
        Instance.BackgroundSound.volume = 0;
    }

    public void PlaySound(AudioClip clip, float volume = 1)
    {
        if (!OnSound)
            return;

        if (Pools.Count > 0)
        {
            Pools[0].clip = clip;
            Pools[0].volume = DataManager.ins.gameSave.volumeSound;
            Pools[0].Play();
            Spawns.Add(Pools[0]);
            Pools.RemoveAt(0);
        }
        else
        {
            int count = Spawns.Count;
            bool has = false;
            for (int i = 0; i < count;)
            {
                if (!Spawns[i].isPlaying)
                {
                    has = true;
                    Pools.Add(Spawns[i]);
                    Spawns.RemoveAt(i);
                    count--;
                }
                else i++;
            }
            if (has)
            {
                PlaySoundEffect(clip, DataManager.ins.gameSave.volumeSound);
            }
            else
            {
                AudioSource sound = gameObject.AddComponent<AudioSource>();
                sound.clip = clip;
                sound.volume = DataManager.ins.gameSave.volumeSound;
                sound.Play();
                Spawns.Add(sound);
            }
        }
    }

    public static void PlaySoundEffect(SoundInfor infor)
    {
        if (infor.Clip == null)
            return;
        Instance.PlaySound(infor.Clip, infor.Volume);
    }

    public static void StopSoundEffect(SoundInfor infor)
    {
        if (infor.Clip == null) return;
    }

    public static void PlaySoundEffect(AudioClip clip, float volume = 1)
    {
        Instance.PlaySound(clip, volume);
    }

    public static void StopSoundEffect()
    {
        int count = Instance.Spawns.Count;
        for (int i = 0; i < count; i++)
        {
            Instance.Spawns[0].Stop();
            Instance.Pools.Add(Instance.Spawns[0]);
            Instance.Spawns.RemoveAt(0);
        }
    }

    public static void StopAll()
    {
        StopBackgroundSound();
        StopSoundEffect();
    }
}

[Serializable]
public class SoundInfor
{
    public AudioClip Clip;
    [Range(0, 1)]
    public float Volume = 1;
}
