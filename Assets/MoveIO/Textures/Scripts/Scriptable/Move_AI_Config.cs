﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AI_Config", menuName = "Assets/AI_Config", order = 2)]
//Lưu thông tin của AI
public class Move_AI_Config : ScriptableObject
{
    public float Ranger;

    //Tỷ lệ đi thẳng về phía Enemy đó khi trong tầm nhìn (Tầm nhìn > tấm đánh)
    [Header("-------- Time Move To Enemy --------")]
    public int rate_MoveToEnemy;
    public float time_MoveToEnemy_Min;
    public float time_MoveToEnemy_Max;

    //Khoảng thời gian đi sâu vào RangeAttack của Player khác
    //[Header("-------- Time Move Range Attack --------")]
    //public bool isMoveRangeAttack;
    //public float time_MoveRangeAttack_Min;
    //public float time_MoveRangeAttack_Max;

    //Đứng im 1 chút rồi mới attack
    [Header("-------- Delay Attack --------")]
    public bool isDelayAttack;
    public float time_DelayAttack_Min;
    public float time_DelayAttack_Max;

    //Khoảng thời gian attack
    [Header("-------- Attack --------")]
    //public bool isWaitFullTime;//Có Idle đợi đủ thời gian Attack ko? Hay là nếu ko có Enemy thì đi tiếp luôn
    public float time_Attack_Min;
    public float time_Attack_Max;
    public int rate_NewNode;//Tỷ lệ đổi hướng sau khi tấn công

    //Khoảng thời gian di chuyển sau khi tấn công
    [Header("-------- Time Move Dodge --------")]
    public bool isMoveEnemy_Dodge;//Có biết né đạn theo phương vuông góc ko?
    public float time_MoveDodge_Min;
    public float time_MoveDodge_Max;


    //Có biết chạy 1 đoạn rồi đứng im ko?
    [Header("-------- Idle --------")]
    //public bool isMoveEnemy_Idle;
    public float time_MoveEnemy_Run_Min;
    public float time_MoveEnemy_Run_Max;
    public float time_MoveEnemy_Idle_Min;
    public float time_MoveEnemy_Idle_Max;

    //Khi gặp Enemy
    public bool isNoAttack;

    //public bool isJustPlayer;//Chỉ tấn công Player
    public bool isMoveEnemy_Excape; //Có biết chạy trốn Enemy mạnh hơn ko?
    public bool isMoveEnemy_Chase; //Có biết đuổi theo Enemy yếu hơn ko?
}