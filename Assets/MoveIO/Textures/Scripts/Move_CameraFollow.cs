﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_CameraFollow : MonoBehaviour
{
    public static Move_CameraFollow Ins;
    private void Awake()
    {
        Ins = this;
    }

    public int state = 0;
    private int oldState = 0;

    public Vector3 Pos1;
    public Vector3 Rot1;
    public Vector3 Pos2;
    public Vector3 Rot2;
    public Vector3 Pos3;
    public Vector3 Rot3;

    public float time = 1f;

    void LateUpdate()
    {
        if (state == oldState) return;
        if (state == 0)
        {//FormMainMenu
            oldState = 0;
            transform.DOLocalMove(Pos1, time);
            transform.DOLocalRotate(Rot1, time);
        }
        else if (state == 1)
        {//FormGamePlay
            oldState = 1;
            transform.DOLocalMove(Pos2, time);
            transform.DOLocalRotate(Rot2, time);
        }
        else if (state == 2)
        {//FormGamePlay
            oldState = 2;
            transform.DOLocalMove(Pos3, time);
            transform.DOLocalRotate(Rot3, time);
        }
    }
}
