﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleInput : MonoBehaviour
{
    #region Inspector Variables
    public InputManager manager;
    #endregion

    #region Member Variables
    bool isActive = false;
    #endregion

    #region Unity Methods
    private void Start()
    {
        //Debug.Log("Input start");
        manager.OnTouch += HandleTouch;
        manager.OnStartDrag += HandleStartDrag;
        manager.OnDraging += HandleDraging;
        manager.OnFinishDrag += HandleFinishDrag;
        manager.OnTap += HandleTap;
        Active();
    }

    private void OnDestroy()
    {
        //Debug.Log("Input destroy");
        manager.OnTouch -= HandleTouch;
        manager.OnStartDrag -= HandleStartDrag;
        manager.OnDraging -= HandleDraging;
        manager.OnFinishDrag -= HandleFinishDrag;
        manager.OnTap -= HandleTap;
    }
    #endregion

    #region Public Methods
    public void Active()
    {
        isActive = true;
    }

    public void Deactive()
    {
        isActive = false;
        if (drag)
        {
            drag = false;
        }
    }
    #endregion

    #region Private Methods
    /*
     * Kiểm tra ấn vào vật thể (3D) được gắn tag "Element" và layer "Element"
     */
    private void HandleTouch(Vector3 pos)
    {        
        if (!isActive)
            return;
        var ray = Camera.main.ScreenPointToRay(pos);
        RaycastHit hit = new RaycastHit();
        Physics.Raycast(ray, out hit, LayerMask.GetMask("Element"));
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Element"))
            {
                IElement element = hit.collider.gameObject.GetComponent<IElement>();
                if (element != null)
                    element.Press();
            }
        }
    }

    private bool drag = false;
    private void HandleStartDrag()
    {
        if (!isActive)
            return;
        //Debug.Log("Start drag");
    }

    Vector3 lastPos = Vector3.zero;
    private void HandleDraging(Vector3 pos)
    {
        if (!isActive)
            return;
        //Debug.Log("Dragging");

    }

    private void HandleFinishDrag(Vector3 pos)
    {
        if (!isActive)
            return;
        if (drag)
        {
            //Debug.Log("Finish drag");
            drag = false;
        }

    }

    private void HandleTap(Vector3 pos)
    {

    }


    #endregion
}
