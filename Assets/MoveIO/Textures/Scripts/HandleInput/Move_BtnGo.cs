﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_BtnGo : Element
{
    public Move_SelectWeapon move_SelectWeapon;

    public override void Action()
    {
        if (move_SelectWeapon.weaponIndx == -1) return;
        if (move_SelectWeapon.weaponIndx == 2) MaxManager.Ins.ShowRewardedAd("move_select_weapon", AdsDone);
        else AdsDone();
    }

    public void AdsDone()
    {
        Move_GameManager.Ins.GetReadyGo();
    }
}
