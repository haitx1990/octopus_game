﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element : MonoBehaviour, IElement
{
    #region Member Variables
    [HideInInspector] public Transform mTrans;
    public bool isInteractive;
    #endregion

    #region Unity Methods
    private void Awake()
    {
        mTrans = GetComponent<Transform>();
    }
    private void Update()
    {

    }
    #endregion

    #region Public Methods
    public virtual void Action()
    {
        _manager.ChooseWeapon(_idWeapon, _slot);
    }
    private int _idWeapon;
    private int _slot;
    private Move_SelectWeapon _manager;
    public virtual void Init(int slot, int idWeapon, Move_SelectWeapon manager)
    {
        _idWeapon = idWeapon;
        _manager = manager;
        _slot = slot;
    }
    public virtual bool CheckInteractive()
    {
        return isInteractive;
    }
    #endregion


    public void Press()
    {
        Action();
    }
}
