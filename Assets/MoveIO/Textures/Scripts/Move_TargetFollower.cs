﻿using UnityEngine;

public class Move_TargetFollower : MonoBehaviour
{
    public SpriteRenderer mat1, mat2;

    private Transform _player;
    private Transform _target;
    private Move_EnemyController _e;

    private bool isFollowing;
    public void Init(Move_EnemyController target, Material mat)
    {
        mat1.color = mat.color;
        mat2.color = mat.color;
        _target = target.transform;
        _e = target;
        _player = Move_PlayerController.Ins.transform;
        isFollowing = true;
    }

    private void Update()
    {
        if (!isFollowing) return;
        if (_target == null || _e._isDead) Destroy(gameObject);
        var dir = _player.position - _target.position;
        dir = -dir.normalized;
        transform.position = _player.position + dir * 3;
        transform.LookAt(_target);
    }
}
