﻿// © 2015 Mario Lelas
using UnityEngine;

/*
    Script for testing demo on andriod 
*/

namespace MLSpace
{
    public class AndroidTest : MonoBehaviour
    {
        private RagdollManagerGen m_Ragdoll;
        private RagdollManagerHum m_Ragdoll2;

        public float m_HitForce = 16.0f;

        private bool m_DoubleClick = false;
        private float m_DoubleClickMaxTime = 0.2f;
        private float m_DoubleClickCurTime = 0.0f;
        private int clickCount = 0;
        private Ray currentRay;


        // Use this for initialization
        void Start()
        {
            m_Ragdoll = GetComponent<RagdollManagerGen>();
            m_Ragdoll2 = GetComponent<RagdollManagerHum>();

            Animator anim = GetComponent<Animator>();

            //// example of disabling character capsule
            //// at entering ragdoll
            //// and enabling it after ragdoll ends
            //CapsuleCollider capsule = GetComponent<CapsuleCollider>();
            //Rigidbody rigidbody = GetComponent<Rigidbody>();
            if (m_Ragdoll!= null)
            {
                m_Ragdoll.OnHit = () =>
                {
                    //capsule.enabled = false;
                    //rigidbody.isKinematic = true;
                    anim.applyRootMotion = false;
                };
                m_Ragdoll.LastEvent = () =>
                {
                    //capsule.enabled = true;
                    //rigidbody.isKinematic = false;
                    anim.applyRootMotion = true;
                };


                m_Ragdoll.ragdollEventTime = 3.0f;
                m_Ragdoll.OnTimeEnd = () =>
                {
                    m_Ragdoll.blendToMecanim();
                };

            }
            else
            {
                m_Ragdoll2.OnHit = () =>
                {
                    //capsule.enabled = false;
                    //rigidbody.isKinematic = true;
                    anim.applyRootMotion = false;
                };
                m_Ragdoll2.LastEvent = () =>
                {
                    //capsule.enabled = true;
                    //rigidbody.isKinematic = false;
                    anim.applyRootMotion = true;
                };


                m_Ragdoll2.ragdollEventTime = 3.0f;
                m_Ragdoll2.OnTimeEnd = () =>
                {
                    m_Ragdoll2.blendToMecanim();
                };
            }

            
        }

        // Update is called once per frame
        void Update()
        {

            bool clicked = Input.GetMouseButtonDown(0);
            if (clicked)
            {
                currentRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                m_DoubleClickCurTime = 0.0f;
            }


            m_DoubleClickCurTime += Time.deltaTime;
            if (m_DoubleClickCurTime <= m_DoubleClickMaxTime)
            {
                if (clicked) clickCount++;
                if (clickCount > 1)
                    m_DoubleClick = true;
            }
            else
            {
                if (clickCount > 0)
                {
                    if (m_DoubleClick)
                    {
                        doRagdoll();
                    }
                    else
                    {
                        doHitReaction();
                    }
                    m_DoubleClickCurTime = 0.0f;
                }
                clickCount = 0;
                m_DoubleClick = false;
            }
        }

        private void doHitReaction()
        {
            int mask = LayerMask.GetMask("ColliderLayer", "ColliderInactiveLayer");
            RaycastHit rhit;
            if (Physics.Raycast(currentRay, out rhit, 120.0f, mask))
            {
                BodyColliderScript bcs = rhit.collider.GetComponent<BodyColliderScript>();
                int[] parts = new int[] { bcs.index };
                if (m_Ragdoll != null)
                {
                    m_Ragdoll.startHitReaction(parts, currentRay.direction * m_HitForce);
                }
                else
                {
                    m_Ragdoll2.startHitReaction(parts, currentRay.direction * m_HitForce);
                }
            }
        }

        private void doRagdoll()
        {
            int mask = LayerMask.GetMask( "ColliderLayer", "ColliderInactiveLayer");
            RaycastHit rhit;
            if (Physics.Raycast(currentRay, out rhit, 120.0f, mask))
            {
                BodyColliderScript bcs = rhit.collider.GetComponent<BodyColliderScript>();
                int[] parts = new int[] { bcs.index };
                if (m_Ragdoll != null)
                {
                    m_Ragdoll.startRagdoll(parts, currentRay.direction * m_HitForce);
                }
                else
                {
                    m_Ragdoll2.startRagdoll(parts, currentRay.direction * m_HitForce);
                }
            }
        }
    } 
}
