﻿using UnityEngine;
using MLSpace;

public class Squid_GEN_TEST : MonoBehaviour
{

    public Transform OrientTransform;

    private RagdollManager m_Ragdoll;
    private Animator m_Animator;



    // Use this for initialization
    void Start()
    {
        m_Ragdoll = GetComponent<RagdollManager>();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown (KeyCode.F))
        {
            m_Ragdoll.startRagdoll();
        }
        if(Input.GetKeyDown (KeyCode.G))
        {
            m_Ragdoll.blendToMecanim();
        }
    }
}
