﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GlassGame3
{
    public class ManageGlassObj : MonoBehaviour
    {
        // Start is called before the first frame update
        GameObject glass1, glass2;
        public Material matGlass, matFlor;
        public GameObject availableGlass, nextGround;
        public bool isPerfect;
        Material hintMat;
        public bool isGlass1;
        public bool isTrigger = false;
        
        void Start()
        {
            
            isPerfect = true;
            glass1 = transform.GetChild(0).gameObject;
            glass2 = transform.GetChild(1).gameObject;
            hintMat = Resources.Load<Material>("itemStrengmat");
            isGlass1 = Random.Range(1, 10) >= 5;
            if (isGlass1)
            {
                glass1.GetComponent<MeshRenderer>().material = matGlass;
                glass2.GetComponent<MeshRenderer>().material = matFlor;
              glass1.GetComponent<BoxCollider>().isTrigger = true;
            }
            else
            {

                glass1.GetComponent<MeshRenderer>().material = matFlor;
                glass2.GetComponent<MeshRenderer>().material = matGlass;
              glass2.GetComponent<BoxCollider>().isTrigger = true;
            }
            GlassGameHandle.OnPlayerReviveComplete.AddListener(OnPlayerRevive);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnBroke()
        {
            isPerfect = false;
            /*if(transform.GetChild(0).gameObject.activeInHierarchy)
            {
                this.availableGlass = transform.GetChild(0).gameObject;

            }
            else
            {
                this.availableGlass = transform.GetChild(1).gameObject;
            }*/
            // this.availableGlass.GetComponent<MeshRenderer>().material = testMat;
            /* if(isGlass1)
              {

              }*/


        }
        public void Hint()
        {
            if (isGlass1)
            {
                StartCoroutine(ActiveHint(glass2.gameObject));
            }
            else
            {
                StartCoroutine(ActiveHint(glass1.gameObject));
            }
        }

        public void OnPlayerRevive()
        {
            if(!isPerfect)
            {
                if (isGlass1)
                {
                    glass1.SetActive(true);
                }
                else
                {
                    glass2.SetActive(true);
                }
            }
        }

        IEnumerator ActiveHint(GameObject hintObj)
        {
            yield return null;
            hintObj.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = hintMat;
            for(int i=0;i<10;i++)
            {
                yield return new WaitForSeconds(0.2f);
                if(i%2!=0)
                {
                    hintObj.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = matGlass;
                }
                else
                {
                    hintObj.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = hintMat;
                }
            }
            hintObj.transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = matGlass;

        }





    }
}
