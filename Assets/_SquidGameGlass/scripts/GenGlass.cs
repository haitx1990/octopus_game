﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GlassGame3
{
    public class GenGlass : Singleton<GenGlass>
    {
        // Start is called before the first frame update
        public GameObject GlassObj, GoalObj;
        public int numOfG;
        public float distancePerG,delayPerBroke;
        public Transform groundStart;
        Vector3 groundGenPos, nextGenVector;
        GameObject beforeNextGround;
        public int isTrigger;
        GameObject fxBorken;


        void Start()
        {
            fxBorken = Resources.Load<GameObject>("FX_BrokenGlass");
            GlassObj = Resources.Load<GameObject>("GlassObjectNew");
            GoalObj = Resources.Load<GameObject>("GoalObject2");
            nextGenVector = new Vector3(0, 0, distancePerG);
            groundGenPos = groundStart.position + nextGenVector;
            GlassGameHandle.OnGoal.AddListener(this.OnPlayerGoal);
            this.GenGround();

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void GenGround()
        {
            //gen unit
            for (int i = 0; i < numOfG; i++)
            {


                // GameObject objGround = new GameObject();
                GameObject objGround = Instantiate(GlassObj, groundGenPos, Quaternion.identity);
                
                groundGenPos += nextGenVector;
                if (i == 0)
                {
                    groundStart.GetComponent<ManageGlassObj>().nextGround = objGround;
                    groundStart.transform.parent = transform;
                }
                else
                {
                    beforeNextGround.GetComponent<ManageGlassObj>().nextGround = objGround;
                }
                beforeNextGround = objGround;
                if(i==isTrigger)
                {
                    //objGround.GetComponent<ManageGlassObj>().isTrigger = true;
                }
                objGround.transform.parent = transform;


            }
            groundGenPos -= nextGenVector;
           groundGenPos += nextGenVector * 0.5f;
            //gen goal
            GameObject objGoal = Instantiate(GoalObj, groundGenPos, Quaternion.identity);
            beforeNextGround.GetComponent<ManageGlassObj>().nextGround = objGoal;

        }
        public void SetupGround()
        {

        }
        public void OnPlayerGoal()
        {
            StartCoroutine(AtiveBlowBridge());
        }
        IEnumerator AtiveBlowBridge()//blow a
        {
            yield return new WaitForSeconds(1.3f);
            gameObject.GetComponent<SoundObject>().PlaySound();
            for (int i=0;i<transform.childCount;i++)
            {
                yield return new WaitForSeconds(delayPerBroke);
                GameObject fxBrokenGlass = Instantiate(fxBorken, transform.GetChild(i).position, Quaternion.identity);
                Destroy(fxBrokenGlass, 1.5f);
                //transform.GetChild(i).gameObject.SetActive(false);
                transform.GetChild(i).GetChild(0).gameObject.SetActive(false);
                transform.GetChild(i).GetChild(1).gameObject.SetActive(false);


            }
        }
    }
}
