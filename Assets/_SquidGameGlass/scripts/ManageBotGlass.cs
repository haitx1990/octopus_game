﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GlassGame3
{
    public class ManageBotGlass : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject nextGlass;
        public float ForceMulti, unitMoveSpeed;
        public float baseRate = 1;
        public float jumpRate;
        float delayJump;
        Rigidbody rigBody;
        Vector3 vectorJump, startPost;
        public float jumpForce;
        bool canJump = false;
        Transform startPoint, lastSave;
        Animator BotAnimator;
        Material testMat;
        bool isGoal, canFX, isDeath,isGameStart;
        GameObject fxFail,fxBrokenGlass;
        float originSpeed;



        void Start()
        {
            originSpeed = this.unitMoveSpeed;
            isGameStart = false;
            startPost = transform.position;
            isDeath = false;
            canFX = true;
            isGoal = false;
            fxFail = Resources.Load<GameObject>("fxSoul");
            fxBrokenGlass= Resources.Load<GameObject>("BrokenGlass");
            nextGlass = GenGlass.Instance.groundStart.gameObject;
            BotAnimator = gameObject.GetComponentInChildren<Animator>();
            rigBody = gameObject.GetComponent<Rigidbody>();
            canJump = false;
            GameObject[] startPoints = GameObject.FindGameObjectsWithTag("startPost");
            if (Vector3.Distance(transform.position, startPoints[0].transform.position) < Vector3.Distance(transform.position, startPoints[1].transform.position))
            {
                startPoint = startPoints[0].transform;
            }
            else
            {
                startPoint = startPoints[1].transform;
            }

            if (Vector3.Distance(transform.position, startPoint.transform.position) > 0.5f)
            {
                //this.BotAnimator.SetFloat("speed", unitMoveSpeed);
            }
            testMat = Resources.Load<Material>("itemStrengmat");
            GlassGameHandle.OnGlassGameStart.AddListener(OnGameStart);
            GlassGameHandle.OnPlayerReviveComplete.AddListener(OnPlayerRivive);
            GlassGameHandle.OnGoal.AddListener(OnPlayerGotGoal);


        }

        // Update is called once per frame
        void Update()
        {
            if (!isDeath&& isGameStart)
            {
                if (canJump)
                {
                    this.MakeJump();
                }
                this.moveToStartPoint();
            }
        }
        public void OnPlayerGotGoal()
        {
           // Debug.LogError("vao day r");
            this.BotAnimator.Play("Idle");
            isGameStart = false;
        }
        public void MakeJump()
        {
            jumpRate = baseRate / (unitMoveSpeed / 100);
            delayJump += Time.deltaTime;
            if (delayJump > jumpRate)
            {
                //this.CalculateVectorJump();
                // this.Jump();
                int a = Random.Range(0, 100) > 50 ? 0 : 1;
                // StartCoroutine(JumpToDirect(nextGlass.transform.GetChild(a).transform.position));
                this.BotAnimator.SetTrigger("jump");
                StartCoroutine(JumpToDirect(this.GetNextGround()));
                delayJump = 0;
            }
        }
        public void Jump()
        {
            BotAnimator.SetTrigger("jump");
            BotAnimator.SetFloat("speed", 0);
            //Vector3 vectorJump = (transform.forward + new Vector3(0f, jumpForce, 0f)) * ForceMulti;
            Vector3 vectorJump = (transform.forward + this.vectorJump) * ForceMulti;
            rigBody.AddForceAtPosition(vectorJump, transform.position, ForceMode.Force);
        }
        public void CalculateVectorJump()
        {
            this.vectorJump = Vector3.up * jumpForce;
        }
        public void moveToStartPoint()
        {
            if (!canJump && !isGoal)
            {
                if (Vector3.Distance(transform.position, startPoint.transform.position) > 0.5f)
                {
                    Vector3 dir = startPoint.transform.position - transform.position;
                    dir.Normalize();
                    transform.position += dir * unitMoveSpeed / 15 * Time.deltaTime;
                }
                else
                {
                    BotAnimator.SetFloat("speed", 0);
                    unitMoveSpeed *= Random.Range(0.15f, 0.8f);
                    canJump = true;
                }
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.name.Contains("Glass"))
            {


                // other.GetComponent<MeshRenderer>().material = testMat;
                if(other.transform.parent.GetComponent<ManageGlassObj>()!=null )
                this.nextGlass = other.transform.parent.GetComponent<ManageGlassObj>().nextGround;
                if (other.GetComponent<BoxCollider>().isTrigger)
                {

                    if (canFX)
                    {
                      GameObject fxF=  Instantiate(fxFail, transform.position, Quaternion.identity);
                        Destroy(fxF, 1.5f);
                        if (!other.name.Contains("triggerFall"))
                        {
                            GameObject fxGlass = Instantiate(this.fxBrokenGlass, transform.position, transform.rotation);
                            Destroy(fxGlass, 3.5f);
                        }
                        BotAnimator.SetTrigger("fall");
//canJump = false;
                        isDeath = true;
                        canFX = false;
                        GlassGameHandle.OnGlassBroke.Invoke(other.transform);
                        other.transform.parent.GetComponent<ManageGlassObj>().OnBroke();
                        other.gameObject.SetActive(false);
                        StartCoroutine(ActiveRevive());
                    }

                }
                else
                {
                    lastSave = other.transform;

                }


            }
            if (other.name.Contains("triggerFall"))
            {

                if (canFX)
                {
                  GameObject fxF=  Instantiate(fxFail, transform.position, Quaternion.identity);
                    Destroy(fxF, 1.5f);
                    BotAnimator.SetTrigger("fall");
                    //rigBody.mass += 100;
                    isDeath = true;
                    canFX = false;

                    StartCoroutine(ActiveRevive());
                }
            }


        }
        public Vector3 GetNextGround()
        {
            if (!nextGlass.name.Contains("Goal"))
            {
                if (!nextGlass.GetComponent<ManageGlassObj>().isPerfect)
                {
                    //return nextGlass.GetComponent<ManageGlassObj>().availableGlass.transform.position;
                    if (nextGlass.GetComponent<ManageGlassObj>().isGlass1)
                    {
                        return ConverToRandomAround(nextGlass.transform.GetChild(1).transform.position);
                    }
                    else
                    {
                        return ConverToRandomAround(nextGlass.transform.GetChild(0).transform.position);
                    }
                }
                else
                {
                    int a = Random.Range(0, 100) > 50 ? 0 : 1;
                    return ConverToRandomAround(nextGlass.transform.GetChild(a).transform.position);

                }
            }
            else
            {
                isGoal = true;
                canJump = false;
                BotAnimator.SetTrigger("goal");
                return ConverToRandomAround(nextGlass.transform.GetChild(0).transform.position);
            }



        }
        public Vector3 ConverToRandomAround(Vector3 origin)
        {
            return new Vector3(origin.x * Random.Range(0.8f, 1.15f), origin.y, origin.z * Random.Range(0.98f, 1.03f));
        }

        public float jumpStartVelocityY, jumpDuration;
        public bool jumping;
        IEnumerator JumpToDirect(Vector3 target)
        {
            jumping = true;
            Vector3 startPoint = transform.position;
            Vector3 targetPoint = target;
            float time = 0;
            float jumpProgress = 0;
            float velocityY = jumpStartVelocityY;
            float height = startPoint.y;

            transform.rotation = Quaternion.LookRotation(target-transform.position);
            while (jumping)
            {
                jumpProgress = time / jumpDuration;

                if (jumpProgress > 1)
                {
                    jumping = false;
                    jumpProgress = 1;
                }

                Vector3 currentPos = Vector3.Lerp(startPoint, targetPoint, jumpProgress);
                currentPos.y = height;
                transform.position = currentPos;

                //Wait until next frame.
                yield return null;

                height += velocityY * Time.deltaTime;
                velocityY += Time.deltaTime * Physics.gravity.y;
                time += Time.deltaTime;
            }

            transform.position = targetPoint;
            transform.rotation = Quaternion.LookRotation(Vector3.forward);

            yield break;
        }
        IEnumerator ActiveRevive()
        {
            //rigBody.mass -= 100;
            yield return new WaitForSeconds(3.5f*Random.Range(1.1f,2.5f));
            isDeath = false;
            canFX = true;
            nextGlass = GenGlass.Instance.groundStart.gameObject;
            canJump = false;
            //canJump = false;
            BotAnimator.Play("Idle");
            BotAnimator.SetFloat("speed",5);
            this.unitMoveSpeed = originSpeed;


            /*  if (lastSave != null)
              {
                  transform.position = lastSave.position;
              }
              else
              {
                  transform.position = startPoint.position;
              }*/
            transform.position = startPost;
            
            //this.nextGlass = GenGlass.Instance.groundStart.gameObject;
            yield return null;
        }

        public void OnGameStart()
        {
            isGameStart = true;
            this.BotAnimator.SetFloat("speed", unitMoveSpeed);
        }
        public void OnPlayerRivive()
        {
            isDeath = false;
            canFX = true;
            nextGlass = GenGlass.Instance.groundStart.gameObject;
            canJump = false;
            BotAnimator.Play("Idle");
            BotAnimator.SetFloat("speed", 5);
            this.unitMoveSpeed = originSpeed;
            transform.position = startPost;
        }
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.name.Contains("Glass"))
            {
                if (collision.transform.parent.GetComponent<ManageGlassObj>())
                {
                    if (collision.transform.parent.GetComponent<ManageGlassObj>().isTrigger)
                    {

                        if (GlassGameHandle.Instance.canTurnOffLight)
                        {
                            GlassGameHandle.OnTriggerLight.Invoke();
                        }
                    }
                }
            }
            if (collision.gameObject.name.Contains("sannha") || collision.gameObject.name.Contains("fallGround"))
            {
                BotAnimator.SetTrigger("fallEnd");
                //playerAudio.Stop();
                ManageSoundGlassGame.Instance.PlaySoundOnFloor();
            }
        }

    }
}
