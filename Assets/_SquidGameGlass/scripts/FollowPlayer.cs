﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlassGame3
{
    public class FollowPlayer : MonoBehaviour
    {
        public Transform player;
        public float rotateSpeed,cdRotate;
        public bool canRotate;
        Quaternion originQuater;
        Vector3 originPos;
        float originCD;

        // Start is called before the first frame update
        void Start()
        {
            canRotate = false;
            GlassGameHandle.OnPlayerFall.AddListener(OnPlayerFall);
            GlassGameHandle.OnPlayerReviveComplete.AddListener(OnPlayerRevive);
            originQuater = transform.rotation;
            originPos = transform.position;
            originCD = cdRotate;
        }

        // Update is called once per frame
        void Update()
        {
            transform.position = player.position;
            if(canRotate)
            {
                cdRotate -= Time.deltaTime;
                if(cdRotate<=0)
                {
                    canRotate = false;
                }
                transform.Rotate(transform.rotation.x, transform.rotation.y + Time.deltaTime * rotateSpeed, transform.rotation.z);
            }
        }
        public void OnPlayerFall()
        {
            canRotate = true;
        }
        public void OnPlayerRevive()
        {
            transform.rotation = originQuater;
            transform.position = originPos;
            canRotate = false;
            cdRotate = originCD;
        }

    }
}
