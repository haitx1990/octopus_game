﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlassGame3
{
    public class ManageEnviGlass : Singleton<ManageEnviGlass>
    {
        // Start is called before the first frame update
        public GameObject border;
        void Start()
        {
            GlassGame3.GlassGameHandle.OnGlassGameStart.AddListener(DeactiveBorder);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void DeactiveBorder()
        {
            border.SetActive(false);
        }
    }
}
