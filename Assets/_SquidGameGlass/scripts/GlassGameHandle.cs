﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
using UnityEngine.UI;


namespace GlassGame3
{
    public class GlassGameHandle : Singleton<GlassGameHandle>
    {
        // Start is called before the first frame update
        public float gravity;
        public bool canStartGame,canTurnOffLight;
        public bool isGoal = false;
        GameObject cameraGlow;
        public bool isTouching,canCallRevive;
        public GameObject fakeJoy;
        void Start()
        {
            canCallRevive = true;
            canStartGame = false;
            canTurnOffLight = true;
            Debug.LogError(Physics.gravity);
            Physics.gravity = new Vector3(0, gravity, 0);
           // Debug.LogError(Physics.gravity);
            OnTriggerLight.AddListener(TurnOffLight);
            GlassGameHandle.OnGoal.AddListener(this.OnPlayerGotGoal);
            GlassGameHandle.OnPlayerReviveComplete.AddListener(this.OnPlayerRevive);

        }
        public class OnGlassGameStartHandler : UnityEvent { }
        public static OnGlassGameStartHandler OnGlassGameStart = new OnGlassGameStartHandler();
        public class OnGlassBrokeHandle : UnityEvent<Transform> { }
        public static OnGlassBrokeHandle OnGlassBroke = new OnGlassBrokeHandle();

        public class OnGoalHandle : UnityEvent { }
        public static OnGoalHandle OnGoal = new OnGoalHandle();

        public class OnPlayerFallHandle : UnityEvent { }
        public static OnPlayerFallHandle OnPlayerFall = new OnPlayerFallHandle();

        public class OnOverLimitTimeStayHandle : UnityEvent { }
        public static OnOverLimitTimeStayHandle OnOverLimitTimeStay = new OnOverLimitTimeStayHandle();

        public class OnGameOverHandle : UnityEvent { }
        public static OnGameOverHandle OnGameOver = new OnGameOverHandle();

        public class OnPlayerReviveHandle : UnityEvent { }
        public static OnPlayerReviveHandle OnPlayerReviveComplete = new OnPlayerReviveHandle();

        public class OnTriggerLightActiveHandle : UnityEvent { }
        public static OnTriggerLightActiveHandle OnTriggerLight = new OnTriggerLightActiveHandle();

        public class On1stTouchHandle : UnityEvent { }
        public static On1stTouchHandle On1stTouch= new On1stTouchHandle();

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }
        // Update is called once per frame
        void Update()
        {
            if(this.isTouching)
            {
                fakeJoy.gameObject.SetActive(false);
            }
            else
            {
                fakeJoy.gameObject.SetActive(true);
            }
        }
        public IEnumerator DelayStartGame()
        {
            yield return new WaitForSeconds(1.0f);
            this.canStartGame = true;

            OnGlassGameStart.Invoke();
        }
        public void StartGame()
        {
            StartCoroutine(DelayStartGame());
            this.isGoal = false;
            GlassGameHandle.OnGoal.AddListener(this.OnPlayerGotGoal);
        }
        private void OnDestroy()
        {
            OnGlassGameStart.RemoveAllListeners();
            OnGlassBroke.RemoveAllListeners();
            OnGoal.RemoveAllListeners();
            OnPlayerFall.RemoveAllListeners();
            OnOverLimitTimeStay.RemoveAllListeners();
            OnGameOver.RemoveAllListeners();
            OnPlayerReviveComplete.RemoveAllListeners();
            OnTriggerLight.RemoveAllListeners();
            On1stTouch.RemoveAllListeners();
        }
        public void TurnOffLight()
        {
            GameObject directLight = GameObject.FindGameObjectWithTag("directLight");
            directLight.GetComponent<Light>().intensity = 0;
            GameObject[] lights = GameObject.FindGameObjectsWithTag("light");
            for (int i = 0; i < lights.Length; i++)
            {
                if (lights[i].activeInHierarchy)
                {
                    lights[i].SetActive(false);
                }
            }
            canTurnOffLight = false;
        }
        public void OnPlayerGotGoal()
        {
           cameraGlow = GameObject.FindGameObjectWithTag("cameraGlow");
            isGoal = true;
            if (cameraGlow && cameraGlow.gameObject.activeInHierarchy)
            {
                cameraGlow.gameObject.SetActive(false);
            }
        }
        public void OnPlayerRevive()
        {
            canCallRevive = true;
        }
        
    }
}
