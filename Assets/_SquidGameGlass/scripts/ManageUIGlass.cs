﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
namespace GlassGame3
{
    public class ManageUIGlass : Singleton<ManageUIGlass>
    {
        public enum PanelType
        {
            REVIVE,
            WIN,
            LOST
        }

        // Start is called before the first frame update
        public float countdownToStart,globalCountDown;
        TextMeshProUGUI tmp_textCountdown,tmp_textCountDownRevive;
        public Transform panelWin, panelRevive, panelLost;
        Button btReviveByCoin, btReviveByAds, btContinueFromWin, btContinueFromLost,btNoThank;
       public bool canCd,canGlobalCD;
        public float delayEach,addForWin;
        float originGlobalCountDown;
        public float cdReviveClaim;
        bool canCdRiviveClaim;
        float originCdReviveClaim;
        public GameObject btStart;
       

        void Start()
        {
            originGlobalCountDown = globalCountDown;
            canCd = true;
            btReviveByAds = TransformUtils.GetRecursive<Button>(panelRevive, "ButtonFreeAds");
           // btReviveByCoin = TransformUtils.GetRecursive<Button>(panelRevive, "ButtonCoin");
          //tmp_textCountDownRevive= TransformUtils.GetRecursive<TextMeshProUGUI>(panelRevive, "tmpReviveCountdown");
            btContinueFromWin= TransformUtils.GetRecursive<Button>(panelWin, "ButtonContinue");
            btContinueFromLost= TransformUtils.GetRecursive<Button>(panelLost, "ButtonContinuefromLost");
            btNoThank= TransformUtils.GetRecursive<Button>(panelRevive, "ButtonNoThank");
           
            this.tmp_textCountdown = TransformUtils.GetRecursive<TextMeshProUGUI>(transform, "tmp_countDown");
           // btReviveByCoin.onClick.AddListener(this.OnClickReviveByCoin);
            btReviveByAds.onClick.AddListener(this.OnClickReviveByAds);
            btContinueFromLost.onClick.AddListener(OnClickContinueFromLost);
            btContinueFromWin.onClick.AddListener(OnClickContinueFromWin);
            btNoThank.onClick.AddListener(OnClickContinueFromLost);
            GlassGameHandle.OnPlayerFall.AddListener(this.OnPlayerFall);
            GlassGameHandle.OnGoal.AddListener(this.OnGoal);
            GlassGameHandle.OnGameOver.AddListener(this.OnLost);
            GlassGameHandle.On1stTouch.AddListener(this.On1stTouch);
        }

        // Update is called once per frame
        void Update()
        {
          /*  if (canCd)
            {
                this.CountDownToStart();
            }*/
            if(canGlobalCD)
            {
                this.CountDownGlobal();
            }
            if(canCdRiviveClaim)
            {

            }

        }
        public void CountDownReviveClaim()
        {
            if(cdReviveClaim>0)
            {
                cdReviveClaim -= Time.deltaTime;
                this.DisplayCountDownRevive(cdReviveClaim);

            }
        }
        public void DisplayCountDownRevive(float _currentTime)
        {
            _currentTime += 1;
            float seconds = Mathf.FloorToInt(_currentTime % 60);
            //float minutes = Mathf.FloorToInt(_currentTime / 60);
            tmp_textCountDownRevive.text = string.Format("{0:0}", seconds);
        }
        public void CountDownToStart()
        {
            if (countdownToStart > 0)
            {
                countdownToStart -= Time.deltaTime;
                this.DisplayCountDown(countdownToStart);
            }
            else
            {
                canCd = false;
                tmp_textCountdown.text = "GO!";
                canGlobalCD = true;
                GlassGameHandle.Instance.StartGame();
                //Destroy(tmp_textCountdown, 0.5f);

            }
        }
        public void StartGame()
        {
            canGlobalCD = true;
            ManageSoundGlassGame.Instance.PlaySoundStart();
            GlassGameHandle.Instance.StartGame();
        }

        public void CountDownGlobal()
        {
            if (globalCountDown > 0)
            {
                globalCountDown -= Time.deltaTime;
                this.DisplayGlobalCountDown(globalCountDown);
            }
            else
            {
                canGlobalCD = false;
              
                tmp_textCountdown.text = "Game Over";
                if(!GlassGameHandle.Instance.isGoal)
                GlassGameHandle.OnGameOver.Invoke();//tong thoi gian man choi ket thuc
                Destroy(tmp_textCountdown, 0.5f);

            }
        }
        public void DisplayCountDown(float _currentTime)
        {
            _currentTime += 1;
            float seconds = Mathf.FloorToInt(_currentTime % 60);
            //float minutes = Mathf.FloorToInt(_currentTime / 60);
            tmp_textCountdown.text = string.Format("{0:0}",seconds);
        }
        public void DisplayGlobalCountDown(float _currentTime)
        {
            _currentTime += 1;
            float seconds = Mathf.FloorToInt(_currentTime % 60);
            float minutes = Mathf.FloorToInt(_currentTime / 60);
            tmp_textCountdown.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        IEnumerator ActivePanel(PanelType pn)
        {
            yield return new WaitForSeconds(delayEach);
            switch(pn)
            {
                case PanelType.REVIVE:
                    yield return new WaitForSeconds(1.5f);
                    
                    //panelRevive.gameObject.SetActive(true);
                   // Debug.LogError("revive");
                    SceneManager.ins.ShowPopup_Revive(this.Revive);
                    SceneManager.ins.isPause = true;
                    break;
                case PanelType.WIN:
                    yield return new WaitForSeconds(addForWin);
                    //panelWin.gameObject.SetActive(true);
                    SceneManager.ins.ShowPopup_EndGame(true);

                    break;
                case PanelType.LOST:
                    //panelLost.gameObject.SetActive(true);
                    SceneManager.ins.ShowPopup_EndGame(false);

                    break;
                default:
                    break;
            }
        }
        public void OnPlayerFall()
        {
            StartCoroutine(ActivePanel(PanelType.REVIVE));

        }
        public void OnGoal()
        {
            StartCoroutine(ActivePanel(PanelType.WIN));
            canGlobalCD = false;
        }
        public void OnLost()
        {
            StartCoroutine(ActivePanel(PanelType.LOST));

        }
        public void Revive()
        {
            // Time.timeScale = 1;
            SceneManager.ins.isPause=false;
            this.canGlobalCD = true;
            ManagePlayerGlass.Instance.Revive();
            this.globalCountDown = originGlobalCountDown;
            panelRevive.gameObject.SetActive(false);
        }
        public void OnClickReviveByCoin()
        {
            this.Revive();
            panelRevive.gameObject.SetActive(false);

        }
        public void OnClickReviveByAds()
        {
            // Time.timeScale = 0;
            this.canGlobalCD = false;
          /*  MaxManager.Ins.ShowRewardedAd("GlassGameRevive", () =>
             {
                 this.Revive();
             });*/
            //this.Revive();
            
        }
        public void OnClickContinueFromLost()
        {
           // SceneManager.LoadScene(SceneManager.GetActiveScene().name);
           if(panelRevive.gameObject.activeInHierarchy)
            {
                SceneManager.ins.ShowPopup_EndGame(false);
                panelRevive.gameObject.SetActive(false);
            }
           if(panelLost.gameObject.activeInHierarchy)
            {
                SceneManager.ins.ShowPopup_EndGame(false);
                panelLost.gameObject.SetActive(false);
            }
          
          
        }
        public void OnClickContinueFromWin()
        {
           // SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            panelWin.gameObject.SetActive(false);
        }
        public void On1stTouch()
        {
            if(btStart.activeInHierarchy)
            {
                btStart.SetActive(false);
                this.StartGame();
            }
        }


    }
}
