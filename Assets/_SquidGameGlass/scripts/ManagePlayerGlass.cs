﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace GlassGame3
{
    public class ManagePlayerGlass : Singleton<ManagePlayerGlass>
    {
        public Transform tran_Rotate;
        public GameObject nextGlass;
        // Start is called before the first frame update
        Animator animator;
        Rigidbody rig;
        public float forceUp;
        Joystick joyControl;
        public float moveSpeed, ratioForceMove,ratioForceUp;
        public Vector2 directJump;
        GameObject fxFail, fxHeadShot, fxRevive,fxBrokeGlass;
        bool canFX, isGameStart;
        bool isOnGround;
        Vector3 revivePost,startPost;
        float cdTime;
        public float limitTimeStay;
        public Transform headShot;
        public TextMeshProUGUI tmpLog;
        float originGlobalcd;
       public Image imgstayTime;
        //setup control like base
        Vector3 moveDirection = Vector3.zero;
        CharacterController controller;
       public Camera _camera;
        AudioSource playerAudio;
        bool canJump,isGoal=false;//just check for fall

        public Data_Sound soundFalling, soundBrokeAGlass, soundBrokeAllGlass, soundToFloor, soundToGlass;
        public bool canCallRevive;



        public bool isTestUnit=false;

        void Start()
        {
            canCallRevive = true;
            nextGlass = GenGlass.Instance.groundStart.gameObject;
            canJump = true;
           // _camera = Camera.main;
            controller = gameObject.GetComponent<CharacterController>();
           // imgstayTime = TransformUtils.GetRecursive<Image>(transform, "progressStayTime");
            startPost = transform.position;
            animator = gameObject.GetComponentInChildren<Animator>();
            rig = gameObject.GetComponent<Rigidbody>();
            joyControl = FindObjectOfType<Joystick>();
            fxFail = Resources.Load<GameObject>("fxSoul");
            fxHeadShot = Resources.Load<GameObject>("BloodSplatWide");
            fxRevive = Resources.Load<GameObject>("MagicBuffGreen");
            fxBrokeGlass= Resources.Load<GameObject>("BrokenGlass");
            canFX = true;
            revivePost = transform.position;
            isGameStart = false;
            GlassGameHandle.OnGlassGameStart.AddListener(this.OnStartGame);
            GlassGameHandle.OnOverLimitTimeStay.AddListener(GotShot);
            GlassGameHandle.OnGoal.AddListener(OnPlayerGotGoal);
            isGameStart = false;
            tran_Rotate = transform;
            playerAudio =  gameObject.GetComponentInChildren<AudioSource>();


           
        }
        public void OnPlayerGotGoal()
        {
           // Debug.LogError("vao day r");
            Transform goalPos = GameObject.FindGameObjectWithTag("goalPos").transform;
            if(Vector3.Distance(transform.position,goalPos.position)>5)
            {
                transform.position = goalPos.position;
            }
            animator.SetTrigger("goal");
            isGoal = true;
        }
        

        // Update is called once per frame
        void Update()
        {
            Collider[] grounds = Physics.OverlapSphere(transform.position, detectGround, 1);
            tmpLog.text = "Limit time stay: " + (limitTimeStay - cdTime).ToString();
            imgstayTime.fillAmount = (limitTimeStay - cdTime) / limitTimeStay;
            //check game bat dau start neu player dung cach vi tri an toan <5 vuot qua limitTimeStay
            if (isGameStart)
            {
                if (Vector3.Distance(transform.position, revivePost) < 5.0f|| Vector3.Distance(transform.position, startPost)<8)
                {
                    cdTime += Time.deltaTime;
                    if (cdTime > limitTimeStay&& canFX &&!isGoal)
                    {
                        GlassGameHandle.OnOverLimitTimeStay.Invoke();
                        canFX = false;
                    }
                }
                else
                {
                    cdTime = 0;//reset time stay
                }
            }

            if (grounds.Length > 1)
            {
                
                isOnGround = true;
            }
            else
            {
                isOnGround = false;
                if(!canSoundOnGlass)
                {
                    canSoundOnGlass = true;
                }
            }
   
            if(!isTestUnit)
            if(joyControl.Direction != Vector2.zero )
            {
                if (isOnGround)
                {

                    transform.position += transform.forward * moveSpeed * Time.deltaTime*0.8f;
                }
                
                animator.SetFloat("speed", 5);
                if (TouchRotateSingle.eulerRotation != Vector3.zero && canFX)
                {
                  
                }
                transform.rotation = Quaternion.LookRotation(new Vector3(joyControl.Direction.x,0,joyControl.Direction.y));
            }
            else
            {
                //Nếu ko tap vào màn hình -> Nhân vật sẽ đứng yên
                animator.SetFloat("speed", 0);
            }

            //animator.SetFloat("speed", 5);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if(!isTestUnit)
                this.Jump();
                else
                {
                    
                    animator.SetTrigger("jump");
                    if (this.joyControl.Direction != Vector2.zero)
                    {
                        rig.AddForce((transform.forward * ratioForceMove + Vector3.up * ratioForceUp) * forceUp * 1.3f, ForceMode.Force);
                    }
                    else
                    {
                        rig.AddForce(Vector3.up * ratioForceUp * forceUp * 1.3f, ForceMode.Force);
                    }
                }

                // Debug.LogError("vao day r");
            }
            if(isTestUnit)
            {
                
                this.UpdateTouch();
                if(IsTouching&&canFX && !isGoal)
                {
                    Vector3 _moveDirect = this.DirectControl(StartTouchPos, CurrentTouchPos, minForceDistance);
                    if (_moveDirect!=Vector3.zero)
                    animator.SetFloat("speed", moveSpeed);
                    transform.rotation = Quaternion.LookRotation(-_moveDirect);
                    rig.MovePosition(transform.position - _moveDirect * moveSpeed * Time.deltaTime*1.15f);
                }
                else
                {
                    animator.SetFloat("speed", 0);
                }
            }
           


        }
        public float detectGround;
        private void OnDrawGizmos()
        {
            Gizmos.DrawSphere(transform.position, detectGround);
        }
        public void Jump()
        {
            if (isOnGround && canJump)
            {
                
                isOnGround = false;
                if (this.joyControl.Direction != Vector2.zero)
                {
                    //  rig.AddForce((transform.forward * ratioForce + Vector3.up * ratioForce) * forceUp, ForceMode.Force);//old jump
                    rig.AddForce((transform.forward * ratioForceMove + Vector3.up * ratioForceUp) * forceUp * 1.3f, ForceMode.Force);
                    
                }
                else
                {
                    rig.AddForce(Vector3.up * ratioForceUp * forceUp*1.3f, ForceMode.Force);
                   // rig.AddForce((transform.forward * ratioForce + Vector3.up * ratioForce) * forceUp * 1.3f, ForceMode.Force);
                }
                //moveDirection.y = this.forceUp;

                animator.SetTrigger("jump");
                isOnGround = false;

            }
        }
        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("fall");
            if (!other.tag.Contains("goal") && !other.tag.Contains("bot"))
            {



                if (canFX)
                {
                    canJump = false;
                   GameObject fxF= Instantiate(fxFail, transform.position, Quaternion.identity);
                    Destroy(fxF, 1.5f);
                    if (!other.name.Contains("triggerFall"))
                    {
                        GameObject fxGlass = Instantiate(this.fxBrokeGlass, transform.position+Vector3.down*1.5f, transform.rotation);
                        Destroy(fxGlass, 3.5f);
                    }
                    animator.SetTrigger("fall");
                    // playerAudio.Play();
                    gameObject.GetComponent<SoundObject>().PlaySound(0);
                    canFX = false;
                    GlassGameHandle.OnGlassBroke.Invoke(other.transform);
                    if (other.name.Contains("Glass"))
                    {
                       // this.nextGlass = other.transform.parent.GetComponent<ManageGlassObj>().nextGround;
                        other.transform.parent.GetComponent<ManageGlassObj>().OnBroke();
                        other.gameObject.SetActive(false);
                        if (GlassGameHandle.Instance.canCallRevive)
                        {
                            GlassGameHandle.OnPlayerFall.Invoke();
                            GlassGameHandle.Instance.canCallRevive = false;
                                }
                    }
                }

                if (other.tag.Contains("triggerFall"))
                {
                    if (GlassGameHandle.Instance.canCallRevive)
                    {
                        GlassGameHandle.OnPlayerFall.Invoke();
                        GlassGameHandle.Instance.canCallRevive = false;
                    }
                    animator.SetTrigger("fall");
                    //playerAudio.Play();
                    gameObject.GetComponent<SoundObject>().PlaySound(0);
                }

            }

            if (other.tag.Contains("goal"))
            {
                GlassGameHandle.OnGoal.Invoke();
                
               // Debug.LogError("Goal Goal");
            }
        }
        bool canSoundOnGlass = true;
           
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.transform.name.Contains("Glass"))
            {
                if (collision.transform.parent.GetComponent<ManageGlassObj>())
                nextGlass = collision.transform.parent.GetComponent<ManageGlassObj>().nextGround;
                if (canSoundOnGlass)
                {

                    //  ManageSoundGlassGame.Instance.PlaySoundOnGlass();
                    gameObject.GetComponent<SoundObject>().PlaySound(4);
                    canSoundOnGlass = false;
                }
                revivePost = collision.transform.position;
                if(collision.transform.parent.GetComponent<ManageGlassObj>())
                if(collision.transform.parent.GetComponent<ManageGlassObj>().isTrigger)
                {

                    if (GlassGameHandle.Instance.canTurnOffLight)
                    {
                        GlassGameHandle.OnTriggerLight.Invoke();
                    }
                }

            }
            if (collision.gameObject.name.Contains("sannha")||collision.gameObject.name.Contains("fallGround"))
            {
                animator.SetTrigger("fallEnd");
                //playerAudio.Stop();
                //ManageSoundGlassGame.Instance.PlaySoundOnFloor();
                gameObject.GetComponent<SoundObject>().PlaySound(3);
            }
        }
        public void PlayerUseHint()
        {
            //Time.timeScale = 0;
            ManageUIGlass.Instance.canGlobalCD = false;
            MaxManager.Ins.ShowRewardedAd("GlassGameHint", () =>
            {
                this.ActiveHint();
            });

        }
        public void ActiveHint()
        {
            //Time.timeScale = 1;
            ManageUIGlass.Instance.canGlobalCD = true;
            nextGlass.gameObject.GetComponent<ManageGlassObj>().Hint();
        }
        public void FootL()
        {

        }
        public void FootR()
        {
        }
        public void Revive()
        {
            nextGlass = GenGlass.Instance.groundStart.gameObject;
            canJump = true;
            transform.position = this.startPost;
           GameObject fxR= Instantiate(fxRevive, transform.position, Quaternion.identity);
            Destroy(fxR, 1.5f);
            canFX = true;
            animator.Play("Idle");
            cdTime = 0;//reset time stay
            GlassGameHandle.OnPlayerReviveComplete.Invoke();//active player revive
        }
        IEnumerator ActiveFall()
        {
            yield return new WaitForSeconds(1.5f);
           // GlassGameHandle.OnPlayerFall.Invoke();
        }
        public void OnStartGame()
        {
            isGameStart = true;
        }
        public void GotShot()
        {
           GameObject fxH= Instantiate(fxHeadShot, headShot.position, Quaternion.identity);
            Destroy(fxH, 1.5f);
            animator.SetTrigger("gotShot");
            GlassGameHandle.OnPlayerFall.Invoke();
        }

        public void SetPlayerWon()
        {
            GlassGameHandle.OnGoal.Invoke();
        }

        public void ChangeControl()
        {
            if(isTestUnit)
            {
                isTestUnit = false;
                return;
            }
            else
            {
                isTestUnit = true;
                return;
            }

        }





        
        public Vector2 StartTouchPos = new Vector2(0.5f, 0.5f), CurrentTouchPos, TouchVector, StartTouchPosVp, CurrentTouchPosVp;
       
        public bool IsTouching;
        private void UpdateTouch()
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0) && IsTouching)
            {
                IsTouching = false;
            }

            if (IsTouching || Input.GetMouseButtonDown(0))
            {
                if (StartTouchPos == Vector2.zero)
                {
                    StartTouchPos = new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
                    CurrentTouchPosVp = new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
                }

                CurrentTouchPos = Input.mousePosition;
                CurrentTouchPosVp = new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
                TouchVector = CurrentTouchPos - StartTouchPos;
            }

            if (Input.GetMouseButtonDown(0) && IsTouching == false)
            {
                StartTouchPos = Input.mousePosition;//lấy width vì màn hình là dọc
                StartTouchPosVp = new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
                IsTouching = true;
                //VeryGolf.CoreGame.Instance.StartTouch(new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height));
            }

            IsTouching = Input.GetMouseButton(0);
#else
            if (Input.touchCount == 0 && IsTouching)
            {
                IsTouching = false;
            }

            if (IsTouching || Input.touchCount == 1)
            {
                Touch curTouch = Input.GetTouch(0);
                CurrentTouchPos = curTouch.position;
                CurrentTouchPosVp = new Vector2(curTouch.position.x / Screen.width, curTouch.position.y / Screen.height);
                TouchVector = CurrentTouchPos - StartTouchPos;
            }

            if (Input.touchCount == 1 && IsTouching == false)
            {
                Touch curTouch = Input.GetTouch(0);
                StartTouchPos = curTouch.position;//lấy width vì màn hình là dọc
                StartTouchPosVp = new Vector2(curTouch.position.x / Screen.width, curTouch.position.y / Screen.height);
                IsTouching = true;
               
            }
            IsTouching = Input.touchCount == 1;
#endif
            //setSliderActive(ballState == BallState.Touched);
        }

        float dis;
        Vector3 camForwardPrj, camRightPrj;
        private float minForceDistance = 0.03f;
        private Vector2 p1UI, p2UI, touchDir;
        public Vector3 PotentialForceDirection { get; protected set; }
        private void calculateForceFromUI(Vector2 startTouch, Vector2 currentTouch, float minD)
        {
            float thisRatio = 0;


            
           
           
            Vector2 vd = currentTouch - startTouch;
            float ratio = Screen.height / Screen.width;
            dis = new Vector2(vd.x / Screen.width, vd.y * ratio / Screen.height).magnitude;
            camForwardPrj = _camera.transform.forward;
            camForwardPrj.y = 0f;
            camForwardPrj = -camForwardPrj.normalized;
            camRightPrj = -_camera.transform.right;
            if (dis > minForceDistance)
            {
                touchDir = vd.normalized;
              

                    PotentialForceDirection = (camRightPrj * touchDir.x + camForwardPrj * touchDir.y).normalized + Vector3.up * thisRatio;
               

            }
            else
            {
                PotentialForceDirection = Vector3.zero;
            }

           
        }

        public Vector3 DirectControl(Vector2 startTouch, Vector2 currentTouch, float minD)
        {

            Vector3 _direct;
            Vector2 vd = currentTouch - startTouch;
            float ratio = Screen.height / Screen.width;
            dis = new Vector2(vd.x / Screen.width, vd.y * ratio / Screen.height).magnitude;
            camForwardPrj = _camera.transform.forward;
            camForwardPrj.y = 0f;
            camForwardPrj = -camForwardPrj.normalized;
            camRightPrj = -_camera.transform.right;
            if (dis > minForceDistance)
            {
                touchDir = vd.normalized;
                _direct = (camRightPrj * touchDir.x + camForwardPrj * touchDir.y).normalized;

            }
            else
            {
                _direct = Vector3.zero;
            }
            return _direct;


        }

    }
}
