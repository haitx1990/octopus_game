﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveBrokenGlass : MonoBehaviour
{
    // Start is called before the first frame update
    float maxY;

    void Start()
    {
        maxY = transform.localPosition.y;

    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y>maxY)
        {
            transform.position = new Vector3(transform.position.x,maxY, transform.position.z);
        }
    }
}
