﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlassGame3
{
    public class ManageSubCamFXGlassGame : MonoBehaviour
    {
        // Start is called before the first frame update
        bool canActive = false;
        public Transform player;
        public float speedMove, limitDistance,speedZoom;
        Vector3 originPos;
        Quaternion origiRot;


        void Start()
        {
            originPos = transform.position;
            origiRot = transform.rotation;
            GlassGameHandle.OnPlayerFall.AddListener(OnPlayerFall);
            GlassGameHandle.OnPlayerReviveComplete.AddListener(OnPlayerRevive);
        }

        // Update is called once per frame
        void Update()
        {
            if (canActive)
            {
               /* transform.LookAt(player);
                if (Vector3.Distance(transform.position, player.position) < limitDistance)
                {
                    Vector3 direct = player.position - transform.position;
                    direct.Normalize();
                    transform.position += direct * Time.deltaTime * speedMove;
                }*/
               if(gameObject.GetComponent<Camera>().fieldOfView>31)
                {
                    gameObject.GetComponent<Camera>().fieldOfView -= Time.deltaTime*speedZoom;
                   transform.Rotate(Vector3.right * Time.deltaTime*speedMove);
                }
            }
        }
        public void OnPlayerFall()
        {
            gameObject.GetComponent<Camera>().enabled = true;
            canActive = true;
        }
        public void OnPlayerRevive()
        {
            gameObject.GetComponent<Camera>().enabled = false;
            canActive = false;
            gameObject.GetComponent<Camera>().fieldOfView = 60;
            transform.rotation = origiRot;
           // transform.position = originPos;
        }
    }
}
