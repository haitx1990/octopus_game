﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageCamGlass : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 cameraOffset;
    public Transform player;
    [Min(1f)]
    private float followSpeed = 5f;
    public float yOffSet = 10;

    // Start is called before the first frame update
    void Start()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("player").transform;
        cameraOffset = transform.position - player.transform.position;
        float _n = float.Parse(Screen.width.ToString()) / float.Parse(Screen.height.ToString());
       
    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
        {
            
            Vector3 newPos = player.transform.position + cameraOffset;
            transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * followSpeed);
            
        }
    }
    public void OnPlayerGoal()
    {
        gameObject.GetComponent<Camera>().enabled = false;
    }
    
}
