﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlassGame3
{
    public class ManageSoundGlassGame : Singleton<ManageSoundGlassGame>
    {
        AudioSource audioSource;
        public AudioClip audioStartGame, audioPlayerGotShoot, audioCollisionToGlass, audioCollisionToFloor, audioBrokeGlass, audioBrokeAllGlass;
        // Start is called before the first frame update
        void Start()
        {
            audioSource = gameObject.GetComponent<AudioSource>();
            GlassGameHandle.OnGlassBroke.AddListener(PlaySoundOnGlassBroke);
            GlassGameHandle.OnGoal.AddListener(PlaySoundOnAllGlassBroke);
            GlassGameHandle.OnOverLimitTimeStay.AddListener(PlaySoundOnPlayerGotShot);
           // GlassGameHandle.OnGlassGameStart.AddListener(PlaySoundStart);
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void PlaySoundOnGlassBroke( Transform tranf)
        {
            this.audioSource.clip = audioBrokeGlass;
            //this.audioSource.Play();
        }
        public void PlaySoundOnFloor()
        {
            this.audioSource.clip = audioCollisionToFloor;
           // this.audioSource.Play();
        }
        public void PlaySoundOnGlass()
        {
            this.audioSource.clip = audioCollisionToGlass;
            //this.audioSource.Play();
        }
        public void PlaySoundStart()
        {
            this.audioSource.clip = audioStartGame;
           // this.audioSource.Play();
        }
        public void PlaySoundOnAllGlassBroke()
        {
            this.audioSource.clip = audioBrokeAllGlass;
            //this.audioSource.Play();
        }
        public void PlaySoundOnPlayerGotShot()
        {
            this.audioSource.clip = audioPlayerGotShoot ;
            //this.audioSource.Play();

        }

    }
}
