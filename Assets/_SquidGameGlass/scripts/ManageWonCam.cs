﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlassGame3
{
    public class ManageWonCam : MonoBehaviour
    {
        // Start is called before the first frame update
        Vector3 originPos;
        bool canActiveWonCam;
        public float speedMove;
        public float limitZ;
        void Start()
        {
            originPos = transform.position;
            GlassGameHandle.OnGoal.AddListener(this.OnPlayerGoal);

        }

        // Update is called once per frame
        void Update()
        {
            /*if (canActiveWonCam)
            {
                if (transform.position.z < limitZ)
                    transform.position += Vector3.forward * speedMove * Time.deltaTime;
            }*/

        }

        public void OnPlayerGoal()
        {
            gameObject.GetComponent<Camera>().enabled = true;
            StartCoroutine(OnWonCameraActive());
        }
        IEnumerator OnWonCameraActive()
        {
            yield return new WaitForSeconds(1.5f);
            canActiveWonCam = true;
        }
    }
}
