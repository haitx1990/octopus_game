﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageGenBotGlass : MonoBehaviour
{
    // Start is called before the first frame update
    public int numOfBot;
    void Start()
    {
        // numOfBot = Random.Range(4, 9);
        /*GameObject[] allbots = GameObject.FindGameObjectsWithTag("bot");
        for(int i=0;i<numOfBot;i++)
        {
            allbots[i].SetActive(true);
        }*/
        StartCoroutine(ActiveBot());

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator ActiveBot()
    {
        yield return null;
        yield return new WaitForSeconds(0.5f);
        GameObject[] bots = GameObject.FindGameObjectsWithTag("bot");
        int num = bots.Length - numOfBot;
        for(int i=0;i<num;i++)
        {
            if(bots[i].gameObject.activeInHierarchy)
            {
                bots[i].gameObject.SetActive(false);
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
