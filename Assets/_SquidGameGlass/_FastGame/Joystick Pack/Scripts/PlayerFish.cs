﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFish : BaseFish
{


    Joystick myJoystick;
    Vector2 velocity;
    int localScale;
    bool isMove;

    // Start is called before the first frame update
    void Start()
    {
        this.fishType = FishType.PLAYER;
        myJoystick = FindObjectOfType<Joystick>();
    }

    // Update is called once per frame
    void Update()
    {
        //if(velocity != Vector2.zero)
        this.Move();

    }
    public override void Move()
    {
        velocity = new Vector2(myJoystick.Horizontal * moveSpeed, myJoystick.Vertical * moveSpeed);
        //Debug.Log(velocity.ToString());
    // Debug.Log(myJoystick.Direction.ToString());
        if (myJoystick.Direction.y > 0)
        {
            //Debug.LogError("UP");
        }
        else
        {
           // Debug.LogError("DOWN");
        }
       
        if (this.velocity.x > 0)
        {
            // transform.localScale = new Vector3(1, 1, 1);

            localScale = 1;

        }
        if (this.velocity.x < 0)
        {
            localScale = -1;
            //transform.localScale = new Vector3(-1, 1, 1);

        }
        if (this.velocity != Vector2.zero)
        {
            if(myJoystick.Direction.y>0 && myJoystick.Direction.x > 0)
            {
                transform.GetChild(0).eulerAngles = new Vector3(0, 0, 9);
            }
            if(myJoystick.Direction.y<0 && myJoystick.Direction.x > 0)
            {
                transform.GetChild(0).eulerAngles = new Vector3(0, 0, -9);
            }
            if (myJoystick.Direction.y > 0 && myJoystick.Direction.x < 0)
            {
                transform.GetChild(0).eulerAngles = new Vector3(0, 0, -9);
            }
            if (myJoystick.Direction.y < 0 && myJoystick.Direction.x < 0)
            {
                transform.GetChild(0).eulerAngles = new Vector3(0, 0, 9);
            }
            isMove = true;
            transform.localScale = new Vector3(localScale, 1, 1);
            transform.position += new Vector3(velocity.x, velocity.y, 0) * Time.deltaTime * moveSpeed;
        }
        else
        {
            transform.GetChild(0).eulerAngles = Vector3.zero;
        }
        transform.position = new Vector3(
   Mathf.Clamp(transform.position.x, -33, 30),
   Mathf.Clamp(transform.position.y, -7, 20),
   Mathf.Clamp(transform.position.z, 0, 0));



        //base.Move();
    }

}
