﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClampCam : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.position;
        transform.position = new Vector3(
  Mathf.Clamp(transform.position.x, -30,26.6f),
  Mathf.Clamp(transform.position.y, 0, 13),
  Mathf.Clamp(transform.position.z, -10, -10));

    }
}
