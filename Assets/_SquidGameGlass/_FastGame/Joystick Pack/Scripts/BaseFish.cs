﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseFish : MonoBehaviour
{
    public enum FishType { ENEMY,PLAYER};
    public enum FishLV { LV1,LV2,LV3};
    public FishType fishType;
    public FishLV fishLV;
    public float moveSpeed;
    public int ratio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public virtual void Move()
    { }
}
