﻿using System.Collections;
using UnityEngine;
using UIHelper.UI.Animation;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UIHelper.UI
{
    public class MyScrollViewEffect : MonoBehaviour
    {
        private MyScrollView myScroll;

        void Awake()
        {
            myScroll = GetComponent<MyScrollView>();    
        }
        void Start()
        {

        }

        public void RemoveIndex(int indexItem, UnityAction done)
        {
            myScroll.cells[indexItem].GetComponent<CanvasGroup>().DOCrossAlpha(PresetCurves.InEase, .5f, 1, 0)
                .onComplete(() =>
                {
                    StartCoroutine(onRemoveComplete(indexItem, done));
                });
        }

        private IEnumerator onRemoveComplete(int indexItem, UnityAction done)
        {
            float _duration = 0.5f;
            Destroy(myScroll.cells[indexItem].gameObject);

            yield return new WaitForEndOfFrame();


            myScroll.RefreshView();
            for (int i = 0; i < myScroll.cells.Count; i++)
            {
                myScroll.cells[i].DOMove(PresetCurves.Linear, _duration, myScroll.cells[i].GetRectTransform().anchoredPosition, myScroll.fillOrigin * myScroll.listPosition[i]);
            }
            yield return new WaitForSeconds(_duration + 0.2f);
            done.TryInvoke();
        }
    }
}