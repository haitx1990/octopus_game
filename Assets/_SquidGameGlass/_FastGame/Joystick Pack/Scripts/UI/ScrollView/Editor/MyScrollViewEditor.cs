﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace UIHelper.UI
{
    [CustomEditor(typeof(MyScrollView))]
    public class MyScrollViewEditor : Editor
    {
        private string[] titleHorizontal = new string[] { "Left", "Right" };
        private string[] titleVerical = new string[] { "Bottom", "Top" };
        private int[] originalFills = new int[] { -1, 1 };

        private MyScrollView myScroll;
        private void Awake()
        {
            myScroll = (MyScrollView)target;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            myScroll.Tab = GUILayout.Toolbar(myScroll.Tab, new string[] { "Horizontal", "Vertical" });

            myScroll.prefabItem = (GameObject)EditorGUILayout.ObjectField("Prefab Item", myScroll.prefabItem, typeof(GameObject), true);
            if(myScroll.prefabItem == null)
            {
                EditorGUILayout.HelpBox("Require Prefab Item not null!", MessageType.Error, false);
            }

            myScroll.content = (RectTransform)EditorGUILayout.ObjectField("Content", myScroll.content, typeof(RectTransform), true);
            myScroll.spacing = EditorGUILayout.FloatField("Spacing", myScroll.spacing);
            switch (myScroll.Tab)
            {
                case 0:
                    myScroll.padding = EditorGUILayout.FloatField("Padding Left Right", myScroll.padding);
                    break;
                case 1:
                    myScroll.padding = EditorGUILayout.FloatField("Padding Top Bottom", myScroll.padding);
                    break;
            }

            EditorGUILayout.Space();

            GUILayout.BeginVertical("PHYSICAL SCROLLING", "window");
            myScroll.sensitivity = EditorGUILayout.Slider("Sensitivity", myScroll.sensitivity, 0, 2);
            myScroll.decelerationRate = EditorGUILayout.Slider("DecelerationRate", myScroll.decelerationRate, 0, 1);
            myScroll.elasticity = EditorGUILayout.Slider("Elasticity", myScroll.elasticity, 0.1f, 1f);
            GUILayout.EndVertical();

            EditorGUILayout.Space();

            GUILayout.BeginVertical("Box");
            switch (myScroll.Tab)
            {
                case 0:
                    myScroll.fillOrigin = EditorGUILayout.IntPopup("Fill Origin", myScroll.fillOrigin, titleHorizontal, originalFills);
                    break;
                case 1:
                    myScroll.fillOrigin = EditorGUILayout.IntPopup("Fill Origin", myScroll.fillOrigin, titleVerical, originalFills);
                    break;
            }

            myScroll.IsSnap = GUILayout.Toggle(myScroll.IsSnap, "Is Snap");
            GUILayout.EndVertical();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(myScroll);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif