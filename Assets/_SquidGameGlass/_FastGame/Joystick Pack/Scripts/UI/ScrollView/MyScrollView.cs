﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UIHelper.UI
{
    public class MyScrollView : MonoBehaviour, IInitializePotentialDragHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [HideInInspector] public int Tab;
        private bool Horizontal
        {
            get => Tab == 0;
        }

        [HideInInspector] public GameObject prefabItem;

        [HideInInspector] public RectTransform content;
        private RectTransform ViewportRect;

        [HideInInspector] public float spacing;
        [HideInInspector] public float padding;

        [HideInInspector] public float sensitivity = 1;
        [HideInInspector] public float decelerationRate = .05f;
        [HideInInspector] public float elasticity = 0.1f;

        [HideInInspector] public int fillOrigin = -1;
        [HideInInspector] public bool IsSnap;

        //Item Size
        private float width;
        private float height;

        internal List<Vector2> listPosition = new List<Vector2>();
        internal List<ScrollCell> cells = new List<ScrollCell>();

        private bool isScrolling;
        private bool isInertia;

        private Vector2 velocity = Vector2.zero;
        private Vector2 direction;
        internal Vector2 limitContentPosition;

        public float NormalizedPosition
        {
            get 
            {
                if (!IsSnap)
                {
                    if (Horizontal)
                        return (-fillOrigin * limitContentPosition.x - content.anchoredPosition.x).Abs() / (2 * limitContentPosition.x);
                    else
                        return (-fillOrigin * limitContentPosition.y - content.anchoredPosition.y).Abs() / (2 * limitContentPosition.y);
                }
                return 0;
            }
            set
            {
                if (!IsSnap)
                {
                    if (Horizontal)
                        content.anchoredPosition = -fillOrigin * (limitContentPosition.x - (value.Claim() * 2 * limitContentPosition.x)) * Vector2.right;
                    else
                        content.anchoredPosition = -fillOrigin * (limitContentPosition.y - (value.Claim() * 2 * limitContentPosition.y)) * Vector2.up;
                }

            }
        }

        private float smoothTime = 0;
        private float SmoothTime
        {
            get
            {
                if (IsSnap)
                {
                    if (Horizontal && content.anchoredPosition.x.Abs() > content.sizeDelta.x / 2)
                    {
                        return smoothTime + (content.anchoredPosition.x.Abs() - content.sizeDelta.x / 2) / sizePerItem;
                    }
                    else if (content.anchoredPosition.y.Abs() > content.sizeDelta.y / 2)
                    {
                        return smoothTime + (content.anchoredPosition.y.Abs() - content.sizeDelta.y / 2) / sizePerItem;
                    }
                }
                else
                {
                    if (Horizontal && content.anchoredPosition.x.Abs() > limitContentPosition.x)
                    {
                        return smoothTime + (content.anchoredPosition.x.Abs() - limitContentPosition.x) / sizePerItem;
                    }
                    else if (content.anchoredPosition.y.Abs() > limitContentPosition.y)
                    {
                        return smoothTime + (content.anchoredPosition.y.Abs() - limitContentPosition.y) / sizePerItem;
                    }
                }
           
                return smoothTime;
            }
        }
        private bool InBound
        {
            get
            {
                if (IsSnap)
                    return Horizontal ? content.anchoredPosition.x.Abs() <= content.sizeDelta.x / 2 : content.anchoredPosition.y.Abs() <= content.sizeDelta.y / 2;
                else
                    return Horizontal ? content.anchoredPosition.x.Abs() <= limitContentPosition.x : content.anchoredPosition.y.Abs() <= limitContentPosition.y;
            }
        }

        private int curIndex = 0;

        internal float sizePerItem = 0;

        private int maxItem;

        public UnityAction<int> OnStageChange;

        public UnityAction<int, GameObject> OnFill;

        #region INITIALIZATION METHODS
        public void Init(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                GameObject obj = Instantiate(prefabItem, content, false);
                obj.name = string.Format("{0} ({1})", prefabItem.name, i);
                OnFill.Invoke(i, obj);
            }
            onAwake();
            onStart();
        }

        private void onAwake()
        {
            ViewportRect = content.parent.GetRectTransform();
            var itemRect = prefabItem.GetRectTransform();
            width = itemRect.sizeDelta.x;
            height = itemRect.sizeDelta.y;
        }
        void onStart()
        {
            RefreshView();
            for (int i = 0; i < maxItem; i++)
            {
                cells[i].GetRectTransform().anchoredPosition = fillOrigin * listPosition[i];
            }
        }
        public void RefreshView()
        {
            maxItem = content.childCount;
            cells.Clear();
            listPosition.Clear();
            if (Horizontal)
            {
                sizePerItem = width + spacing;
                content.sizeDelta = new Vector2(width * maxItem + spacing * (maxItem - 1) + 2 * padding, height);
                limitContentPosition = (content.sizeDelta.x - ViewportRect.sizeDelta.x) / 2 * Vector2.right;
            }
            else
            {
                sizePerItem = height + spacing;
                content.sizeDelta = new Vector2(width, height * maxItem + spacing * (maxItem - 1) + 2 * padding);
                limitContentPosition = (content.sizeDelta.y - ViewportRect.sizeDelta.y) / 2 * Vector2.up;
            }

            if (maxItem % 2 == 0)
            {
                for (int i = maxItem - 1; i > -maxItem; i -= 2)
                {
                    if (Horizontal)
                    {
                        Vector2 pos = new Vector2(i * (sizePerItem / 2), 0);
                        listPosition.Add(pos);
                    }
                    else
                    {
                        Vector2 pos = new Vector2(0, i * (sizePerItem / 2));
                        listPosition.Add(pos);
                    }
                }
            }
            else
            {
                for (int i = (maxItem - 1) / 2; i > -(maxItem - 1) / 2 - 1; i--)
                {
                    if (Horizontal)
                    {
                        Vector2 pos = new Vector2(i * sizePerItem, 0);
                        listPosition.Add(pos);
                    }
                    else
                    {
                        Vector2 pos = new Vector2(0, i * sizePerItem);
                        listPosition.Add(pos);
                    }
                }
            }

            for (int i = 0; i < maxItem; i++)
            {
                cells.Add(content.GetChild(i).GetComponent<ScrollCell>());
                if (IsSnap)
                {
                    int _index = i;
                    cells[i].SetAction(() => LefpToStage(_index));
                }
            }
        }
        #endregion

        private void Update()
        {
            for (int i = 0; i < cells.Count; i++)
            {
                //Calculate normalizeTime Item's Animation
                if (Horizontal)
                {
                    cells[i].UpdateState(1 - (content.localPosition.x + fillOrigin * listPosition[i].x).Abs() / (2 * sizePerItem));
                }
                else
                {
                    cells[i].UpdateState(1 - (content.localPosition.y + fillOrigin * listPosition[i].y).Abs() / (2 * sizePerItem));
                }
            }
        }
        protected void LateUpdate()
        {
            if (!isScrolling)
            {
                if (IsSnap)
                {
                    if (Vector2.Distance(content.anchoredPosition, listPosition[curIndex]) > .1f)
                    {
                        content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, listPosition[curIndex], ref velocity, elasticity / sensitivity);
                    }
                }
                else
                {
                    if (Horizontal)
                    {
                        if (content.anchoredPosition.x.Abs() > limitContentPosition.x)
                        {
                            if (content.sizeDelta.x <= ViewportRect.sizeDelta.x)
                            {
                                if (Vector2.Distance(content.anchoredPosition, fillOrigin * -limitContentPosition) > .1f)
                                    content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, fillOrigin * -limitContentPosition, ref velocity, elasticity / sensitivity);
                            }
                            else
                            {
                                if (content.anchoredPosition.x >= 0)
                                {
                                    if (Vector2.Distance(content.anchoredPosition, limitContentPosition) > .1f)
                                        content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, limitContentPosition, ref velocity, elasticity / sensitivity);
                                }
                                else
                                {
                                    if (Vector2.Distance(content.anchoredPosition, -limitContentPosition) > .1f)
                                        content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, -limitContentPosition, ref velocity, elasticity / sensitivity);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (content.anchoredPosition.y.Abs() > limitContentPosition.y)
                        {
                            if (content.sizeDelta.y <= ViewportRect.sizeDelta.y)
                            {
                                if (Vector2.Distance(content.anchoredPosition, fillOrigin * -limitContentPosition) > .1f)
                                    content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, fillOrigin * -limitContentPosition, ref velocity, elasticity / sensitivity);
                            }
                            else
                            {
                                if (content.anchoredPosition.y >= 0)
                                {
                                    if (Vector2.Distance(content.anchoredPosition, limitContentPosition) > .1f)
                                        content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, limitContentPosition, ref velocity, elasticity / sensitivity);
                                }
                                else
                                {
                                    if (Vector2.Distance(content.anchoredPosition, -limitContentPosition) > .1f)
                                        content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, -limitContentPosition, ref velocity, elasticity / sensitivity);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (isInertia)
                {
                    if (IsSlowing())
                    {
                        content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition, content.anchoredPosition + direction, ref velocity, SmoothTime);
                        direction = Vector2.LerpUnclamped(direction, Vector2.zero, decelerationRate + SmoothTime);
                    }
                    else
                    {
                        if (IsSnap) LefpToStage(GetIndex());
                        else isScrolling = false;
                        isInertia = false;
                    }
                }
            }
        }

        private bool IsSlowing()
        {
            if (Horizontal)
            {
                return velocity.x.Abs() > 1;
            }
            else
            {
                return velocity.y.Abs() > 1;
            }
        }
        private int GetIndex()
        {
            if (Horizontal)
            {
                if (content.localPosition.x > content.sizeDelta.x / 2) return 0;
                else if (content.localPosition.x < -content.sizeDelta.x / 2) return maxItem - 1;
                else
                {
                    for (int i = 0; i < listPosition.Count; i++)
                    {
                        if ((listPosition[i].x - content.localPosition.x).Abs() < sizePerItem / 2)
                            return i;
                    }
                    return 0;
                }
            }
            else
            {
                if (content.localPosition.y > content.sizeDelta.y / 2) return 0;
                else if (content.localPosition.y < -content.sizeDelta.y / 2) return maxItem - 1;
                else
                {
                    for (int i = 0; i < listPosition.Count; i++)
                    {
                        if ((listPosition[i].y - content.localPosition.y).Abs() < sizePerItem / 2)
                            return i;
                    }
                    return 0;
                }
            }
        }
        public void LefpToStage(int index)
        {
            isScrolling = false;
            if (index != curIndex)
            {
                curIndex = index;
                UpdateCellState();
            }
        }
        public void PreviousStage()
        {
            if (curIndex > 0)
            {
                curIndex--;
                UpdateCellState();
            }
            else
            {
                curIndex = 0;
            }
        }
        public void NextStage()
        {
            if (curIndex < maxItem - 1)
            {
                curIndex++;
                UpdateCellState();
            }
            else
            {
                curIndex = maxItem - 1;
            }
        }
        private void UpdateCellState()
        {
            OnStageChange.TryInvoke(curIndex);
        }

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            isInertia = false;
            direction = Vector2.zero;
        }
        public void OnBeginDrag(PointerEventData eventData)
        {
            isScrolling = true;
        }
        public void OnDrag(PointerEventData eventData)
        {
            content.anchoredPosition = Vector2.SmoothDamp(content.anchoredPosition,
                                                       content.anchoredPosition + (Horizontal ? new Vector2(sensitivity * eventData.delta.x, 0) : new Vector2(0, sensitivity * eventData.delta.y)),
                                                       ref velocity,
                                                       SmoothTime);
        }
        public void OnEndDrag(PointerEventData eventData)
        {
            if (InBound)
            {
                direction = Horizontal ? sensitivity * eventData.delta.x * Vector2.right : sensitivity * eventData.delta.y * Vector2.up;
            }
            else
            {
                direction = Vector2.zero;
                velocity = Vector2.zero;
            }
            isInertia = true;
        }

    }
}