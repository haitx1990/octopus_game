﻿using UIHelper.UI.Animation;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIHelper.UI
{
    public abstract class ScrollCell : MonoBehaviour
    {
        protected UnityAction<float> updateCellMethods;

        private void Awake()
        {
            SetAnimation();
        }

        protected abstract void SetAnimation();

        public void UpdateState(float normalizeTime)
        {
            if(updateCellMethods != null)
            {
                updateCellMethods.Invoke(normalizeTime);
            }
        }

        public void SetAction(UnityAction action)
        {
            GetComponent<Button>().SetAction(action);
        }
    }
}