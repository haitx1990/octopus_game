﻿using System.Collections;
using UnityEngine;
using UIHelper.UI.Animation;
using UnityEngine.Events;

namespace UIHelper.UI.LuckyWheel
{
    public class LuckyWheelController : MonoBehaviour
    {
        public Transform Wheel;

        public Transform Arrow;

        private const float DISTANCE_ARROW_IMPACT = 16;

        private const int ANGLE_PER_ITEM = 360 / 8;

        public AnimationCurve rotateCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        private void OnEnable()
        {
            Wheel.localRotation = Quaternion.Euler(0, 0, Utils.CommonUtils.Random(0, 360));
        }

        void Update()
        {
            int point = GetPoint();
            if(point == -1)
            {
                Arrow.localRotation = Quaternion.LerpUnclamped(Arrow.rotation, Quaternion.identity, .25f);
            }
            else
            {
                Arrow.localRotation = Quaternion.Euler(0, 0, (1 - ((Wheel.localEulerAngles.z - (point * ANGLE_PER_ITEM)) / DISTANCE_ARROW_IMPACT)) * 65);
            }
        }

        public void OnSpin(int indexReward, UnityAction<int> onComplete)
        {
            float endAngle = GetRandomAngleReward(indexReward);
            Vector3 endRotation = new Vector3(0, 0, endAngle - 6 * 360);
            Wheel.DORotate(rotateCurve, 10, endRotation)
                .onComplete(() =>
                {

                    if (Wheel.localEulerAngles.z > indexReward * ANGLE_PER_ITEM && Wheel.localEulerAngles.z < indexReward * ANGLE_PER_ITEM + DISTANCE_ARROW_IMPACT)
                        Wheel.DORotate(PresetCurves.InOutEase, 2f, (indexReward * ANGLE_PER_ITEM + DISTANCE_ARROW_IMPACT) * Vector3.forward).onComplete(() => onComplete.TryInvoke(indexReward));
                    else
                        onComplete.TryInvoke(indexReward);
                });
        }


        private int GetPoint()
        {
            float angleZ = Wheel.localEulerAngles.z;
            for (int i = 0; i < 8; i++)
            {
                if (angleZ >= i * ANGLE_PER_ITEM && angleZ < i * ANGLE_PER_ITEM + DISTANCE_ARROW_IMPACT)
                {
                    return i;
                }
            }
            return -1;
        }

        private int GetIndexReward()
        {
            float angleZ = Wheel.localEulerAngles.z;
            return (int)angleZ / ANGLE_PER_ITEM;
        }

        private float GetRandomAngleReward(int indexReward)
        {
            return Utils.CommonUtils.Random(indexReward * ANGLE_PER_ITEM + 1, indexReward * ANGLE_PER_ITEM + ANGLE_PER_ITEM);
        }
    }
}