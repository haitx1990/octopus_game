﻿using System.Collections;
using UnityEngine;

namespace UIHelper.UI.LuckyWheel
{
    public class AutoRotate : MonoBehaviour
    {
        private Transform m_transform;
        public float angle;
        private void Awake()
        {
            m_transform = transform;
        }
        void Update()
        {
            m_transform.Rotate(0, 0, angle);
        }
    }
}