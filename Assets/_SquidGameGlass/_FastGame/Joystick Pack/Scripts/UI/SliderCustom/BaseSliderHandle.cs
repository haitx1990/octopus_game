﻿using System.Collections;
using UnityEngine;

namespace UIHelper.UI
{
    public abstract class BaseSliderHandle : MonoBehaviour
    {
        public abstract void onValueChanged(float normalized);
    }
}