﻿using UIHelper.UI.Animation;
using System.Collections;
using UnityEngine;

namespace UIHelper.UI
{
    public class SliderHandleBackground : BaseSliderHandle
    {
        public Transform handle;
        public Transform[] paths;
        public override void onValueChanged(float normalized)
        {
            handle.position = PresetCurves.GetBrizerRecusive(normalized, paths[0].position, paths[1].position, paths[2].position, paths[3].position);
            handle.LookAt(PresetCurves.GetBrizerRecusive(normalized + 0.1f, paths[0].position, paths[1].position, paths[2].position, paths[3].position));
        }
    }
}