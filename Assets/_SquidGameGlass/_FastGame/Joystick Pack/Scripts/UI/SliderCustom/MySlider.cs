﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIHelper.UI
{
    public class MySlider : MonoBehaviour
    {
        [HideInInspector] public float minValue = 0;
        [HideInInspector] public float maxValue = 1;
        [HideInInspector] public float value;
        public float Value
        {
            get => value;
            set
            {
                this.value = value;
                onValueChanged.Invoke();
            }
        }
        [HideInInspector] public Image imgFill;

        [HideInInspector] public UnityAction onValueChanged;
        [HideInInspector] public BaseSliderHandle handle;

        public void onValueChangedMethod()
        {
            imgFill.fillAmount = value / maxValue;
            handle.onValueChanged(imgFill.fillAmount);
        }
        void Awake()
        {
            onValueChanged = onValueChangedMethod;
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(MySlider))]
    public class MySliderEditor : Editor
    {
        private MySlider m_target;
        private void Awake()
        {
            m_target = target as MySlider;
            m_target.onValueChanged = m_target.onValueChangedMethod;
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            m_target.minValue = EditorGUILayout.FloatField("Min Value", m_target.minValue);
            m_target.maxValue = EditorGUILayout.FloatField("Max Value", m_target.maxValue);

            m_target.value = EditorGUILayout.Slider("Value", m_target.value, m_target.minValue, m_target.maxValue);
            m_target.imgFill = (Image)EditorGUILayout.ObjectField("Image Fill", m_target.imgFill, typeof(Image), true);
            m_target.handle = (BaseSliderHandle)EditorGUILayout.ObjectField("Handle", m_target.handle, typeof(BaseSliderHandle), true);

            if (GUI.changed)
            {
                m_target.onValueChanged.Invoke();
                EditorUtility.SetDirty(m_target);
            }


            
        }

    }

#endif
}