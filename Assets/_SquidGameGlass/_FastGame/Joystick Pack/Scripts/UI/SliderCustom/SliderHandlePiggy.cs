﻿using UnityEngine;

namespace UIHelper.UI
{
    public class SliderHandlePiggy : BaseSliderHandle
    {
        public Transform handle;
        public override void onValueChanged(float normalized)
        {
            handle.rotation = Quaternion.Euler(0, 0, -186 * normalized);
        }
    }
}