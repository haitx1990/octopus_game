﻿using UnityEngine;

namespace UIHelper.UI.Animation
{
    public static class AnimationUIExtension
    {
        public static AnimationUIUpdate DOAnimation<T>(this T obj, AnimationCurve animationCurve, float duration, float delay = 0) where T : class
        {
            return new AnimationUIUpdate(delay, duration, animationCurve).Play();
        }
        public static AnimationUIBase DOScale<T>(this T arg, AnimationCurve curve, float duration, Vector3 startScale, Vector3 endScale, float delay = 0) where T : Component
        {
            return new AnimationUIScale(delay, duration, curve, arg is Transform ? arg as Transform : arg.transform, startScale, endScale).Play();
        }
        public static AnimationUIBase DOScaleXY<T>(this T arg, AnimationCurve curve, float duration, float delay = 0) where T : Component
        {
            return new AnimationUIScaleXY(delay, duration, curve, arg is Transform ? arg as Transform : arg.transform).Play();
        }
        public static AnimationUIBase DOCrossAlpha<T>(this T type, AnimationCurve curve, float duration, float startAlpha, float endAlpha, float delay = 0) where T : Component
        {
            return new AnimationUICrossAlpha(delay, duration, curve, type is CanvasGroup ? type as CanvasGroup : type.GetComponent<CanvasGroup>(), startAlpha, endAlpha).Play();
        }
        public static AnimationUIBase DOMove<T>(this T obj, AnimationCurve curve, float duration, Vector2 startPos, Vector2 endPos, float delay = 0) where T : Component
        {
            return new AnimationUIMove(delay, duration, curve, obj is RectTransform ? obj as RectTransform : obj.GetRectTransform(), startPos, endPos).Play();
        }
        public static AnimationUIBase DOMovePath<T>(this T obj, AnimationCurve curve, float duration, float delay = 0, params Vector2[] path) where T : Component
        {
            return new AnimationUIMovePath(delay, duration, curve, obj is Transform ? obj as Transform : obj.transform, path).Play();
        }
        public static AnimationUIBase DORotate<T>(this T arg, AnimationCurve curve, float duration, Vector3 endAngle, float delay = 0) where T : Component
        {
            return new AnimationUIRotate(delay, duration, curve, arg is Transform ? arg as Transform : arg.transform, endAngle).Play();
        }
        //====================================================================================================================================================
        public static AnimationUIBase DOScaleOnCreate<T>(this T arg, AnimationCurve curve, Vector3 startScale, Vector3 endScale) where T : Component
        {
            return new AnimationUIScale(0, 0, curve, arg is Transform ? arg as Transform : arg.transform, startScale, endScale);
        }
        public static AnimationUIBase DOCrossAlphaOnCreate<T>(this T arg, AnimationCurve curve, float startAlpha, float endAlpha) where T : Component
        {
            return new AnimationUICrossAlpha(0, 0, curve, arg is CanvasGroup ? arg as CanvasGroup : arg.GetComponent<CanvasGroup>(), startAlpha, endAlpha);
        }
        public static AnimationUIFloat DOFloatOnCreate<T>(this T arg, AnimationCurve curve, float delay = 0) where T : class
        {
            return new AnimationUIFloat(delay, 0, curve, 0, 1);
        }
        public static AnimationUIBase DOMoveCreate<T>(this T obj, AnimationCurve curve, float duration, Vector2 startPos, Vector2 endPos, float delay = 0) where T : Component
        {
            return new AnimationUIMove(delay, duration, curve, obj is RectTransform ? obj as RectTransform : obj.GetRectTransform(), startPos, endPos);
        }
        public static AnimationUIBase DOMovePathCreat<T>(this T obj, AnimationCurve curve, float duration, float delay = 0, params Vector2[] path) where T : Component
        {
            return new AnimationUIMovePath(delay, duration, curve, obj is Transform ? obj as Transform : obj.transform, path);
        }
        public static AnimationUIBase DORotateCreate<T>(this T arg, AnimationCurve curve, float duration, Vector3 endAngle, float delay = 0) where T : Component
        {
            return new AnimationUIRotate(delay, duration, curve, arg is Transform ? arg as Transform : arg.transform, endAngle);
        }
    }
}