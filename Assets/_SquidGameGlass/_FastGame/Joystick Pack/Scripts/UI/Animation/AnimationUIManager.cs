﻿using UnityEngine;
using UnityEngine.Events;

namespace UIHelper.UI.Animation
{
    public class AnimationUIManager : MonoBehaviour
    {
        private static AnimationUIManager instance = null;
        public static AnimationUIManager Instance
        {
            get
            {
                if (instance == null)
                {
                    var obj = new GameObject("AnimationUIManager", typeof(AnimationUIManager)).GetComponent<AnimationUIManager>();
                    instance = obj;
                }
                return instance;
            }
        }

        public UnityAction updateEvent;

        //public static void SetUp()
        //{
        //    Debug.Log("Initialize AnimationUIManager Done.");
        //}

        static AnimationUIManager()
        {
            Debug.Log("Initializing AnimationUIManager.");
            var obj = new GameObject("AnimationUIManager", typeof(AnimationUIManager)).GetComponent<AnimationUIManager>();
            instance = obj;
            DontDestroyOnLoad(obj.gameObject);
        }

        void Update()
        {
            if (!ReferenceEquals(updateEvent, null))
            {
                updateEvent.Invoke();
            }
        }
    }
}