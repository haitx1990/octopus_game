﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIHelper.UI.Animation
{
    public abstract class AnimationUIBase
    {
        public float elapsedTime;
        public float duration;
        protected float delay;
        public AnimationCurve curve;
        protected UnityAction completeAction;
        protected bool isCompleted;
        protected int loop;

        public AnimationUIBase SetDuration(float duration)
        {
            this.duration = duration;
            return this;
        }
        public AnimationUIBase SetDelay(float delay)
        {
            this.delay = delay;
            return this;
        }
        public AnimationUIBase SetLoop(int loop)
        {
            this.loop = loop;
            return this;
        }
        ~AnimationUIBase()
        {
            curve = null;
            completeAction = null;
        }
        public AnimationUIBase(float delay, float duration, AnimationCurve curve)
        {
            this.delay = delay;
            this.duration = duration;
            this.curve = curve;
        }

        private void UpdateOnBehaviour()
        {
            if (elapsedTime < 1)
            {
                elapsedTime += Time.deltaTime * 1 / duration;
                if (elapsedTime < 0)
                {
                    return;
                }
                UpdateAnimationMethod(curve.Evaluate(elapsedTime));
            }
            else if(loop < 0)
            {
                elapsedTime = 0;
            }
            else if(loop > 0)
            {
                elapsedTime = 0;
                loop--;
            }
            else if(!isCompleted)//complete
            {
                completeAction.TryInvoke();
                Stop();
            }
        }

        protected abstract void UpdateAnimationMethod(float value);

        public AnimationUIBase onComplete(UnityAction completeMethod)
        {
            this.completeAction = completeMethod;
            return this;
        }

        public void UpdateManual(float time)
        {
            UpdateAnimationMethod(curve.Evaluate(time));
        }

        public virtual AnimationUIBase Play()
        {
            isCompleted = false;
            elapsedTime = -delay;
            AnimationUIManager.Instance.updateEvent += UpdateOnBehaviour;
            return this;
        }

        public AnimationUIBase Stop()
        {
            isCompleted = true;
            AnimationUIManager.Instance.updateEvent -= UpdateOnBehaviour;
            return this;
        }
    }
    //=================================================================================================================================================
    public class AnimationUIUpdate : AnimationUIBase
    {
        public UnityAction<float> updateAction;

        public AnimationUIUpdate(float delay, float duration, AnimationCurve curve) : base(delay, duration, curve)
        {
        }
      
        public AnimationUIBase onUpdate(UnityAction<float> updateMethod)
        {
            this.updateAction = updateMethod;
            return this;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            updateAction.Invoke(value);
        }
        new public AnimationUIUpdate Play()
        {
            base.Play();
            return this;
        }
    }

    //=================================================================================================================================================
    public class AnimationUICrossAlpha : AnimationUIBase
    {
        private CanvasGroup grpCanvas;
        private float startAlpha;
        private float endAlpha;

        public AnimationUICrossAlpha(float delay, float duration, AnimationCurve curve, CanvasGroup grpCanvas, float startAlpha, float endAlpha) : base(delay, duration, curve)
        {
            this.grpCanvas = grpCanvas;
            this.startAlpha = startAlpha;
            this.endAlpha = endAlpha;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            grpCanvas.alpha = startAlpha + (endAlpha - startAlpha) * value;
        }

        public override AnimationUIBase Play()
        {
            grpCanvas.alpha = startAlpha;
            return base.Play();
        }
    }

    //=================================================================================================================================================
    public class AnimationUIScale : AnimationUIBase
    {
        private Transform m_transform;
        private Vector3 startScale;
        private Vector3 endScale;

        ~AnimationUIScale()
        {
            m_transform = null;
        }
        public AnimationUIScale(float delay, float duration, AnimationCurve curve, Transform m_transform, Vector3 startScale, Vector3 endScale) : base(delay, duration, curve)
        {
            this.m_transform = m_transform;
            this.startScale = startScale;
            this.endScale = endScale;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            if (m_transform == null)
            {
                Stop();
                return;
            }
            m_transform.localScale = startScale + (endScale - startScale) * value;
        }

        public override AnimationUIBase Play()
        {
            m_transform.localScale = startScale;
            return base.Play();
        }
    }

    public class AnimationUIScaleXY : AnimationUIBase
    {
        private Transform m_transform;
        private Vector3 _scale;
        private readonly Vector3 defScale;
        public AnimationUIScaleXY(float delay, float duration, AnimationCurve curve, Transform m_transform) : base(delay, duration, curve)
        {
            this.m_transform = m_transform;
            _scale = m_transform.localScale;
            defScale = Vector3.one;// m_transform.localScale;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            _scale.x = defScale.x * value;
            _scale.y = defScale.y * 2 - value;
            m_transform.localScale = _scale;
        }
       
    }
    //=================================================================================================================================================
    public class AnimationUIRotate : AnimationUIBase
    {
        private Transform m_transform;
        private Vector3 startAngle;
        private Vector3 endAngle;

        public AnimationUIRotate(float delay, float duration, AnimationCurve curve, Transform m_transform, Vector3 endAngle) : base(delay, duration, curve)
        {
            this.m_transform = m_transform;
            this.endAngle = endAngle;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            if(m_transform == null)
            {
                Stop();
                return;
            }
            Vector3 euler = new Vector3
                (
                    startAngle.x + (endAngle.x - startAngle.x) * value,
                    startAngle.y + (endAngle.y - startAngle.y) * value,
                    startAngle.z + (endAngle.z - startAngle.z) * value
                );
            m_transform.rotation = Quaternion.Euler(euler);
        }

        public override AnimationUIBase Play()
        {
            startAngle = m_transform.rotation.eulerAngles;
            return base.Play();
        }
    }
    //=================================================================================================================================================
    public class AnimationUIMove : AnimationUIBase
    {
        private RectTransform m_transform;
        private Vector2 startPosition;
        private Vector2 endPosition;

        public AnimationUIMove(float delay, float duration, AnimationCurve curve, RectTransform m_transform, Vector2 startPosition, Vector2 endPosition) : base(delay, duration, curve)
        {
            this.m_transform = m_transform;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            if (m_transform == null)
            {
                Stop();
                return;
            }
            m_transform.anchoredPosition = startPosition + (endPosition - startPosition) * value;
        }

        public override AnimationUIBase Play()
        {
            m_transform.anchoredPosition = startPosition;
            return base.Play();
        }
    }

    public class AnimationUIMovePath : AnimationUIBase
    {
        private Transform m_transform;
        private Vector2[] paths;

        ~AnimationUIMovePath()
        {
            m_transform = null;
            System.Array.Clear(paths, 0, paths.Length);
        }

        public AnimationUIMovePath(float delay, float duration, AnimationCurve curve, Transform m_transform, params Vector2[] paths) : base(delay, duration, curve)
        {
            this.m_transform = m_transform;
            this.paths = paths;
        }

        protected override void UpdateAnimationMethod(float value)
        {
            if (m_transform == null)
            {
                Stop();
                return;
            }
            m_transform.position = PresetCurves.GetBrizerRecusive(value, paths);
        }

        public override AnimationUIBase Play()
        {
            m_transform.position = paths[0];
            return base.Play();
        }
    }

    //=================================================================================================================================================
    public class AnimationUIFloat : AnimationUIUpdate
    {
        private float startValue;
        private float endValue;

        new public AnimationUIFloat SetDelay(float delay)
        {
            this.delay = delay;
            return this;
        }

        public AnimationUIFloat(float delay, float duration, AnimationCurve curve, float startValue, float endValue) : base(delay, duration, curve)
        {
            this.startValue = startValue;
            this.endValue = endValue;
        }
        protected override void UpdateAnimationMethod(float value)
        {
            updateAction.TryInvoke(startValue + (endValue - startValue) * value);
        }

        new public AnimationUIFloat onUpdate(UnityAction<float> updateMethod)
        {
            this.updateAction = updateMethod;
            return this;
        }

        new public AnimationUIFloat onComplete(UnityAction completeMethod)
        {
            base.onComplete(completeMethod);
            return this;
        }

        public AnimationUIFloat Restart(float startValue, float endValue, float duration = 1)
        {
            Stop();
            this.duration = duration;
            this.startValue = startValue;
            this.endValue = endValue;
            Play();
            return this;
        }
    }

}