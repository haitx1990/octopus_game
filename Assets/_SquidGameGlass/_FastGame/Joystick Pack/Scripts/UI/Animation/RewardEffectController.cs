﻿using System.Collections;
using UnityEngine;
using UIHelper.Utils;
using UnityEngine.Events;
using System.Collections.Generic;
using TMPro;

namespace UIHelper.UI.Animation
{
    public class RewardEffectController : MonoBehaviour
    {
        public GameObject prefabStar;
        public GameObject prefabCoin;
        public GameObject prefabGoldenTicket;
        public GameObject prefabFuel;

        public GameObject prefabText;
        public AnimationCurve curveText;

        private RectTransform[] objItems;

        private Queue<IEnumerator> queueEffect = new Queue<IEnumerator>();
        private bool isRunning;

        public void RunEffectStar(int amount, Vector2[] posArray, UnityAction complete, int radiusPoints = 100)
        {
            queueEffect.Enqueue(RunCoroutine(prefabStar, amount, posArray, PresetCurves.InOutClimb, complete, radiusPoints));
            if (!isRunning)
                Deployment();
        }

        public void RunEffectCoin(Vector2[] posArray, UnityAction complete, int radiusPoints = 100)
        {
            queueEffect.Enqueue(RunCoroutine(prefabCoin, 10, posArray, PresetCurves.InEase, complete, radiusPoints));
            if (!isRunning)
                Deployment();
        }

        public void RunEffectGoldenTicket(Vector2[] posArray, UnityAction complete, int radiusPoints = 100)
        {
            queueEffect.Enqueue(RunCoroutine(prefabGoldenTicket, 6, posArray, PresetCurves.InEase, complete, radiusPoints));
            if (!isRunning)
                Deployment();
        }

        public void RunEffectFuel(Vector2[] posArray, UnityAction complete, int radiusPoints = 100)
        {
            queueEffect.Enqueue(RunCoroutine(prefabFuel, 1, posArray, PresetCurves.InOutClimb, complete, radiusPoints));
            if (!isRunning)
                Deployment();
        }

        private IEnumerator RunCoroutine(GameObject prefab, int amount, Vector2[] posArray, AnimationCurve curve, UnityAction complete, int radiusPoints = 100)
        {
            isRunning = true;
            objItems = new RectTransform[amount];
            Vector2[] pos;
            transform.position = posArray[0];
            for (int i = 0; i < objItems.Length; i++)
            {
                pos = new Vector2[posArray.Length];
                //objItems[i] = SimplePool.Spawn(prefab, Vector3.zero, Quaternion.identity, transform).GetRectTransform();
                objItems[i].anchoredPosition = new Vector2(CommonUtils.Random(-radiusPoints, radiusPoints), CommonUtils.Random(-radiusPoints, radiusPoints));
                pos[0] = objItems[i].position;
                for (int j = 1; j < posArray.Length; j++)
                {
                    pos[j] = posArray[j];
                }
                if (i == 0)
                    objItems[i].DOMovePath(curve, 1.5f, 0, pos).onComplete(()=>{
                        complete.TryInvoke();
                        isRunning = false;
                        Deployment();
                    });
                else
                    objItems[i].DOMovePath(curve, 1.5f, 0, pos);
                yield return null;
            }
        }

        private void Deployment()
        {
            if (queueEffect.Count > 0)
            {
                StartCoroutine(queueEffect.Dequeue());
            }
        }

        public void ShowRewardAmount(Vector3 pos, int amount)
        {
            RectTransform TextFX = SimplePool.Spawn<RectTransform>(prefabText, pos, Quaternion.identity, transform);
            TextMeshProUGUI tmp = TextFX.GetComponentInChildren<TextMeshProUGUI>();
            TextFX.DOScale(curveText, 1.5f, Vector3.zero, Vector3.one);
            TextFX.DOMove(PresetCurves.InOutEase, 1.5f, TextFX.anchoredPosition, TextFX.anchoredPosition + Vector2.up * 50)
                .onComplete(() => SimplePool.Despawn(TextFX.gameObject));
            tmp.SetTextFormat("+{0}", amount);
            
        }
    }
}