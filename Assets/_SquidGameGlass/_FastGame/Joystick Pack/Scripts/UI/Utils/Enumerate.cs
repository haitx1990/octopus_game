﻿
namespace UIHelper
{
    public class Enumerate
    {
        public enum PopupKey
        {
            POPUP_BACKGROUND,
            POPUP_QUEST,
            POPUP_DAILY_GIFT,
            POPUP_LUCKY_WHEEL,
            POPUP_PIGGY_BANK,
            POPUP_TRANSPORTATION
        }

        public enum RewardKey
        {
            COIN,
            GOLDEN_TICKET,
            SKILL_UNDO,
            SKILL_ADDITIONAL_CARD,
            SKILL_WILD_CARD,
            FUEL
        }

        public enum DailyQuestKey
        {
            COLLECT_BLACK_CARD,
            USE_WILD_CARD,
            COLLECT_RED_CARD,
            GET_STREAKS,
            SAVE_CARD_IN_HAND
        }
    }
}