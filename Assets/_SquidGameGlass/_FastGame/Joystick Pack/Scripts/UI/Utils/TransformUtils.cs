﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UIHelper.Utils
{
    public class TransformUtils
    {
        public static Transform GetTransformRecursive(Transform parent, string name)
        {
            if (parent == null)
            {
                return null;
            }

            Transform result = null;
            for (int i = 0; i < parent.childCount; i++)
            {
                Transform child = parent.GetChild(i);
                if (child.name.Equals(name))
                {
                    return child;
                }
                result = GetTransformRecursive(child, name);
                if (result != null)
                {
                    return result;
                }
            }
            return result;
        }

        public static GameObject GetGameObjectRecursive(Transform parent, string name)
        {
            Transform transformResult = GetTransformRecursive(parent, name);
            if (transformResult != null)
            {
                return transformResult.gameObject;
            }
            return null;
        }

        public static T GetRecursive<T>(Transform parent, string name)
        {
            Transform transformResult = GetTransformRecursive(parent, name);
            if (transformResult != null)
            {
                return transformResult.GetComponent<T>();
            }
            return default(T);
        }

        public static List<Transform> GetAllTransformsRecursive(Transform transform, string name)
        {
            List<Transform> listTransform = new List<Transform>();

            if (transform.name.Equals(name))
            {
                listTransform.Add(transform);
            }

            foreach (Transform child in transform)
            {
                listTransform.AddRange(GetAllTransformsRecursive(child, name));
            }

            return listTransform;
        }

        public static List<GameObject> GetAllGameObjectsRecursive(Transform transform, string name)
        {
            List<GameObject> listGameObject = new List<GameObject>();

            if (transform.name.Equals(name))
            {
                listGameObject.Add(transform.gameObject);
            }

            foreach (Transform child in transform)
            {
                listGameObject.AddRange(GetAllGameObjectsRecursiveStartWith(child, name));
            }

            return listGameObject;
        }

        public static List<T> GetAllRecursive<T>(Transform parent, string name)
        {
            List<T> list = new List<T>();

            if (parent.name.Equals(name))
            {
                list.Add(parent.GetComponent<T>());
            }

            for (int i = 0; i < parent.childCount; i++)
            {
                list.AddRange(GetAllRecursive<T>(parent.GetChild(i), name));
            }

            return list;
        }

        public static List<Transform> GetAllTransformsRecursiveStartWith(Transform transform, string subName)
        {
            List<Transform> listTransform = new List<Transform>();

            if (transform.name.StartsWith(subName))
            {
                listTransform.Add(transform);
            }

            foreach (Transform child in transform)
            {
                listTransform.AddRange(GetAllTransformsRecursiveStartWith(child, subName));
            }

            return listTransform;
        }

        public static List<GameObject> GetAllGameObjectsRecursiveStartWith(Transform transform, string subName)
        {
            List<GameObject> listGameObject = new List<GameObject>();

            if (transform.name.StartsWith(subName))
            {
                listGameObject.Add(transform.gameObject);
            }

            foreach (Transform child in transform)
            {
                listGameObject.AddRange(GetAllGameObjectsRecursiveStartWith(child, subName));
            }

            return listGameObject;
        }

        public static List<T> GetAllRecursiveStartWith<T>(Transform parent, string subName)
        {
            List<T> list = new List<T>();

            if (parent.name.StartsWith(subName))
            {
                list.Add(parent.GetComponent<T>());
            }

            for (int i = 0; i < parent.childCount; i++)
            {
                list.AddRange(GetAllRecursiveStartWith<T>(parent.GetChild(i), subName));
            }

            return list;
        }

        public static (List<Image>, List<TextMeshProUGUI>) GetAllImagesAndTexts(Transform parent)
        {
            List<Image> listImg = new List<Image>();
            List<TextMeshProUGUI> listTxt = new List<TextMeshProUGUI>();

            for (int i = 0; i < parent.childCount; i++)
            {
                Image img = parent.GetChild(i).GetComponent<Image>();
                if (img != null)
                {
                    listImg.Add(img);
                }

                TextMeshProUGUI txt = parent.GetChild(i).GetComponent<TextMeshProUGUI>();
                if (txt != null)
                {
                    listTxt.Add(txt);
                }

                var list = GetAllImagesAndTexts(parent.GetChild(i));
                listImg.AddRange(list.Item1);
                listTxt.AddRange(list.Item2);
            }

            return (listImg, listTxt);
        }
    }
}
