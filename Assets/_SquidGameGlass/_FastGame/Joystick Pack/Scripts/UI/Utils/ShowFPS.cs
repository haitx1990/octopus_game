﻿using System.Collections;
using UnityEngine;

namespace UIHelper.Utils
{
    public class ShowFPS : MonoBehaviour
    {
        private float _deltaTime;
        private GUIStyle style;
        private Rect rect, rect1;

        float msec;
        float fps;
        string text;

        private static ShowFPS instance;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            int w = Screen.width, h = Screen.height;
            style = new GUIStyle();
            rect = new Rect(0, 0, w, h * 2 / 100);
            rect1 = new Rect(0, 40, w, h * 2 / 100);

            style.alignment = TextAnchor.UpperLeft;
            style.fontSize = h * 2 / 60;
            style.normal.textColor = new Color(0.0f, 1.0f, 0.0f, 1.0f);
        }

        void Update()
        {
            _deltaTime += (Time.unscaledDeltaTime - _deltaTime) * 0.1f;
        }
        void OnGUI()
        {

            if (Time.frameCount % 5 == 0)
            {
                msec = _deltaTime * 1000.0f;
                fps = 1.0f / _deltaTime;
                text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            }
            GUI.Label(rect, text, style);

            GUI.Label(rect1, string.Format("ShaderLevel:{0}\n graphicsMemorySize: {1}\n processorCount: {2}\n processorFrequency: {3}",
                                           SystemInfo.graphicsShaderLevel,
                                           SystemInfo.graphicsMemorySize,
                                           SystemInfo.processorCount,
                                           SystemInfo.processorFrequency
                                           ), style);

        }
    }
}