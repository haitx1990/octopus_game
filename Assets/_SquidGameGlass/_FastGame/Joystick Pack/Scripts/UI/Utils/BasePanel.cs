﻿using UIHelper.UI.Animation;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace UIHelper.UI.Home
{
    public abstract class BasePanel
    {

        public GameObject Panel;

        protected abstract string GetPrefabName();
        public abstract void SetActive();
        public abstract void SetDeactive();

        protected void ResetPanelAnchored()
        {
            Panel.GetRectTransform().anchoredPosition = Vector2.zero;
            Panel.GetRectTransform().localScale = Vector3.one;
        }

        //public BasePanel(HomeManager homeMng, Transform root)
        //{
        //    var prefab = Resources.Load<GameObject>(string.Format("Home/{0}", GetPrefabName()));
        //    prefab.SetActive(false);
        //    if (ReferenceEquals(prefab, null))
        //    {
        //        Debug.LogErrorFormat("Error! Can't find prefab \"{0}\" in assets.", GetPrefabName());
        //        return;
        //    }
        //    Panel = Object.Instantiate(prefab, root);
        //    Panel.onDeactive();

        //    this.homeMng = homeMng;
        //}
        public BasePanel(Transform root)
        {
           
        }

        public virtual void onVisible()
        {
            Panel.transform.DOScale(PresetCurves.InBounce, .35f, Vector3.one * .00001f, Vector3.one);
            Panel.transform.DOMove(PresetCurves.OutBounce, .35f, Vector2.down * 600, Vector2.zero);
        }

        public virtual void onInvisible(UnityAction complete = null)
        {
            Panel.transform.DOScale(PresetCurves.InMiddleBounce, .35f, Vector3.one, Vector3.zero).onComplete(complete);
            Panel.transform.DOMove(PresetCurves.InMiddleBounce, .35f, Vector2.zero, Vector2.down * 600);
        }
    }
}