﻿using UnityEngine;

namespace UIHelper.Utils
{
    public class CommonUtils
    {
        private static System.Random random = new System.Random();
        public static int Random(int min, int max)
        {
            return random.Next(min, max);
        }

        public static int Max(int a, int b)
        {
            if (b > a)
            {
                return b;
            }
            return a;
        }

        private static Vector2 GetBrizerPos(float smoothTime, Vector2 pos1, Vector2 pos2)
        {
            return Vector2.Lerp(pos1, pos2, smoothTime);
        }

        public static Vector2 GetBrizerRecusive(float smoothTime, params Vector2[] positions)
        {
            if (positions.Length <= 1)
            {
                return positions[0];
            }

            Vector2[] vectors = new Vector2[positions.Length - 1];
            for (int i = 1; i < positions.Length; i++)
            {
                vectors[i - 1] = GetBrizerPos(smoothTime, positions[i - 1], positions[i]);
            }
            return GetBrizerRecusive(smoothTime, vectors);

        }
        public static float Pow(float f, int exponent)
        {
            float result = 1;
            for (int i = 0; i < exponent; i++)
            {
                result *= f;
            }
            return result;
        }

        /// <summary>
        /// Find line: y = ax + b come through point1, point2
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns>a, b</returns>
        public static (float, float) LineEquation(Vector2 point1, Vector2 point2)
        {
            float a = (point1.y - point2.y) / (point1.x - point2.x);
            float b = point1.y - point1.x * a;
            return (a, b);      //line: y = ax + b;
        }

        /// <summary>
        /// Find projection of point onto a line come through point1, point2
        /// </summary>
        /// <param name="point"></param>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns>Point Projection</returns>
        public static Vector2 PointProjection(Vector2 point, Vector2 point1, Vector2 point2)
        {
            (float a, float b) = LineEquation(point1, point2);
            float x = (point.x + point.y * a - a * b) / (Pow(a, 2) + 1);
            float y = a * x + b;
            return new Vector2(x, y);
        }

        public static float VectorDistance(Vector2 point1, Vector2 point2)
        {
            return (float)System.Math.Sqrt(Pow(point1.x - point2.x, 2) + Pow(point1.y - point2.y, 2));
        }

        public static float VectorDistance(Vector3 point1, Vector3 point2)
        {
            return (float)System.Math.Sqrt(Pow(point1.x - point2.x, 2) + Pow(point1.y - point2.y, 2) + Pow(point1.z - point2.z, 2));
        }

        public static bool IsLessThanOne(float f1)
        {
            return f1 < 1f;
        }
        public static bool IsGreaterThanZero(float f1)
        {
            return f1 > 0f;
        }

        public static float Add(float f1, float f2)
        {
            return f1 + f2;
        }

        public static float Subtract(float f1, float f2)
        {
            return f1 - f2;
        }
    }
}