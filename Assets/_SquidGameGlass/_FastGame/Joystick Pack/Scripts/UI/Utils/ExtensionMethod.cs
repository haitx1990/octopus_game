﻿using System.Collections.Generic;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIHelper
{
    public static class ExtensionMethod
    {
        #region Transform Extention

        public static Transform FindDeepChilds(this Transform transf, string key)
        {
            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(transf);
            Transform temp = null;
            while (queue.Count > 0)
            {
                temp = queue.Peek().Find(key);
                if (temp != null)
                {
                    queue.Clear();
                    return temp;
                }
                else
                {
                    foreach (Transform item in queue.Dequeue())
                    {
                        queue.Enqueue(item);
                    }
                }
            }
            Debug.LogErrorFormat("Error! Not found object with name \"{0}\" - is child of object \"{1}\"", key, transf.name);
            return null;
        }
        public static Transform FindDeepChilds(this Transform transf, params string[] keys)
        {
            Transform _transform = transf;
            for (int i = 0; i < keys.Length; i++)
            {
                _transform = _transform.FindDeepChilds(keys[i]);
                if (_transform == null) return null;
            }
            return _transform;
        }

        public static TKey FindDeepChilds<TKey>(this Transform transf, string key)
        {
            return transf.FindDeepChilds(key).GetComponent<TKey>();
        }
        public static TKey FindDeepChilds<TKey>(this Transform transf, params string[] keys)
        {
            return transf.FindDeepChilds(keys).GetComponent<TKey>();
        }
        public static GameObject FindDeepChilds(this GameObject gameObj, params string[] keys)
        {
            return gameObj.transform.FindDeepChilds(keys).gameObject;
        }
        public static TKey FindDeepChilds<TKey>(this GameObject gameObj, params string[] keys)
        {
            return gameObj.FindDeepChilds(keys).GetComponent<TKey>();
        }

        #endregion

        #region UnityAction Extension

        public static void TryInvoke(this UnityAction action)
        {
            if (action != null)
                action.Invoke();
        }
        public static void TryInvoke(this UnityAction<float> action, float value)
        {
            if (action != null)
                action.Invoke(value);
        }
        public static void TryInvoke(this UnityAction<int> action, int value)
        {
            if (action != null)
                action.Invoke(value);
        }

        #endregion

        #region UnityEngine UI Extension

        public static void SetAction(this Button button, UnityAction action)
        {
            button.onClick.RemoveListener(action);
            button.onClick.AddListener(action);
        }
        public static void SetAction(this Button button, UnityAction<int> action, int value)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() =>
            {
                action.Invoke(value);
            });
        }
        public static void SetAction(this Button button, UnityAction<Enumerate.PopupKey> action, Enumerate.PopupKey key)
        {
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(()=>
            {
                action.Invoke(key);
            });
        }
        public static void SetText<T>(this Button button, string format, params object[] args) where T : Component
        {
            (button.GetComponentInChildren<T>() as TextMeshProUGUI).SetTextFormat(format, args);
        }
        public static void SetText(this TextMeshProUGUI _tmp, object obj)
        {
            _tmp.SetText(string.Format("{0}", obj));
        }
        public static void SetTextFormat(this TextMeshProUGUI _tmp, string _format, params object[] args)
        {
            _tmp.SetText(string.Format(_format, args));
        }
        public static void SetText(this Text _txt, object obj)
        {
            _txt.text = string.Format("{0}", obj);
        }
        public static void SetText(this Text _txt, string _format, params object[] args)
        {
            _txt.text = (string.Format(_format, args));
        }

        public static Vector3 GetPosition<T>(this T obj) where T : Component
        {
            return obj.transform.position;
        }
        public static Vector3 GetAnchoredPosition<T>(this T obj) where T : Component
        {
            return obj.GetRectTransform().anchoredPosition;
        }

        #endregion

        public static void SetEnable<T>(this T component) where T : MonoBehaviour
        {
            component.enabled = true;
        }
        public static void SetDisable<T>(this T component) where T : MonoBehaviour
        {
            component.enabled = false;
        }
        public static void TryEnable<T>(this T component) where T : MonoBehaviour
        {
            if (component != null)
                component.enabled = true;
        }
        public static void TryDisable<T>(this T component) where T : MonoBehaviour
        {
            if (component != null)
                component.enabled = false;
        }

        public static void onActive<T>(this T obj) where T : Component
        {
            obj.gameObject.SetActive(true);
        }
        public static void onDeactive<T>(this T obj) where T : Component
        {
            obj.gameObject.SetActive(false);
        }
        public static void onActive(this GameObject obj)
        {
            obj.SetActive(true);
        }
        public static void onDeactive(this GameObject obj)
        {
            obj.SetActive(false);
        }
        public static RectTransform GetRectTransform<T>(this T component) where T : Component
        {
            return component.GetComponent<RectTransform>();
        }
        public static RectTransform GetRectTransform(this GameObject component)
        {
            return component.GetComponent<RectTransform>();
        }

        public static int Abs(this int value)
        {
            return value >= 0 ? value : -value;
        }
        public static float Abs(this float value)
        {
            return value >= 0 ? value : -value;
        }
        public static float Claim(this float value, float minValue = 0, float maxValue = 1)
        {
            if (value < minValue) value = minValue;
            if (value > maxValue) value = maxValue;
            return value;
        }

        public static Vector3 GetRandomPointInsideCollider(this BoxCollider boxCollider)
        {
            Vector3 extents = boxCollider.size / 2f;
            Vector3 point = new Vector3(
                Random.Range(-extents.x, extents.x),
                Random.Range(-extents.y, extents.y),
                Random.Range(-extents.z, extents.z)
            );

            return boxCollider.transform.TransformPoint(point);
        }

        #region Coroutine Methods
        public static Coroutine StartCoroutine<T>(this T obj, IEnumerator routine) where T : class
        {
            return null;
        }

       
        #endregion

        #region Array Methods

        public static T[] FindAll<T>(this T[] array, System.Predicate<T> match)
        {
            return System.Array.FindAll(array, match);
        }

        #endregion

        #region List Extension
        public static List<T> ToList<T>(this T obj)
        {
            List<T> list = new List<T>();
            list.Add(obj);
            return list;
        }
        #endregion

        #region Common Extension
        public static void print<T>(this T _class, LogType type, string formatString, params object[] args) where T : class
        {
            Debug.LogFormat(type, LogOption.None, null, formatString, args);
        }

        #endregion
    }
}