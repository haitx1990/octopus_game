﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupBase : MonoBehaviour{
    public PopupUI idPopup;
    public bool isCloseByEscape;
    public bool isOpened;
    public bool isHireUIForm;

    [HideInInspector]
    public Action OnClosed;

    public virtual void Show()
    {
        if (!isHireUIForm)
        {
            SceneManager.ins.HideForm(true);
        }
        isOpened = true;
        gameObject.SetActive(true);
    }

    public virtual void Close(bool showUIForm = true)
    {
        isOpened = false;
        if (OnClosed != null)
        {
            OnClosed();
            OnClosed = null;
        }
        if (showUIForm && !isHireUIForm)
        {
            SceneManager.ins.HideForm(false);
        }
        gameObject.SetActive(false);
    }
}
