﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Form_Home : FormBase
{
    private int AIPlayer_Show;
    private int piggyBank_Case;

    public GameObject obj_Gold;
    public TextMeshProUGUI txt_Gold;

    public TextMeshProUGUI txt_Level;
    public TextMeshProUGUI txt_Day;
    public TextMeshProUGUI txt_PiggyBank;
    public GameObject obj_BtnSkin;

    public PiggyBankControl piggyBankControl;
    public MyPlayer_Home myPlayerHome;
    public camFollower camFollower;

    public AIPlayer_Home[] listAIPlayer;
    public GameObject fakeLoading;

    private void Awake()
    {
        if (GameManager.ins == null)
        {
            SceneManager.ins.ChangeForm(FormUI.Form_Loading.ToString());
            return;
        }
        fakeLoading.SetActive(true);
    }

    public override void Show()
    {
        base.Show();
        if (GameManager.ins == null || DataManager.ins == null) return;
        txt_Level.text = "Level " + (DataManager.ins.gameSave.level + 1);
        txt_Gold.text = DataManager.ins.gameSave.gold + "";
        txt_Day.text = "DAY " + (DataManager.ins.gameSave.level + 1);
        //Tùy theo số ngày mà hiển thị số lượng AIPlayer còn lại
        switch(DataManager.ins.gameSave.level) {
            case 0:
                AIPlayer_Show = listAIPlayer.Length;
                break;
            case 1:
                AIPlayer_Show = Mathf.Clamp(listAIPlayer.Length - 2, 3, listAIPlayer.Length);
                break;
            case 2:
                AIPlayer_Show = Mathf.Clamp(listAIPlayer.Length - 4, 3, listAIPlayer.Length);
                break;
            case 3:
                AIPlayer_Show = Mathf.Clamp(listAIPlayer.Length - 6, 3, listAIPlayer.Length);
                break;
            case 4:
                AIPlayer_Show = Mathf.Clamp(listAIPlayer.Length - 8, 3, listAIPlayer.Length);
                break;
            default:
                AIPlayer_Show = 1;
                break;
        }
        for(int i = 0; i < listAIPlayer.Length; i++) {
            listAIPlayer[i].gameObject.SetActive(i < AIPlayer_Show);
        }

        //Bật lại nhạc nền bị tắt ở FormGamePlay
        GameManager.ins.soundManager.Enable();

        //Nếu Player bị reset về Day 1 thì xóa hết tiền trong PiggyBank
        if (DataManager.ins.gameSave.level == 0)
        {
            piggyBankControl.DeleteData();
        } else { //Load lại lượng tiền trong PiggyBank
            piggyBankControl.load_and_init();
            //Rơi thêm tiền thưởng nếu PiggyBank chưa đủ
            piggyBank_Case = (DataManager.ins.gameSave.piggyBank + DataManager.ins.gameSave.piggyBank_Case) / Constant.moneyPlayerDie * 3 - piggyBankControl.Money_Count();
            if (piggyBank_Case > 0) piggyBankControl.SpawnMoney(piggyBank_Case * 0.12f, 0.12f);
        }

        DataManager.ins.gameSave.piggyBank = DataManager.ins.gameSave.piggyBank + DataManager.ins.gameSave.piggyBank_Case;
        DataManager.ins.gameSave.piggyBank_Case = 0;
        txt_PiggyBank.text = (DataManager.ins.gameSave.piggyBank) + "$";
    }

    #region Button
    public void Btn_Play()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();
        DataManager.ins.gameSave.playTime++;
        DataManager.ins.gameSave.playTime_Session++;
        DataManager.ins.SaveGame();
        FirebaseManager.ins.level_start(GameManager.ins.listMiniGame[DataManager.ins.gameSave.level].ToString());
        if (piggyBankControl.coroutine_Spawn != null) StopCoroutine(piggyBankControl.coroutine_Spawn);
        //Chuyển Scene
        GameManager.ins.formGamePlay = GameManager.ins.levelCheat != MiniGame.None ? GameManager.ins.levelCheat : GameManager.ins.listMiniGame[ DataManager.ins.gameSave.level ];
        if(GameManager.ins.isLoading2) {//Nếu bật Loading2 thì phải qua màn Loading2 trc rồi mới vào Minigame
            SceneManager.ins.ChangeForm(FormUI.Form_Loading2.ToString());
        } else {//Nếu ko thì vào luôn Minigame
            SceneManager.ins.ChangeForm(GameManager.ins.formGamePlay.ToString(), 0);
        }
    }

    public void Btn_Skin()
    {

        GameManager.ins.soundManager.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Skin();
    }

    public void Btn_Settings()
    {

        GameManager.ins.soundManager.sound_Click.PlaySound();
        SceneManager.ins.ShowPopup_Settings();
    }
    #endregion
}
