﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_Settings : PopupBase
{
    public GameObject obj_Vibration_On;
    public GameObject obj_Vibration_Off;
    public Slider slider_Music;
    public Slider slider_Sound;

    //Cheat
    public GameObject obj_Cheat;
    //public Toggle toggle_Gold;
    //public Toggle toggle_HideUI;
    //public Toggle toggle_AllSkins;
    public TMP_InputField inputField_Levels;

    public override void Show()
    {
        base.Show();
        slider_Music.value = DataManager.ins.gameSave.volumeMusic;
        slider_Sound.value = DataManager.ins.gameSave.volumeSound;

        obj_Vibration_On.SetActive(!DataManager.ins.gameSave.isVibrate);
        obj_Vibration_Off.SetActive(DataManager.ins.gameSave.isVibrate);

        if(GameManager.ins.isBuildMarketing && obj_Cheat != null)
        {
            obj_Cheat.gameObject.SetActive(true);
            //toggle_HideUI.isOn = GameManager.ins.isHideUI;
            //toggle_Gold.isOn = false;
            //toggle_AllSkins.isOn = false;
            inputField_Levels.text = (int)GameManager.ins.levelCheat + "";
        }
        else if (obj_Cheat != null)
        {
            obj_Cheat.gameObject.SetActive(false);
        }
    }

    #region BUTTON
    public void BtnReloadMusic() {
        DataManager.ins.gameSave.volumeMusic = (int)(slider_Music.value);
        DataManager.ins.SaveGame();
        if (GameManager.ins.soundManager != null)
            GameManager.ins.soundManager.ReloadMusic();
    }


    public void BtnReloadSound() {
        DataManager.ins.gameSave.volumeSound = (int) (slider_Sound.value);
        DataManager.ins.SaveGame();
    }

    public void BtnVibration() {
            GameManager.ins.soundManager.sound_Click.PlaySound();
        DataManager.ins.gameSave.isVibrate = !DataManager.ins.gameSave.isVibrate;
        obj_Vibration_On.SetActive(DataManager.ins.gameSave.isVibrate);
        obj_Vibration_Off.SetActive(!DataManager.ins.gameSave.isVibrate);
    }

    public void BtnClose()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();
        Close();
        DataManager.ins.SaveGame();
    }

    
    public void BtnCheat()
    {
        /*GameManager.ins.isHideUI = toggle_HideUI.isOn;
        if (toggle_Gold.isOn)  DataManager.ins.gameSave.gold = 99999;
        if (toggle_AllSkins.isOn)
        {
            for (int i = 0; i < DataManager.ins.gameSave.listSkins.Length; i++)
            {
                DataManager.ins.gameSave.listSkins[i].isUnlock = true;
            }
        }*/
        GameManager.ins.levelCheat = (MiniGame) Mathf.Clamp(int.Parse(inputField_Levels.text), 0, System.Enum.GetValues(typeof(MiniGame)).Length);
        //SceneManager.ins.ChangeForm(FormUI.Form_Home);
    }
}
    #endregion

