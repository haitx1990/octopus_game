﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_Revive : PopupBase {
    public int time;
    public bool isWacthingVideoAds = false;
    public TextMeshProUGUI txt_Time;
    public TextMeshProUGUI txt_Money;
    public DOTweenAnimation twn_Countdown;
    public Image image_ButtonFree;
    public GameObject obj_LoseIt;
    public SoundObject soundCountdown;

    public override void Show()
    {
        base.Show();
        time = 3;
        txt_Money.text = "" + Constant.costRevive;
        txt_Time.text = time + "";

        soundCountdown.PlaySound();//bật tiếng Countdown time
        twn_Countdown.DORestart();//Ảnh loading quay hết một vòng thì tự động gọi hàm CountTime() 1 lần
        SceneManager.ins.isPause = true;
        isWacthingVideoAds = false;
        //Thực hiện riêng cho game Đèn Xanh Đèn Đỏ
        if (SceneManager.ins.form_GamePlay.miniGame == MiniGame.RedLightGreenLight)
        {
            SceneManager.ins.form_GamePlay.soundLast10Serconds.Pause();
            SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioSing.Pause();
            SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioTurnAround.Pause();
        }
    }

    public void CountTime() {
        if(isWacthingVideoAds) return;//Nếu đang xem VideoAds thì bỏ qua lần đếm này
        //Check VideoAds
        if(MaxManager.Ins.isRewardedVideoAvailable() || Application.isEditor) {//Nếu VideoAds sẵn sàng -> Hiển thị button sáng lên
            image_ButtonFree.color = Color.white;
        } else {//Nếu Chưa sẵn sàng -> Button Video Ads mờ đi
            image_ButtonFree.color = Color.gray;
        }
        time--;
        txt_Time.text = time + "";
        if (time < 0 && SceneManager.ins.isPause) { //Hết thời gian thì bật Popup thua lên
            Close();
            SceneManager.ins.isPause = false;
            //Thực hiện riêng cho game Đèn Xanh Đèn Đỏ
            if (SceneManager.ins.form_GamePlay.miniGame == MiniGame.RedLightGreenLight)
            {
                SceneManager.ins.form_GamePlay.soundLast10Serconds.UnPause();
                SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioSing.UnPause();
                SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioTurnAround.UnPause();
            }
            SceneManager.ins.ShowPopup_EndGame(false);
        } else {//Nếu chưa hết thời gian thì bật tiếng Countdown time
            soundCountdown.PlaySound();
        }
    }

    #region Button
    public void Btn_ReviveByAds() {
        
        GameManager.ins.soundManager.sound_Click.PlaySound();
        isWacthingVideoAds = true;
        MaxManager.Ins.ShowRewardedAd("Revive", () =>
        {
            isWacthingVideoAds = false;
            Close();
            SceneManager.ins.isPause = false;
            //Thực hiện riêng cho game Đèn Xanh Đèn Đỏ
            if (SceneManager.ins.form_GamePlay.miniGame == MiniGame.RedLightGreenLight)
            {
                SceneManager.ins.form_GamePlay.soundLast10Serconds.UnPause();
                SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioSing.UnPause();
                SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioTurnAround.UnPause();
                SceneManager.ins.form_GamePlay.redLightGreenLight.myPlayer.Reborn();
            }
            if(_c != null)
            {
                _c.Invoke();
                _c = null;
            }
            FirebaseManager.ins.ads_reward_complete("Revive By Ads");
            
        });
    }

    Action _c;
    public void SetReborn(Action c)
    {
        _c = c;
    }

    public void Btn_LoseIt()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();
        Close();
        SceneManager.ins.isPause = false;
        //Thực hiện riêng cho game Đèn Xanh Đèn Đỏ
        if (SceneManager.ins.form_GamePlay.miniGame == MiniGame.RedLightGreenLight)
        {
            SceneManager.ins.form_GamePlay.soundLast10Serconds.UnPause();
            SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioSing.UnPause();
            SceneManager.ins.form_GamePlay.redLightGreenLight.doll.audioTurnAround.UnPause();
        }
        SceneManager.ins.ShowPopup_EndGame(false);
    }

    /*
    public void Btn_ReviveByGold() {
        
            GameManager.ins.soundManager.sound_Click.PlaySound();
        //Trừ tiền nếu đủ -> hồi sinh User
        if(DataManager.ins.gameSave.gold >= Constant.costRevive) {
            Form_Gameplay.isStartRevive = true;//Chạy 1 phần dẫn chuyện vì revive
            DataManager.ins.ChangeGold(-(Constant.costRevive), "Revive");
            Close();
            SceneManager.ins.ChangeForm(FormUI.Form_GamePlay);
        }
    }

    public void Btn_Home()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();
        MaxManager.Ins.ShowInterstitial("Fail", () => {
            GameManager.ins.levelCur = DataManager.ins.gameSave.level;
            GameManager.ins.stageCur = 0;
            GameManager.ins.chooseCur = -1;
            DataManager.ins.gameSave.playTime++;
            DataManager.ins.gameSave.playTime_Session++;
            DataManager.ins.SaveGame();
            Close();
            SceneManager.ins.ChangeForm(FormUI.Form_Home);
        }, () => {

        });
    }*/
    #endregion
}

