﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_Skin : PopupBase {
    private bool isInit = false;

    public int tab = 0;//0 là chọn tóc, 1 là chọn body

    public int idSelecting_Hair = 0;
    public int idSelecting_Body = 0;

    public Card_Skin card_SkinBase;
    public Sprite sprite_Unlock;
    public Sprite sprite_Lock;

    public Transform tran_ListCardSkins_Hair;
    public Transform tran_ListCardSkins_Body;
    public GameObject obj_ListCardSkins_Hair;
    public GameObject obj_ListCardSkins_Body;

    public List<Card_Skin> listSkins_Hair;
    public List<Card_Skin> listSkins_Body;

    public GameObject obj_TapHair_On;
    public GameObject obj_TapBody_On;
    public GameObject obj_TapHair_Off;
    public GameObject obj_TapBody_Off;

    public GameObject obj_BtnSkinLock;
    public GameObject obj_BtnSkinUnlock;
    public GameObject obj_OneTime;
    public TextMeshProUGUI txt_Cost;


    public override void Show()
    {
        base.Show();
        tab = 0;
        obj_ListCardSkins_Hair.SetActive(true);
        obj_ListCardSkins_Body.SetActive(false);
        obj_TapHair_On.SetActive(true);
        obj_TapBody_On.SetActive(false);
        obj_TapHair_Off.SetActive(false);
        obj_TapBody_Off.SetActive(true);

        if (!isInit) listSkins_Hair = new List<Card_Skin>();
        if (!isInit) listSkins_Body = new List<Card_Skin>();
        idSelecting_Hair = DataManager.ins.gameSave.idSkin_Hair;
        idSelecting_Body = DataManager.ins.gameSave.idSkin_Body;

        //Instance các Card_Skin và Truyền thông tin vào các Card_Skin Hair
        for (int i = 0; i < GameManager.ins.listSkins_Hair.Length; i++)
        {
            if (!isInit){
                Card_Skin card = Instantiate(card_SkinBase, tran_ListCardSkins_Hair);
                card.popup_Skin = this;
                listSkins_Hair.Add(card);
            }
            listSkins_Hair[i].idCard = i;
            listSkins_Hair[i].image_BG.sprite = DataManager.ins.gameSave.listSkins_Hair[i].isUnlock ? sprite_Unlock : sprite_Lock;
            listSkins_Hair[i].image_Avatar.sprite = GameManager.ins.listSkins_Hair[i].sprite_Icon;
            listSkins_Hair[i].obj_Selecting.SetActive(idSelecting_Hair == i);
            listSkins_Hair[i].obj_Wearing.SetActive(DataManager.ins.gameSave.idSkin_Hair == i);
        }

        //Instance các Card_Skin và Truyền thông tin vào các Card_Skin Body
        for (int i = 0; i < GameManager.ins.listSkins_Body.Length; i++)
        {
            if (!isInit){
                Card_Skin card = Instantiate(card_SkinBase, tran_ListCardSkins_Body);
                card.popup_Skin = this;
                listSkins_Body.Add(card);
            }
            listSkins_Body[i].idCard = i;
            listSkins_Body[i].image_BG.sprite = DataManager.ins.gameSave.listSkins_Body[i].isUnlock ? sprite_Unlock : sprite_Lock;
            listSkins_Body[i].image_Avatar.sprite = GameManager.ins.listSkins_Body[i].sprite_Icon;
            listSkins_Body[i].obj_Selecting.SetActive(idSelecting_Body == i);
            listSkins_Body[i].obj_Wearing.SetActive(DataManager.ins.gameSave.idSkin_Body == i);
        }

        ClickToSkin_Hair(idSelecting_Hair);
        //Camera đẩy nhân vật lên
        if(SceneManager.ins.form_Home!= null) SceneManager.ins.form_Home.camFollower.state = 2;
        isInit = true;
    }

    private void ClickToSkin_Hair(int idSkin)
    {
        //Tắt hiệu ứng ở Card Selecting cũ
        listSkins_Hair[idSelecting_Hair].obj_Selecting.SetActive(false);
        //Bật hiệu ứng ở Card Selecting mới
        if(DataManager.ins.gameSave.listSkins_Hair[idSkin].isUnlock) {
            listSkins_Hair[DataManager.ins.gameSave.idSkin_Hair].obj_Wearing.SetActive(false);
            DataManager.ins.gameSave.idSkin_Hair = idSkin;
            if (SceneManager.ins.form_Home != null) SceneManager.ins.form_Home.myPlayerHome.playerSkinManager.idSkin_Hair = DataManager.ins.gameSave.idSkin_Hair;
            listSkins_Hair[idSkin].obj_Wearing.SetActive(true);
        }
        idSelecting_Hair = idSkin;
        listSkins_Hair[idSelecting_Hair].obj_Selecting.SetActive(true);
        listSkins_Hair[idSelecting_Hair].image_BG.sprite = DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].isUnlock ? sprite_Unlock : sprite_Lock;
        obj_BtnSkinLock.SetActive(!DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].isUnlock);
        obj_BtnSkinUnlock.SetActive(DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].isUnlock);
        obj_OneTime.SetActive(DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].amountVideAds > 0);
        txt_Cost.text = "" + GameManager.ins.listSkins_Hair[idSelecting_Hair].cost;

        //Mặc thử skin
        SceneManager.ins.form_Home.myPlayerHome.playerSkinManager.WearSkin_Force(idSkin,-1);
    }

    private void ClickToSkin_Body(int idSkin)
    {
        //Tắt hiệu ứng ở Card Selecting cũ
        listSkins_Body[idSelecting_Body].obj_Selecting.SetActive(false);
        //Bật hiệu ứng ở Card Selecting mới
        if(DataManager.ins.gameSave.listSkins_Body[idSkin].isUnlock) {
            listSkins_Body[DataManager.ins.gameSave.idSkin_Body].obj_Wearing.SetActive(false);
            DataManager.ins.gameSave.idSkin_Body = idSkin;
            if(SceneManager.ins.form_Home != null) SceneManager.ins.form_Home.myPlayerHome.playerSkinManager.idSkin_Body = DataManager.ins.gameSave.idSkin_Body;
            listSkins_Body[idSkin].obj_Wearing.SetActive(true);
        }
        idSelecting_Body = idSkin;
        listSkins_Body[idSelecting_Body].obj_Selecting.SetActive(true);
        listSkins_Body[idSelecting_Body].image_BG.sprite = DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].isUnlock ? sprite_Unlock : sprite_Lock;
        obj_BtnSkinLock.SetActive(!DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].isUnlock);
        obj_BtnSkinUnlock.SetActive(DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].isUnlock);
        obj_OneTime.SetActive(DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].amountVideAds > 0);
        txt_Cost.text = "" + GameManager.ins.listSkins_Body[idSelecting_Body].cost;

        //Mặc thử skin
        SceneManager.ins.form_Home.myPlayerHome.playerSkinManager.WearSkin_Force( -1, idSkin);
    }

    public override void Close(bool showUIForm = true){
        base.Close();
        //Camera về lại như cũ
        if (SceneManager.ins.form_Home != null) SceneManager.ins.form_Home.camFollower.state = 1;
        //Load lại skin đang mặc
        SceneManager.ins.form_Home.myPlayerHome.playerSkinManager.ReloadSkin();
    }

    #region Button
    public void Btn_ChangeTab(int idTab){
        GameManager.ins.soundManager.sound_Click.PlaySound();
        tab = idTab;
        if (tab == 0) {
            obj_ListCardSkins_Hair.SetActive(true);
            obj_ListCardSkins_Body.SetActive(false);
            obj_TapHair_On.SetActive(true);
            obj_TapBody_On.SetActive(false);
            obj_TapHair_Off.SetActive(false);
            obj_TapBody_Off.SetActive(true);
        } else {
            obj_ListCardSkins_Hair.SetActive(false);
            obj_ListCardSkins_Body.SetActive(true);
            obj_TapHair_On.SetActive(false);
            obj_TapBody_On.SetActive(true);
            obj_TapHair_Off.SetActive(true);
            obj_TapBody_Off.SetActive(false);
        }
    }

    public void Btn_SelectSkin(Card_Skin card) {
        GameManager.ins.soundManager.sound_Click.PlaySound();
            if (tab == 0)
            {
                ClickToSkin_Hair(card.idCard);
            } else {
                ClickToSkin_Body(card.idCard);
            }
    }

    public void Btn_Buy(){
        GameManager.ins.soundManager.sound_Click.PlaySound();
        if (tab == 0) {
            //Trừ tiền nếu đủ -> hồi sinh User
            if (DataManager.ins.gameSave.gold >= GameManager.ins.listSkins_Hair[idSelecting_Hair].cost) {
                GameManager.ins.soundManager.sound_Click.PlaySound();
                DataManager.ins.ChangeGold(-(GameManager.ins.listSkins_Hair[idSelecting_Hair].cost), "BuySkinHair_Gold");
                DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].amountVideAds = 0;
                DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].isUnlock = true;
                ClickToSkin_Hair(idSelecting_Hair);
            }
        } else {
            //Trừ tiền nếu đủ -> hồi sinh User
            if (DataManager.ins.gameSave.gold >= GameManager.ins.listSkins_Body[idSelecting_Body].cost) {
                GameManager.ins.soundManager.sound_Click.PlaySound();
                DataManager.ins.ChangeGold(-(GameManager.ins.listSkins_Body[idSelecting_Body].cost), "BuySkinBody_Gold");
                DataManager.ins.gameSave.listSkins_Hair[idSelecting_Body].amountVideAds = 0;
                DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].isUnlock = true;
                ClickToSkin_Body(idSelecting_Body);
            }
        }
        
    }

    public void Btn_VideoAds(){
        GameManager.ins.soundManager.sound_Click.PlaySound();
        if (tab == 0) {
            MaxManager.Ins.ShowRewardedAd("BuySkinHair_VideoAds", () =>
            {
                DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].amountVideAds++;
                DataManager.ins.gameSave.listSkins_Hair[idSelecting_Hair].isUnlock = true;
                ClickToSkin_Hair(idSelecting_Hair);
                FirebaseManager.ins.ads_reward_complete("SkinHair");
            });
        } else {
            MaxManager.Ins.ShowRewardedAd("BuySkinBody_VideoAds", () =>
            {
                DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].amountVideAds++;
                DataManager.ins.gameSave.listSkins_Body[idSelecting_Body].isUnlock = true;
                ClickToSkin_Body(idSelecting_Body);
                FirebaseManager.ins.ads_reward_complete("SkinBody");
            });
        }
    }

    public void Btn_Close(){
        GameManager.ins.soundManager.sound_Click.PlaySound();
        Close();
    }
    #endregion
}

