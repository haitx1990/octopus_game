﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Form_Loading2 : FormBase
{

    public override void Show()
    {
        base.Show();
        //Chuyển đến minigame tiếp theo
        SceneManager.ins.ChangeForm(GameManager.ins.formGamePlay.ToString(), 2f);
    }
}
