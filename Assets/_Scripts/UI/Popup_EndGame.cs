﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_EndGame : PopupBase {
    public bool isWin = false;
    public int gold = 0; 
    private bool isVictoryAllGame = false;
    private int levelNext = 0;

    public TextMeshProUGUI txt_Top;
    public TextMeshProUGUI txt_Day;
    public TextMeshProUGUI txt_GoldReward;
    public TextMeshProUGUI txt_MultiGoldReward;
    public TextMeshProUGUI txt_Next;
    public Button button_MultiGold;

    public GameObject obj_TitleWin;
    public GameObject obj_TitleFail;

    public override void Show()
    {
        base.Show();
        txt_Day.text = "DAY " + (DataManager.ins.gameSave.level + 1);

        //Nếu skin nào dùng 1 lần thì hủy Unlock
        if (DataManager.ins.gameSave.listSkins_Hair[DataManager.ins.gameSave.idSkin_Hair].amountVideAds > 0) {
            DataManager.ins.gameSave.listSkins_Hair[DataManager.ins.gameSave.idSkin_Hair].amountVideAds = 0;
            DataManager.ins.gameSave.listSkins_Hair[DataManager.ins.gameSave.idSkin_Hair].isUnlock = false;
            DataManager.ins.gameSave.idSkin_Hair = 0;
        }
        if (DataManager.ins.gameSave.listSkins_Body[DataManager.ins.gameSave.idSkin_Body].amountVideAds > 0)
        {
            DataManager.ins.gameSave.listSkins_Body[DataManager.ins.gameSave.idSkin_Body].amountVideAds = 0;
            DataManager.ins.gameSave.listSkins_Body[DataManager.ins.gameSave.idSkin_Body].isUnlock = false;
            DataManager.ins.gameSave.idSkin_Body = 0;
        }

        if (isWin){
            try {//Bắn Firebase
                FirebaseManager.ins.OnSetUserProperty();
                FirebaseManager.ins.level_complete(GameManager.ins.listMiniGame[DataManager.ins.gameSave.level].ToString(), DataManager.ins.gameSave.dieCount_levelCur, DataManager.ins.gameSave.winMiniGameCount);
               // FirebaseManager.ins.check_point(DataManager.ins.gameSave.winMiniGameCount);
            }catch{ Debug.LogError("Lỗi bắn firebase post_event_level_end và OnSetUserProperty");}
            DataManager.ins.gameSave.dieCount_levelCur = 0;
            DataManager.ins.gameSave.failCount_levelCur = 0;
            DataManager.ins.gameSave.winMiniGameCount++;
            obj_TitleWin.SetActive(true);
            obj_TitleFail.SetActive(false);
            txt_Next.text = "CONTINUE";

            DataManager.ins.gameSave.level++;
            if (DataManager.ins.gameSave.levelMax < DataManager.ins.gameSave.level) DataManager.ins.gameSave.levelMax = DataManager.ins.gameSave.level;
            if (SceneManager.ins.form_GamePlay.top > 0) {
                txt_Top.gameObject.SetActive(true);
                txt_Top.text = "TOP #" + SceneManager.ins.form_GamePlay.top;
                gold = Constant.moneyEndgame / SceneManager.ins.form_GamePlay.top;
            } else {
                txt_Top.gameObject.SetActive(false);
                gold = Constant.moneyEndgame / 5;
            }
            //Tiền thưởng thêm vào = *Số Player đã chết trong trò chơi này * Tiền thưởng mỗi khi 1 Player chết
            if(DataManager.ins.gameSave.remainingPlayer - SceneManager.ins.form_GamePlay.alivePlayer >= 1) { 
                DataManager.ins.gameSave.piggyBank_Case = (DataManager.ins.gameSave.remainingPlayer - SceneManager.ins.form_GamePlay.alivePlayer) * Constant.moneyPlayerDie;
            } else {//Nếu minigame ko có ai chết hoặc Dev chưa code remainingPlayer thì sẽ coi như 1/2 Player chết
                DataManager.ins.gameSave.piggyBank_Case = (DataManager.ins.gameSave.remainingPlayer + 1)/ 2;
            }
            DataManager.ins.gameSave.remainingPlayer = SceneManager.ins.form_GamePlay.alivePlayer;
            txt_GoldReward.text = "+" + gold + "$";
            txt_MultiGoldReward.text = (gold * 3) + "";
            DataManager.ins.ChangeGold(gold, "EndGame");
        } else  {
            try {//Bắn Firebase
                FirebaseManager.ins.level_fail(GameManager.ins.listMiniGame[DataManager.ins.gameSave.level].ToString(), DataManager.ins.gameSave.winMiniGameCount);
            } catch{ Debug.LogError("Lỗi bắn firebase post_event_level_end và OnSetUserProperty");}
            obj_TitleWin.SetActive(false);
            obj_TitleFail.SetActive(true);
            txt_Next.text = "RESET FROM DAY 1";
            txt_Top.gameObject.SetActive(false);
            gold = 1;
            txt_GoldReward.text = "+" + gold + "$";
            txt_MultiGoldReward.text = (gold * 3) + "";
            DataManager.ins.ChangeGold(gold, "EndGame");

            //Reset lại mọi data của Level
            DataManager.ins.gameSave.level = 0;
            DataManager.ins.gameSave.piggyBank = 0;
            DataManager.ins.gameSave.piggyBank_Case = 0;
            DataManager.ins.gameSave.failCount_levelCur ++;
            DataManager.ins.gameSave.remainingPlayer = Constant.totalPlayer;
        }
        DataManager.ins.SaveGame();

        //Nếu chơi quá 2,5 phút thì Offer 1 lần
        /*
        if (GameManager.ins.timeOffer > 150)
        {
            SceneManager.ins.BlockInput(1.1f);
            GameManager.ins.timeOffer = 0;
            Timer.Schedule(this,1, ()=> { 
                SceneManager.ins.ShowPopup_Offer(); 
            }); 
        }*/
    }

    #region Button
    public void Btn_Next()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();

        MaxManager.Ins.ShowInterstitial("End Game", () => {
            SceneManager.ins.ChangeForm(FormUI.Form_Home.ToString(), 0);
        }, () => {
            //Đến số lần hiển thị Inter của 1 User
        });
    }
    /*
    public void Btn_Restart()
    {
        
            GameManager.ins.soundManager.sound_Click.PlaySound();
        MaxManager.Ins.ShowInterstitial("End Game", () => {
            GameManager.ins.stageCur = 0;
            GameManager.ins.chooseCur = -1;
            SceneManager.ins.ChangeForm(FormUI.Form_GamePlay, 0);
        }, () => {
            //Đến số lần hiển thị Inter của 1 User
        });
    }

    public void Btn_MultiReward()
    {
        
            GameManager.ins.soundManager.sound_Click.PlaySound();
        MaxManager.Ins.ShowRewardedAd("Multi Reward", () => {
            DataManager.ins.ChangeGold(SceneManager.ins.form_GamePlay.dataLevelCur.goldReward * 3, "EndGame");
            button_MultiGold.interactable = false;
            FirebaseManager.ins.ads_reward_complete("Multi Reward");
        });
    }

    public void Btn_Home()
    {
        
            GameManager.ins.soundManager.sound_Click.PlaySound();
        MaxManager.Ins.ShowInterstitial("End Game", () => {
            SceneManager.ins.ChangeForm(FormUI.Form_Home);
        }, () => {
            //Đến số lần hiển thị Inter của 1 User
        });
    }*/
    #endregion
}

