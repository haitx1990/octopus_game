﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Popup_WinAll : PopupBase {
    public int totalGold;
    public TextMeshProUGUI txt_Gold;
    public GameObject obj_EffectFirework;

    public override void Show()
    {
        base.Show();
        DataManager.ins.gameSave.victoryAllCount++;
        totalGold = DataManager.ins.gameSave.piggyBank + (DataManager.ins.gameSave.remainingPlayer - 1) * Constant.moneyPlayerDie;
        txt_Gold.text = "+" + totalGold + "$";
        DataManager.ins.ChangeGold(totalGold, "EndGame");
        //Reset lại mọi data của Level
        DataManager.ins.gameSave.level = 0;
        DataManager.ins.gameSave.piggyBank = 0;
        DataManager.ins.gameSave.piggyBank_Case = 0;
        DataManager.ins.gameSave.remainingPlayer = Constant.totalPlayer;

        Timer.Schedule(this, 0.8f, () => { obj_EffectFirework.SetActive(true); }); 
    }

    #region Button
    public void Btn_Next()
    {
        GameManager.ins.soundManager.sound_Click.PlaySound();

        MaxManager.Ins.ShowInterstitial("End Game", () => {
            SceneManager.ins.ChangeForm(FormUI.Form_Home.ToString(), 0);
        }, () => {
            //Đến số lần hiển thị Inter của 1 User
        });
    }
    #endregion
}

