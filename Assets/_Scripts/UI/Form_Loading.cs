﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Form_Loading : FormBase
{

    public override void Show()
    {
        base.Show();
        GameManager.ins.isFirstOpen_FormHome = true;
        //Nếu là lần đầu tiên mở game -> vào màn Tut hoặc vào chơi game luôn
        if (DataManager.ins.gameSave.isNew)
        {
            DataManager.ins.gameSave.isNew = false;
            DataManager.ins.gameSave.playTime++;
            DataManager.ins.gameSave.playTime_Session++;
            DataManager.ins.SaveGame();
            FirebaseManager.ins.level_start(GameManager.ins.listMiniGame[DataManager.ins.gameSave.level].ToString());
            //Chuyển Scene
            GameManager.ins.formGamePlay = GameManager.ins.levelCheat != MiniGame.None ? GameManager.ins.levelCheat : MiniGame.RedLightGreenLight;
            if (GameManager.ins.isLoading2)
            {//Nếu bật Loading2 thì phải qua màn Loading2 trc rồi mới vào Minigame
                SceneManager.ins.ChangeForm(FormUI.Form_Loading2.ToString());
            }  else{//Nếu ko thì vào luôn Minigame
                SceneManager.ins.ChangeForm(GameManager.ins.formGamePlay.ToString(), 0);
            }
        } else {//Nếu ko sẽ vào Home
            SceneManager.ins.ChangeForm(FormUI.Form_Home.ToString(), 0f);
        }
    }
}
