﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Form_GamePlay : FormBase
{
    public MiniGame miniGame;//Đang là mini game nào
    public bool isNoChangeLoading = false;

    public bool isStartGame = false;//Đã bắt đầu game chưa
    public bool isEndGame = false;//Game đã kết thúc chưa
    public int alivePlayer;//Số người chơi còn sống
    public int top = 0;//VD: User là người chiến thắng minigame đầu tiên thì top = 1

    public int timeCountdown;//Thời gian đếm ngược
    public TextMeshProUGUI txt_Time;//Text Mesh Pro hiển thị thowifgian đếm ngược
    public SoundObject soundLast10Serconds;//Hiệu ứng âm thanh khi còn 10 giây cuối

    [Header("---------------Control riêng của mỗi game ---------------")]
    public RedLightGreenLight redLightGreenLight;//Đèn Xanh Đèn Đỏ
    

    private void Awake()
    {
        if (GameManager.ins == null && !isNoChangeLoading)
        {
            SceneManager.ins.ChangeForm(FormUI.Form_Loading.ToString());
            return;
        }
       
    }

    public override void Show()
    {
        base.Show();
        if (GameManager.ins == null) return;
        //Tắt nhạc nền
        GameManager.ins.soundManager.Disable();
    }
}
