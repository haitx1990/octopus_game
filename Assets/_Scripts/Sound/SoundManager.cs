﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public Data_Sound dataMusic;

    [Header("--------------- Quản lý một vài Sound đơn khác --------")]
    public SoundObject sound_Click;
    //public SoundObject sound_ClickBuy;
    //public AudioSource audio_Lose;
    //public AudioSource audio_Win;

    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }


    private void OnEnable() {
        if(dataMusic != null && DataManager.ins != null && DataManager.ins.gameSave != null && dataMusic.clip != null) {
            audioSource.clip = dataMusic.clip;
            audioSource.volume = dataMusic.volume * DataManager.ins.gameSave.volumeMusic * 0.01f;
            audioSource.Play();
        }
    }

    public void ReloadMusic() {
        if(dataMusic != null && DataManager.ins != null && DataManager.ins.gameSave != null && dataMusic.clip != null) {
            audioSource.volume = dataMusic.volume * DataManager.ins.gameSave.volumeMusic * 0.01f;
        }
    }

    public void Enable() {
        audioSource.enabled = true;
    }

    public void Disable() {
        audioSource.enabled = false;
    }

}
