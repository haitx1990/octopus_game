﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {
    public int amountRelease ;

    public GameObject obj_All;
    public GameObject obj_Hand;
    //public Animator animator;

    // Use this for initialization
    void Start () {
        amountRelease = 0;
    }

    private void Update() {
        //Hiển thị tốt đa tut 3 lần
        if ( amountRelease < 3 && !SceneManager.ins.obj_BlockInput.activeSelf)
        {
            if (Input.GetMouseButtonDown(0)) {
                amountRelease++;
                obj_All.SetActive(false);
                if(!SceneManager.ins.form_GamePlay.isStartGame) { SceneManager.ins.form_GamePlay.redLightGreenLight.StartGame(); }
            }
            if (Input.GetMouseButton(0)) {
                obj_All.SetActive(false);
            }// Nếu Player vượt qua vạch xuất phát thì ko hiện tut nữa
            if (Input.GetMouseButtonUp(0) && SceneManager.ins.form_GamePlay.redLightGreenLight.myPlayer.transform.localPosition.z < -105) {
                obj_All.SetActive(true);
            }
            
        }
        if (amountRelease >= 3) {
            gameObject.SetActive(false);
        }
    }
}
