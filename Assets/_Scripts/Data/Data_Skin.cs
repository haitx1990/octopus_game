﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data_Skin", menuName = "Assets/Data_Skin", order = 1)]
public class Data_Skin : ScriptableObject {
    public string keyID;
    public string nameCharSkin;
    public int cost;
    //public bool isUnlockByVideoAds;
    public Sprite sprite_Icon;

    //public GameObject obj_Hat;
    //public Texture texure_Skin;//Texture Skin

    //public Vector3 pos_Skin;
    //public Vector3 rot_Skin;
    //public float sca_Skin;
}
