﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;

public class GameManager : MonoBehaviour {
    public static GameManager ins;
    
    [Header("--------------- Cheat & Hack --------")]
    public MiniGame levelCheat = MiniGame.None;//Cheat Levels
    public bool isBuildMarketing = false;//No UIGameplay, No Ads, cheat Gold, cheat Levels
    public bool isHideUI = false;//No UIGameplay
    public bool isSkipAds = false;//No Ads
    public bool isUndying = false;//Bất tử
    public bool isLoading2 = false;
    public MiniGame[] listMiniGame;//Danh sách thứ tự chơi các minigame

    [Header("--------------- Trạng thái đặc biệt --------")]
    [HideInInspector] public bool isIOS = false;
    [HideInInspector] public bool isShowingBanner = false;//Đang show banner
    [HideInInspector] public bool isFirstOpen_FormHome = false;//Lần đầu tiên mở FormMainMenu

    [Header("--------------- Prefab Popup --------")]
    public Popup_Rate popup_Rate;
    public Popup_EndGame popup_EndGame;
    public Popup_Revive popup_Revive;
    public Popup_Settings popup_Settings;
    public Popup_Skin popup_Skin;
    public Popup_Offer popup_Offer;
    public Popup_WinAll popup_WinAll;

    [Header("-------- List Data --------")]
    public Data_Skin[] listSkins_Hair;
    public Data_Skin[] listSkins_Body;

    [Header("--------------- Other --------")]
    public MiniGame formGamePlay;//Khi vào Loading2 sẽ dựa vào giá trị này để vào minigame tương ứng
    public SoundManager soundManager;
    public int timeOffer = 0;
    public int timeCheckPoint = 0;

    #region Unity
    private void Awake()
    {
        if (ins != null)
        {
            Destroy(this.gameObject);
            return;
        }
        ins = this;
        DontDestroyOnLoad(this.gameObject);
        //Khởi tạo sớm 1 số giá trị
        isIOS = (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor);
        isShowingBanner = false;
    }

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = Constant.frameRate;
        //Load data
        DataManager.ins.LoadData();
        //Bật Music
        soundManager.enabled = true;
        //Hiển thị Banner
        CheckBanner();
        //Đếm thời gian chơi
        StartCoroutine(CountTime());
    }
    #endregion

    #region Private
    IEnumerator CountTime()
    {
        yield return new WaitForSecondsRealtime(1f);
        timeOffer++;
        if (DataManager.ins.gameSave.sessionCount == 0) {
            timeCheckPoint++;
            //Firebase 30 checkpoint cách nhau 30s.Bắn thêm Game hiện tại và game cuối cùng
            if (timeCheckPoint % 30 == 0 && timeCheckPoint / 30 <= 30)
                FirebaseManager.ins.check_point(timeCheckPoint / 30, GameManager.ins.listMiniGame[DataManager.ins.gameSave.level].ToString(), GameManager.ins.listMiniGame[DataManager.ins.gameSave.levelMax].ToString());
        }
        //Nếu đang chơi levelLast và levelLast này chưa bắn Firebase -> Đếm thời gian chơi (Để sau này gửi Firebase)
        //if (SceneManager.ins.formCurrent.idForm == FormUI.Form_GamePlay) DataManager.ins.gameSave.seconds_levelCur++;
        StartCoroutine(CountTime());
    }
    #endregion

    #region Public
    public void CheckBanner()
    {
        if (DataManager.ins.gameSave.isNoAds)
        { //Nếu mua RemoveAds rồi thì ẩn Banner
            if (isShowingBanner) MaxManager.Ins.HideBanner();
            isShowingBanner = false;
            Debug.LogWarning("Ẩn Banner");
        }
        else
        {//Hiển thị Banner
            if (!isShowingBanner) MaxManager.Ins.ShowBanner();
            isShowingBanner = true;
            Debug.LogWarning("Hiển thị Banner");
        }
    }
    #endregion
}
