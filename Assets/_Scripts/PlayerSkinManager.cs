﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerSkinManager : MonoBehaviour
{
    public bool isUser = false;
    public int stt;
    //public int idMaterial;
    //public Color color;


    public int idSkin_Hair;
    public int idSkin_Body;

    public GameObject[] listHair;
    public GameObject[] listBody;

    public TextMeshPro txt_STT_Back;
    public TextMeshPro txt_STT_Font;

    public void Awake()
    {
        if (isUser && DataManager.ins != null)
        {//Nếu là User
            idSkin_Hair = DataManager.ins.gameSave.idSkin_Hair;
            idSkin_Body = DataManager.ins.gameSave.idSkin_Body;
            stt = DataManager.ins.gameSave.user_number;
            
        }
        else
        {//Nếu là AI
            idSkin_Hair = Random.Range(0, listHair.Length);
            idSkin_Body = Random.Range(0, listBody.Length);

            //Random ra 1 con số thứ tự
            stt = Random.Range(2, 1000);
            //Nếu số thứ tự trùng Player thì sẽ là số 1
            if (DataManager.ins != null && stt == DataManager.ins.gameSave.user_number) stt = 1;
        }

        //Viết thêm số 0 vào trước stt cho đủ 3 ký tự
        if (stt < 10){
            txt_STT_Font.text = txt_STT_Back.text = "00" + stt;
        }else if (stt < 100){
            txt_STT_Font.text = txt_STT_Back.text = "0" + stt;
        }else{
            txt_STT_Font.text = txt_STT_Back.text = "" + stt;
        }
        //Ghép skin vào
        ReloadSkin();
    }

    public void ReloadSkin()
    {
        //Reload Skin Đầu Tóc
        for (int i = 0; i < listHair.Length; i++) {
            listHair[i].SetActive(i == idSkin_Hair);
        }
        //Reload Skin Body
        for (int i = 0; i < listBody.Length; i++)
        {
            listBody[i].SetActive(i == idSkin_Body);
        }
    }

    public void WearSkin_Force(int idHair = -1, int idBody = -1){//Trong Shop sẽ cho mặc thử 1 bộ skin
        //Reload Skin Đầu Tóc
        if (idHair >=0) {
            for (int i = 0; i < listHair.Length; i++) {
            listHair[i].SetActive(i == idHair);
            }
        }
        //Reload Skin Body
        if (idBody >= 0){
            for (int i = 0; i < listBody.Length; i++){
                listBody[i].SetActive(i == idBody);
            }
        }
    }
}
