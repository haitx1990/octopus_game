﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RedLightGreenLight : MonoBehaviour {
    [HideInInspector] public int zMin;
    [HideInInspector] public int zMax;

    public int idPlayerKilling = 0;
    public float timeDelayKill = 0;
    public int winner;//Số người chơi đang chơi
    public List<int> listPlayer_WillBeKill;//List những Player chuyển động và sẽ bị giết lần lượt

    public Doll doll;
    public MyPlayer myPlayer;
    public List<PlayerBase> listAllPlayer;

    public GameObject obj_EffectRedLight;
    public GameObject obj_EffectDie;

    private void Awake() {
        zMin = -100;
        zMax = 100;
        SceneManager.ins.form_GamePlay.alivePlayer = DataManager.ins != null ? DataManager.ins.gameSave.remainingPlayer : Constant.totalPlayer-1;
        winner = 0;
        for(int i = 0; i < listAllPlayer.Count; i++) {
            //Chỉ set up số AIPlayer còn sống từ minigame trc
            if(i != 0 && i < SceneManager.ins.form_GamePlay.alivePlayer) {
                listAllPlayer[i].idChar = i;
                listAllPlayer[i].ani = Ani_State.Idle_InGame;
                listAllPlayer[i].transform.localPosition += Vector3.forward * Random.Range(0, 4f);
                listAllPlayer[i].AIPlayer.timeIdle_AI = Random.Range(0f, 0.5f);
            } else if(i != 0) {//Ẩn 1 lượng AIPlayer theo số còn sống từ minigame trc
                listAllPlayer[i].isDie = true;
                listAllPlayer[i].gameObject.SetActive(false);
            }
            //Tạo sắn 1 list các Pose sẽ random ra
            listAllPlayer[i].list_IDPose = new int[Constant.maxPoseIdle];
            listAllPlayer[i].list_IDPose = Enumerable.Range(1, listAllPlayer[i].list_IDPose.Length).ToArray();
            listAllPlayer[i].list_IDPose.Shuffle<int>();
            listAllPlayer[i].idPose = Random.Range(0, listAllPlayer[i].list_IDPose.Length);
        }
        SceneManager.ins.BlockInput(0.5f);
        listPlayer_WillBeKill = new List<int>();
    }

    public void StartGame()
    {
        SceneManager.ins.form_GamePlay.redLightGreenLight.myPlayer.isCanControl = true;
        SceneManager.ins.form_GamePlay.isStartGame = true;
        SceneManager.ins.form_GamePlay.isEndGame = false;
        SceneManager.ins.form_GamePlay.timeCountdown = Constant.timeCountdown;
        doll.StartGame();
        StartCoroutine(Countdown());
    }

    private IEnumerator Countdown() {
        if(SceneManager.ins.form_GamePlay.isStartGame && !SceneManager.ins.form_GamePlay.isEndGame) {
            if(SceneManager.ins.form_GamePlay.timeCountdown % 60 < 10) {
                SceneManager.ins.form_GamePlay.txt_Time.text = "0" + (SceneManager.ins.form_GamePlay.timeCountdown / 60) + ":0" + (SceneManager.ins.form_GamePlay.timeCountdown % 60);
            } else {
                SceneManager.ins.form_GamePlay.txt_Time.text = "0" + (SceneManager.ins.form_GamePlay.timeCountdown / 60) + ":" + (SceneManager.ins.form_GamePlay.timeCountdown % 60);
            }
            if(SceneManager.ins.form_GamePlay.timeCountdown == 10) {
                SceneManager.ins.form_GamePlay.txt_Time.color = Color.red;
                SceneManager.ins.form_GamePlay.soundLast10Serconds.PlaySound(); 
            }
            yield return new WaitForSeconds(1f);
            SceneManager.ins.form_GamePlay.timeCountdown--;
            if(SceneManager.ins.form_GamePlay.timeCountdown < 0) {
                SceneManager.ins.form_GamePlay.isEndGame = true;
                //Giết lần lượt tất cả các Player chưa về đích
                StartCoroutine(KillAllPlayer_TimeOut());
            } else {
                StartCoroutine(Countdown());
            }
               
        }
    }

    private IEnumerator KillAllPlayer_TimeOut() {
        for(int i = SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer.Count-1; i >= 0 ; i--) {
            //Chỉ check những Player chưa chết
            if (!SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isDie && SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].ani_Apply != Ani_State.Dead) {
                //Giết tất cả các Player chưa về đích
                if (SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].transform.localPosition.z < SceneManager.ins.form_GamePlay.redLightGreenLight.zMax && !SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isWin){
                    if (i == 0 && GameManager.ins.isUndying){//Ko giết MyPlayer khi cheat bất tử
                    }else{
                        SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isDie = true;
                        yield return new WaitForSeconds(0.1f);
                    }
                }
                else
                {
                    //Nếu chưa bị giết mà kịp chạy qua vạch đích thì vẫn tính win
                    SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isWin = true;
                }
            }
        }
    }

    void FixedUpdate() {
        if(SceneManager.ins.async != null) return;
        if(!SceneManager.ins.form_GamePlay.isStartGame) {//Nếu chưa bắt đầu chơi
            //listAllPlayer[i].AIPlayer.ani = Ani_State.Idle;
        } else if(SceneManager.ins.form_GamePlay.isStartGame && !SceneManager.ins.form_GamePlay.isEndGame) {//Nếu đang chơi
            //Đếm lại số ng còn sống và số ng chiến thắng
            SceneManager.ins.form_GamePlay.alivePlayer = 0;
            winner = 0;
            for(int i = 0; i < SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer.Count; i++)
                //Đếm lại số ng còn sống
                if(!SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isDie) {
                    SceneManager.ins.form_GamePlay.alivePlayer++;
                    //Đếm số ng chiến thắng
                    if(SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isWin) winner++;
                }
            //Nếu ko còn Player nào đang chơi -> Endgame
            if(SceneManager.ins.form_GamePlay.alivePlayer - winner <= 0) {
                //Nếu Player đang chết và thời gian vẫn còn -> Game còn có thể tiếp tục
                if (SceneManager.ins.form_GamePlay.redLightGreenLight.myPlayer.isDie && SceneManager.ins.form_GamePlay.timeCountdown >= 1)
                {

                } else {
                    SceneManager.ins.form_GamePlay.isEndGame = true;
                    if (!GameManager.ins.isUndying){//Ko giết MyPlayer khi cheat bất tử
                        SceneManager.ins.form_GamePlay.redLightGreenLight.myPlayer.isDie = true;
                    }
                    return;
                }
            }

            //Nếu trong thời gian check -> ai đang di chuyển thì chết
            if(SceneManager.ins.form_GamePlay.redLightGreenLight.doll.isCheckAndKill && SceneManager.ins.form_GamePlay.isStartGame && !SceneManager.ins.form_GamePlay.isEndGame) {
                for(int i = 0; i < SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer.Count; i++) {
                    if(SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].ani_Apply == Ani_State.Run
                        && !SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isDie
                        && SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].transform.localPosition.z <= SceneManager.ins.form_GamePlay.redLightGreenLight.zMax
                        && SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].transform.localPosition.z >= SceneManager.ins.form_GamePlay.redLightGreenLight.zMin)
                        if (i == 0 && GameManager.ins.isUndying){//Ko giết MyPlayer khi cheat bất tử
                        }else {
                            //Thêm tất cả các Player bị giết vào list và giết dần
                            if(!listPlayer_WillBeKill.Contains(i)) listPlayer_WillBeKill.Add(i);
                            //StartCoroutine(KillPlayer(SceneManager.ins.form_GamePlay.levelController.listAllPlayer[i]));
                            /*Timer.Schedule(this, Random.Range(1,SceneManager.ins.form_GamePlay.doll.timeTurnOn), () => {
                                SceneManager.ins.form_GamePlay.levelController.listAllPlayer[i].isDie = true;
                            });*/
                        }
                }
                //Giết 1 Player trong list, nếu List còn Player và đang ko giết Player nào
                if(listPlayer_WillBeKill.Count > 0 && timeDelayKill <= 0) {
                    timeDelayKill = 0.18f;
                    idPlayerKilling = Random.Range(0, listPlayer_WillBeKill.Count);
                    SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[listPlayer_WillBeKill[idPlayerKilling]].isDie = true;
                    listPlayer_WillBeKill.RemoveAt(idPlayerKilling);
                } else {//Nếu đang delay để giết Player tiếp theo
                    timeDelayKill -= Time.fixedDeltaTime;
                }
            }

            //Điều khiển hoạt động của các AI (listAllPlayer[0] chính là MyPlayer)
            for(int i = 1; i < listAllPlayer.Count; i++) {
                if(listAllPlayer[i].AIPlayer == null || listAllPlayer[i].AIPlayer.ani_Apply == Ani_State.Dead)
                    continue;
                //Đềm thời gian chạy và thời gian đứng yên của AIPlayer
                listAllPlayer[i].AIPlayer.timeRun_AI -= Time.fixedDeltaTime;
                listAllPlayer[i].AIPlayer.timeIdle_AI -= Time.fixedDeltaTime;

                //Nếu AI đã chết
                if(listAllPlayer[i].AIPlayer.isDie) {
                    listAllPlayer[i].AIPlayer.Dead();
                    continue;
                } else {//Nếu AI chưa chết
                    if(listAllPlayer[i].AIPlayer.isWin) {//Ani chiến thắng
                        listAllPlayer[i].AIPlayer.ani = Ani_State.Win;
                        continue;
                    }

                    //Thời gian Búp bê úp mặt-> AIPlayer sẽ chạy về phía trc
                    if(SceneManager.ins.form_GamePlay.redLightGreenLight.doll.state == Doll_State.TurnOff) {
                        if(listAllPlayer[i].AIPlayer.timeIdle_AI > 0) {
                            //listAllPlayer[i].AIPlayer.ani = Ani_State.Idle_InGame;
                            listAllPlayer[i].AIPlayer.ani = Ani_State.Pose_InGame;
                            listAllPlayer[i].AIPlayer.timeMove = 0;
                            continue;
                        } else if (listAllPlayer[i].AIPlayer.timeRun_AI > 0) {
                            listAllPlayer[i].AIPlayer.Move();
                            listAllPlayer[i].AIPlayer.ani = Ani_State.Run;
                            continue;
                        }
                        else
                        {
                            //listAllPlayer[i].AIPlayer.ani = Ani_State.Idle_InGame;
                            listAllPlayer[i].AIPlayer.ani = Ani_State.Pose_InGame;
                            listAllPlayer[i].AIPlayer.timeMove = 0;
                            continue;
                        }
                    } else {//Thời gian Búp bê quay mặt ra -> AIPlayer sẽ đứng yên
                        if(listAllPlayer[i].AIPlayer.timeRun_AI > 0) {
                            listAllPlayer[i].AIPlayer.Move();
                            listAllPlayer[i].AIPlayer.ani = Ani_State.Run;
                            continue;
                        } else if(listAllPlayer[i].AIPlayer.timeIdle_AI > 0) {
                            //listAllPlayer[i].AIPlayer.ani = Ani_State.Idle_InGame;
                            listAllPlayer[i].AIPlayer.ani = Ani_State.Pose_InGame;
                            listAllPlayer[i].AIPlayer.timeMove = 0;
                            continue;
                        }
                        else
                        {
                            listAllPlayer[i].AIPlayer.Move();
                            listAllPlayer[i].AIPlayer.ani = Ani_State.Run;
                            continue;
                        }
                    }
                }

            }
        } else {//Nếu đã hết giờ hoặc các Player đã chơi xong hết
            for (int i = 1; i < listAllPlayer.Count; i++) {
                if(listAllPlayer[i].isDie && listAllPlayer[i].ani_Apply != Ani_State.Dead) {
                    listAllPlayer[i].isWin = false;
                    listAllPlayer[i].ani = Ani_State.Dead;
                    listAllPlayer[i].Dead();
                } else if(listAllPlayer[i].isWin && listAllPlayer[i].ani_Apply != Ani_State.Win) {
                    listAllPlayer[i].ani = Ani_State.Win;
                }
            }
        }
       
    }

    private int randomTimeRun_AI = 0;
    private int randomTimeIdle_AI = 0;
    public void SetTimeRun_AIPlayers() {
        for(int i = 1; i < listAllPlayer.Count; i++) {
            if(listAllPlayer[i].AIPlayer == null && listAllPlayer[i].AIPlayer.ani_Apply == Ani_State.Dead) continue;
            //Thời gian chạy sẽ từ 80% -> 100% của thời gian Búp bê úp mặt (Để AI bị bắn chết 1 lượng vừa phải)
            randomTimeRun_AI = Random.Range(-10, 2);
            if (randomTimeRun_AI >= 0) randomTimeRun_AI += 12;//Cho AIPlayer chạy thêm 1 đoạn để User nhìn thấy rõ
            listAllPlayer[i].AIPlayer.timeRun_AI = SceneManager.ins.form_GamePlay.redLightGreenLight.doll.timeTurnOff + (0.01f * randomTimeRun_AI) * SceneManager.ins.form_GamePlay.redLightGreenLight.doll.timeTurnOff + 0.2f;
        }
    }

    public void SetTimeIdle_AIPlayers() {
        for(int i = 1; i < listAllPlayer.Count; i++) {
            if(listAllPlayer[i].AIPlayer == null && listAllPlayer[i].AIPlayer.ani_Apply == Ani_State.Dead) continue;
            //Thời gian chạy sẽ từ 101% -> 135% của thời gian Búp bê quay mặt ra (Để AI delay 1 chút rồi mới chạy cho nó thật)
            randomTimeIdle_AI = Random.Range(1, 36);
            listAllPlayer[i].AIPlayer.timeIdle_AI = SceneManager.ins.form_GamePlay.redLightGreenLight.doll.timeTurnOn + (0.01f * randomTimeIdle_AI) * SceneManager.ins.form_GamePlay.redLightGreenLight.doll.timeTurnOn + 0.2f;
        }
    }
}
