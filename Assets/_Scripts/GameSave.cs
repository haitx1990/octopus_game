﻿
[System.Serializable]
public class GameSave
{
    //-------- State --------
    public bool isNew = true;
    public int volumeMusic = 100;
    public int volumeSound = 100;
    public bool isVibrate = true;
    public bool isNoAds = false;
    public bool isTutCompleted = true;
    public int starRate = -1;

    //-------- Value --------
    public int gold = 0;
    public int piggyBank = 0;//Số tiền trong con Heo
    public int piggyBank_Case = 0;//Số tiền sẽ rơi vào con Heo khi ra Home
    public int level = 0;//Level hiện tại
    public int levelMax = 0;//Level cuối cùng đã đến
    public bool isMan = true;
    public int idSkin_Hair = 0;//Skin tóc
    public int idSkin_Body = 0;//Skin body
    public int user_number = 100;
    public int remainingPlayer = 100;//Số người chơi còn lại hiện tại
    public int winMiniGameCount = 0;//Số lần chiến thắng 1 minigame
    public int victoryAllCount = 0;//Số lần chiến thắng tất cả các game


    //-------- Firebase --------
    public string timeInstall;//Thời điểm cài game
    public string timeLastOpen;//Thời điểm cuối cùng mở game
    public int daysPlayed = 0;//Số ngày đã User có mở game lên
    public int sessionCount = 0;//Tống số session
    public int playTime = 0;//Tổng số lần nhấn play game
    public int playTime_Session = 0;//Số lần nhấn play game trong 1 session
    public int dieCount_levelCur = 0;//Số lần chết tại level hiện tại
    public int failCount_levelCur = 0;//Số lần thua tại level hiện tại

    //-------- Other --------
    public Skin_Save[] listSkins_Hair;
    public Skin_Save[] listSkins_Body;

    public GameSave()
    {
        isNew = true;
        volumeMusic = 50;
        volumeSound = 50;
        isVibrate = true;
        isNoAds = false;
        isTutCompleted = true;
        starRate = -1;

        gold = 0;
        piggyBank = 0;
        piggyBank_Case = 0;
        level = 0;
        levelMax = 0;
        isMan = true;
        idSkin_Hair = 0;
        idSkin_Body = 0;
        user_number = 100;
        remainingPlayer = Constant.totalPlayer;
        winMiniGameCount = 0;
        victoryAllCount = 0;

        //Time
        timeInstall = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        timeLastOpen = System.DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        daysPlayed = 0;
        sessionCount = 0;
        playTime = 0;
        playTime_Session = 0;
        dieCount_levelCur = 0;
        failCount_levelCur = 0;
    }
}

[System.Serializable]
public class Skin_Save
{
    public int id;
    public string keyID;//ID để sau này đổi thứ tự vũ khí thì vẫn lấy đc data cũ

    //Những data sẽ thay đổi trong game
    public bool isUnlock;
    public int amountVideAds;//Số VideoAds đã xem để unlock

    public Skin_Save(int id_Skin, string key_Skin)
    {
        id = id_Skin;
        isUnlock = false;
        amountVideAds = 0;
        keyID = key_Skin;
        if (id == 0)
        {
            isUnlock = true;
        }
    }
}
