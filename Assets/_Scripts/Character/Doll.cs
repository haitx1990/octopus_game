﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Doll : MonoBehaviour
{
    public DOTweenAnimation twn_Head;
    public GameObject obj_EffectEye;
    public GameObject obj_ImgRedLight;
    public GameObject obj_ImgGreenLight;

    public SoundObject audioSing;
    public SoundObject audioTurnAround;
    public Doll_State state;
    public bool isCheckAndKill = false;

    public float timeTurnOff = 4f;
    public float timeTurnOn = 4;


    public int turn = 0;
    public bool isFirstTime = true;

    public void StartGame() {
        StartCoroutine(RotateHead());
    }

    private IEnumerator RotateHead() {
        //Búp bê chỉ hoạt động trong thời gian chơi Đèn Xanh Đèn Đỏ
        if(SceneManager.ins.form_GamePlay.isStartGame && !SceneManager.ins.form_GamePlay.isEndGame) {
            //Búp bê up mặt lại
            state = Doll_State.TurnOff;
            if (isFirstTime) {
                isFirstTime = false;
                timeTurnOff = 4f;
            } else {
                //timeTurnOff = Random.Range(2.3f, 4f);
                timeTurnOff = Mathf.Clamp( timeTurnOff - 0.2f + 0.005f*DataManager.ins.gameSave.failCount_levelCur, 2.3f,4f);
            }
            SceneManager.ins.form_GamePlay.redLightGreenLight.SetTimeRun_AIPlayers();
            SceneManager.ins.form_GamePlay.redLightGreenLight.obj_EffectRedLight.SetActive(false);
            obj_ImgRedLight.SetActive(false);
            obj_ImgGreenLight.SetActive(true);
            obj_EffectEye.SetActive(false);
            twn_Head.DOPlayBackwards();
            audioTurnAround.PlaySound();
            yield return new WaitForSeconds(0.2f);

            //Úp mặt trong X giây và  phát ra tiếng hát
            if (audioSing != null)
            {
                audioSing.PlaySound(0, 4.55f / (timeTurnOff + 0.2f));
            }
            yield return new WaitForSeconds(timeTurnOff);

            //Búp bê đang quay mặt ra
            state = Doll_State.TurnOn;
            timeTurnOn = Random.Range(3.75f, 4.75f);
            SceneManager.ins.form_GamePlay.redLightGreenLight.SetTimeIdle_AIPlayers();
            twn_Head.DOPlayForward();
            audioTurnAround.PlaySound();
            SceneManager.ins.form_GamePlay.redLightGreenLight.timeDelayKill = 0.4f;
            isCheckAndKill = true;
            yield return new WaitForSeconds(0.2f);


            //Quay mặt xong và tìm kiếm mục tiêu và bắn chết lần lượt
            audioSing.PlaySound(1);
            obj_ImgRedLight.SetActive(true);
            obj_ImgGreenLight.SetActive(false);
            obj_EffectEye.SetActive(true);
            SceneManager.ins.form_GamePlay.redLightGreenLight.obj_EffectRedLight.SetActive(true);
            yield return new WaitForSeconds(timeTurnOn);

            isCheckAndKill = false;
            StartCoroutine(RotateHead());//Tiếp tục đếm thời gian Block Input
            turn++;
        }
    }


}
