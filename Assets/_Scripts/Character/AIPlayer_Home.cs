﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class AIPlayer_Home : MonoBehaviour {
    [Header("--------------- Data của Char ---------------")]
    public int idChar;
    public int idPose;
    public int idMaterial;
    public Color color;
    //public string namePlayer;

    [Header("--------------- Trạng thái của Char ---------------")]
    public Ani_State ani;
    public Ani_State ani_Apply;

    [Header("--------------- Tham chiếu ---------------")]
    public Animator animator;
    public PlayerSkinManager playerSkinManager;

    public  void Awake() {
        idChar = 1;
        //Load lại Animation
        ReloadAnimation();
    }

    public void Start() {
        
    }
    public void ReloadAnimation() {
        if(ani_Apply != ani) {
            ani_Apply = ani;
            animator.SetInteger(Ani_State.Pose_InHome.ToString(), idPose);
        }
    }
}
