﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MyPlayer : PlayerBase {
    [Header("--------------- MyPlayer ---------------")]
    public bool isCanControl = false;//Có thể điều khiển nhân vật di chuyển
    public bool isRebornByAds = false;//Đã reborn bằng Ads chưa?


    private Vector3 moveDirection = Vector3.zero;
    private float localEulerAnglesY = 0;
    public float timeMove = 0;//Để tính gia tốc
    //public CharacterController controller;
    //public Vector3 targetPosition;
   
    public override void Awake() {
        base.Awake();
        isCanControl = false;
    }

    public override void Start() {
        base.Start();
        idChar = 0;
        //idMaterial = DataManager.ins.gameSave.idMaterial;
        //color = GameManager.ins.listColors[idMaterial];
        //targetPosition = transform.position;
        timeMove = 0;
    }

    public void FixedUpdate() {
        //Chỉ trong Minigame thì mới chạy FixUpdate
        if (SceneManager.ins.formCurrent.idForm != FormUI.RedLightGreenLight) return;
        if(ani_Apply == Ani_State.Dead && !isRebornByAds) return;
        if (ani_Apply == Ani_State.Win) return;
        if (!isDie && ani != Ani_State.Dead && ani_Apply != Ani_State.Dead && ani != Ani_State.Win && transform.localPosition.z > SceneManager.ins.form_GamePlay.redLightGreenLight.zMax)
        {
            isWin = true;
            isCanControl = false;
            ani = Ani_State.Win;
            //Check MyPlayer top mấy
            SceneManager.ins.form_GamePlay.top = 1;
            for(int i = 1; i < SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer.Count; i++) {
                //Đếm các AIPlayer còn sống và đã Win
                if(SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isWin && !SceneManager.ins.form_GamePlay.redLightGreenLight.listAllPlayer[i].isDie) {
                    SceneManager.ins.form_GamePlay.top++;
                }
            }
            //Nếu đã chơi đủ số ngày rồi thì mới show WinAll
            if(DataManager.ins.gameSave.level >= GameManager.ins.listMiniGame.Length-1) {
                SceneManager.ins.ShowPopup_WinAll();
            } else{//Nếu ko thì chỉ show WinMinigame
                SceneManager.ins.ShowPopup_EndGame(true);
            }
        }
        else
        {
            //Nếu nhân vật còn sống
            if (!isDie)
            {
                //Tap vào màn hình -> Nhân sẽ vật di chuyển
                if (Input.GetMouseButton(0) && isCanControl)
                {
                    ani = Ani_State.Run;
                   
                    //Tính độ lớn và hướng của vector di chuyển
                    timeMove += Time.fixedDeltaTime;
                    moveDirection = new Vector3(0.0f, 0.0f, 1);
                    moveDirection = tran_Rotate.TransformDirection(moveDirection);//Di chuyển theo hướng thắng mặt nhân vật
                    rigid.velocity = moveDirection * Time.fixedDeltaTime * (160 + Mathf.Min(timeMove * 360, 360));


                    /*Di chuyen thang deu
                    moveDirection = moveDirection * 9f;
                    targetPosition += moveDirection * Time.fixedDeltaTime;
                    transform.position = Vector3.Lerp(transform.position, targetPosition, 0.1f);*/
                    //Quay mặt theo hướng controll đang điều khiển
                    if (TouchRotateSingle.eulerRotation != Vector3.zero) {
                        tran_Rotate.localRotation = Quaternion.LookRotation(TouchRotateSingle.eulerRotation);
                        //Ko cho MyPlayer quay đầu lại
                        //Debug.LogError(tran_Rotate.localEulerAngles.y);
                        localEulerAnglesY =  tran_Rotate.localEulerAngles.y;
                        if (localEulerAnglesY >=0  && localEulerAnglesY < 90){
                            localEulerAnglesY = Mathf.Clamp(localEulerAnglesY,0 , 80);
                        } else if(localEulerAnglesY >= 90 && localEulerAnglesY < 180) {
                            localEulerAnglesY = 180 - Mathf.Clamp(localEulerAnglesY, 100, 180);
                        }  else if (localEulerAnglesY >= 180 && localEulerAnglesY < 270) {
                            localEulerAnglesY = 540 - Mathf.Clamp(localEulerAnglesY, 180, 260);
                        } else{
                            localEulerAnglesY = Mathf.Clamp(localEulerAnglesY, 280, 360);
                        }
                        tran_Rotate.localEulerAngles = new Vector3(tran_Rotate.localEulerAngles.x , localEulerAnglesY, tran_Rotate.localEulerAngles.z);
                    }
                }
                else
                {//Nếu ko tap vào màn hình -> Nhân vật sẽ đứng yên
                    timeMove = 0;
                    if(!SceneManager.ins.form_GamePlay.isStartGame) {
                        ani = Ani_State.Idle_InGame;
                    }else {
                        //ani = Ani_State.Idle_InGame;
                        ani = Ani_State.Pose_InGame;
                    }
                }
            }
            else
            {//Nếu nhân vật đã chết
                isCanControl = false;
                //Lần đầu sẽ thực hiện Ani Dead 
                if (ani != Ani_State.Dead)
                {
                    Dead();
                }
            }
        }


        //Load lại Animation
        ReloadAnimation();
    }

    public void ReloadAnimation() {
        if(ani_Apply != ani) {
            if(ani == Ani_State.Run) {
                audio_Run.Enable();
            } else if(ani_Apply == Ani_State.Run) {
                Timer.Schedule(this, 0.5f, () => { audio_Run.Disable(); });
            }
            if(ani_Apply == Ani_State.Idle_InGame && ani == Ani_State.Pose_InGame) ani = Ani_State.Idle_InGame;
            ani_Apply = ani;
            animator.SetBool(Ani_State.Idle.ToString(), ani_Apply == Ani_State.Idle);
            animator.SetBool(Ani_State.Run.ToString(), ani_Apply == Ani_State.Run);
            animator.SetBool(Ani_State.Dead.ToString(), ani_Apply == Ani_State.Dead);
            animator.SetBool(Ani_State.Win.ToString(), ani_Apply == Ani_State.Win);
            animator.SetBool(Ani_State.Dance.ToString(), ani_Apply == Ani_State.Dance);
            animator.SetBool(Ani_State.Attack.ToString(), ani_Apply == Ani_State.Attack);
            animator.SetBool(Ani_State.Ulti.ToString(), ani_Apply == Ani_State.Ulti);
            animator.SetBool(Ani_State.Walk_Up.ToString(), ani_Apply == Ani_State.Walk_Up);
            animator.SetBool(Ani_State.Idle_InGame.ToString(), ani_Apply == Ani_State.Idle_InGame);
            if(ani_Apply == Ani_State.Pose_InGame) {
                animator.SetInteger(Ani_State.Pose_InGame.ToString(), list_IDPose[idPose]);
                idPose++;
                if (idPose >= list_IDPose.Length) idPose = 0;
                //animator.SetInteger(Ani_State.Pose_InGame.ToString(), SceneManager.ins.form_GamePlay.redLightGreenLight.doll.turn % Constant.maxPoseIdle + 1);
            } else {
                animator.SetInteger(Ani_State.Pose_InGame.ToString(), 0);
            }
        }
    }

    //Hồi sinh 1 Player
    public void Reborn() {
        isDie = false;
        isCanControl = true;
        isRebornByAds = true;
        effectBlood.SetActive(false);
        rigid.isKinematic = false;
        //tran_Rotate.localPosition = Vector3.zero;
        Timer.Schedule(this, 0.02f, () => { tran_Rotate.localPosition = Vector3.zero; });
        //dieCollider.gameObject.SetActive(false);
        myCollider.enabled = true;
        //dieCollider.transform.localPosition = Vector3.zero;
        //dieCollider.transform.localScale = Vector3.zero;
        m_Ragdoll.blendToMecanim();
        //ChangeMaterial(false);
    }

    public override void Dead() {
        ani = Ani_State.Dead;
        isWin = false;
        audio_Run.Disable();
        audio_Die.PlaySound();
        Dead_Ragdoll();
        effectBlood.SetActive(true);
        rigid.isKinematic = true;
        //dieCollider.gameObject.SetActive(true);
        myCollider.enabled = false;
        SceneManager.ins.form_GamePlay.redLightGreenLight.obj_EffectDie.SetActive(true);
        DataManager.ins.gameSave.dieCount_levelCur++;
        //ChangeMaterial(true);//Tối màu nhân vật đi
        //audio_Die.clip = SceneManager.ins.list_audio_Die[Random.Range(0, SceneManager.ins.list_audio_Die.Length)];
        //audio_Die.Play();
        //Rung điện thoại khi Player Chết
        if(DataManager.ins.gameSave.isVibrate)
            Vibration.Vibrate(GameManager.ins.isIOS ? 15 : 50);
        Timer.Schedule(this, 2, () => {
            //Nếu hết thời gian thì bật PopupEndgame
            if(SceneManager.ins.form_GamePlay.timeCountdown <= 0 || isRebornByAds) {
                SceneManager.ins.ShowPopup_EndGame(false);
            } else {//Nếu chưa hết thời gian thì cho Revive
                SceneManager.ins.ShowPopup_Revive();
            }
        });
    }
}
