﻿using MLSpace;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBase : MonoBehaviour {
    [Header("--------------- Data của Char ---------------")]
    public int idChar;
    public int idMaterial;
    public Color color;
    public int top;

    [Header("--------------- Trạng thái của Char ---------------")]
    public int idPose = 0;
    public int[] list_IDPose;
    public bool isDie;
    public bool isWin = false;//User đã chiến thắng chưa?
    public Ani_State ani;
    public Ani_State ani_Apply;

    [Header("--------------- Tham chiếu ---------------")]
    public Animator animator;
    public Transform tran_Rotate;
    public Rigidbody rigid;
    public CapsuleCollider myCollider;
    //public SkinnedMeshRenderer mesh3D;
    public GameObject effectBlood;
    public SoundObject audio_Die;
    public SoundObject audio_Run;
    public Collider dieCollider;
    public RagdollManagerGen m_Ragdoll;
    public BodyColliderScript bodyColliderScript_Head;

    public MyPlayer myPlayer;
    public AIPlayer AIPlayer;

    public virtual void Awake() {
    }

    public virtual void Start() {
        //// example of disabling character capsule
        //// at entering ragdoll
        //// and enabling it after ragdoll ends
        //CapsuleCollider capsule = GetComponent<CapsuleCollider>();
        //Rigidbody rigidbody = GetComponent<Rigidbody>();
            m_Ragdoll.OnHit = () =>
            {
                //capsule.enabled = false;
                //rigidbody.isKinematic = true;
                animator.applyRootMotion = false;
            };
            m_Ragdoll.LastEvent = () =>
            {
                //capsule.enabled = true;
                //rigidbody.isKinematic = false;
                animator.applyRootMotion = true;
            };

        /*
            m_Ragdoll.ragdollEventTime = 3.0f;
            m_Ragdoll.OnTimeEnd = () =>
            {
                //m_Ragdoll.blendToMecanim();
            };*/
    }

    public void Dead_Ragdoll()
    {
            int[] parts = new int[] { bodyColliderScript_Head.index };
        m_Ragdoll.startRagdoll(parts, Vector3.back * 5);
    }

    public virtual void Dead() { 
    }
}
