﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Army : MonoBehaviour
{
    public Animator animator;
    public Ani_State ani;
    public Ani_State ani_Apply;

    public void OnEnable()
    {
        if (ani_Apply != ani)
        {
            ani_Apply = ani;
            animator.SetBool(Ani_State.Idle_Army.ToString(), ani_Apply == Ani_State.Idle_Army);
            animator.SetBool(Ani_State.Attack_Army_Pistol.ToString(), ani_Apply == Ani_State.Attack_Army_Pistol);
            animator.SetBool(Ani_State.Attack_Army_AK.ToString(), ani_Apply == Ani_State.Attack_Army_AK);
        }
    }
}
