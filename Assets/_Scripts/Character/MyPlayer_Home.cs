﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class MyPlayer_Home : MonoBehaviour {
    [Header("--------------- Data của Char ---------------")]
    public int idChar;
    public int idMaterial;
    public Color color;
    //public string namePlayer;

    [Header("--------------- Trạng thái của Char ---------------")]
    public int idPose = 0;
    public int[] list_IDPose;
    public Ani_State ani;
    public Ani_State ani_Apply;

    [Header("--------------- Tham chiếu ---------------")]
    public Animator animator;
    public PlayerSkinManager playerSkinManager;

    public  void Awake() {
    }

    public void Start() {
        idChar = 0;
        //idMaterial = DataManager.ins.gameSave.idMaterial;
        //color = GameManager.ins.listColors[idMaterial];
        //targetPosition = transform.position;
        //Load lại Animation
        ReloadAnimation();
    }
    public void ReloadAnimation() {
        if(ani_Apply != ani) {
            if(ani_Apply == Ani_State.Idle_InGame && ani == Ani_State.Pose_InGame) ani = Ani_State.Idle_InGame;
            ani_Apply = ani;
            animator.SetBool(Ani_State.Idle.ToString(), ani_Apply == Ani_State.Idle);
            animator.SetBool(Ani_State.Run.ToString(), ani_Apply == Ani_State.Run);
            animator.SetBool(Ani_State.Dead.ToString(), ani_Apply == Ani_State.Dead);
            animator.SetBool(Ani_State.Win.ToString(), ani_Apply == Ani_State.Win);
            animator.SetBool(Ani_State.Dance.ToString(), ani_Apply == Ani_State.Dance);
            animator.SetBool(Ani_State.Attack.ToString(), ani_Apply == Ani_State.Attack);
            animator.SetBool(Ani_State.Ulti.ToString(), ani_Apply == Ani_State.Ulti);
            animator.SetBool(Ani_State.Walk_Up.ToString(), ani_Apply == Ani_State.Walk_Up);
            animator.SetBool(Ani_State.Idle_InGame.ToString(), ani_Apply == Ani_State.Idle_InGame);
            if(ani_Apply == Ani_State.Pose_InGame) {
                /*animator.SetInteger(Ani_State.Pose_InGame.ToString(), list_IDPose[idPose]);
                idPose++;
                if (idPose >= list_IDPose.Length) idPose = 0;*/
                animator.SetInteger(Ani_State.Pose_InGame.ToString(), SceneManager.ins.form_GamePlay.redLightGreenLight.doll.turn % Constant.maxPoseIdle + 1);
            } else {
                animator.SetInteger(Ani_State.Pose_InGame.ToString(), 0);
            }
        }
    }
}
