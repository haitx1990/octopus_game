﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;


public class ChangeScene : Editor {

    [MenuItem("Open Scene/Form_Loading #1")]
    public static void OpenLoading()
    {
        OpenScene("Form_Loading");
    }

    [MenuItem("Open Scene/Form_Home #2")]
    public static void OpenHome()
    {
        OpenScene("Form_Home");
    }

    [MenuItem("Open Scene/RedLightGreenLight #3")]
    public static void OpenRedLightGreenLight()
    {
        OpenScene("RedLightGreenLight");
    }

    private static void OpenScene (string sceneName) {
		if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo ()) {
			EditorSceneManager.OpenScene ("Assets/_Scenes/" + sceneName + ".unity");
		}
	}
}
#endif