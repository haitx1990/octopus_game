﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class camFollower : MonoBehaviour
{
    public static camFollower Instance;
    public Camera _camera;
    //public Transform target;
    //public Vector3 positionOffset;
    [Header("-------- Form Main Menu --------")]
    public float angle_1;
    public Vector3 localPosition_1;

    [Header("-------- Popup Char Skin --------")]
    public float angle_2;
    public Vector3 localPosition_2;

    public float rate21_9;//21/9 y:-3.9 z:-3.2 ->22%

    //Vector3 targetPos;
    //float distance;
    public Transform thisTrans;
    public int state;

    private void Awake()
    {
        Instance = this;
        state = 1;
    }

    public void Start()
    {
        if (thisTrans == null) thisTrans = transform;

        //Zoom out Camera nếu màn hình bị hẹp
        float rate_16_9 = (float)Screen.height / Screen.width - 16.1f / 9;
        if (rate_16_9 > 0)
        {
            _camera.transform.localPosition = _camera.transform.localPosition * (rate_16_9 *0.40f + 1); //21/9 -> rate_16_9= 0.55  -> rate_16_9 *0.35f + 1 = 1.1925
        }
        if(rate_16_9 > 0) rate21_9 = 1 + 0.22f * rate_16_9 / 0.54f;
    }
    
    void LateUpdate()
    {
        if (state == 1)
        {//FormMainMenu
            if (localPosition_1 != thisTrans.localPosition)
            {
                thisTrans.localPosition = Vector3.Lerp(thisTrans.localPosition, localPosition_1 , Time.deltaTime * 9f);
                thisTrans.eulerAngles = new Vector3(Mathf.Lerp(thisTrans.eulerAngles.x, angle_1, Time.deltaTime * 9f), thisTrans.eulerAngles.y, thisTrans.eulerAngles.z);
                if (SceneManager.ins.form_Home != null) {
                    SceneManager.ins.form_Home.txt_PiggyBank.gameObject.SetActive(true);
                    SceneManager.ins.form_Home.obj_BtnSkin.SetActive(true);
                    SceneManager.ins.form_Home.myPlayerHome.ani = Ani_State.Idle;
                    SceneManager.ins.form_Home.myPlayerHome.ReloadAnimation();
                }
            }else {
                state = 0;
            }
        }
        else if (state == 2)
        {//Char Skin
            if (localPosition_2 != thisTrans.localPosition)
            {
                thisTrans.localPosition = Vector3.Lerp(thisTrans.localPosition, localPosition_2, Time.deltaTime * 9f);
                thisTrans.eulerAngles = new Vector3(Mathf.Lerp(thisTrans.eulerAngles.x, angle_2, Time.deltaTime * 9f), thisTrans.eulerAngles.y, thisTrans.eulerAngles.z);
                if (SceneManager.ins.form_Home != null)
                {
                    SceneManager.ins.form_Home.txt_PiggyBank.gameObject.SetActive(false);
                    SceneManager.ins.form_Home.obj_BtnSkin.SetActive(false);
                    SceneManager.ins.form_Home.myPlayerHome.ani = Ani_State.Dance;
                    SceneManager.ins.form_Home.myPlayerHome.ReloadAnimation();
                }
            }
            else{
                state = 0;
            }
        } 
    }
}
