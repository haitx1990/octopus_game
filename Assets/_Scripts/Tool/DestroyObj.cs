﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObj : MonoBehaviour {
    public float lifeTime;
    public bool isDisable = false;

    private void OnEnable()
    {
        if(isDisable) {
            Timer.Schedule(this, lifeTime, () => { gameObject.SetActive(false); }) ;
        } else {
            Destroy(gameObject, lifeTime);
        }
    }
}
