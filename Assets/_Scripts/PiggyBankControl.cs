﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Money_Data
{
    public Vector3 pos;
    public Vector3 euler;
}
[System.Serializable]
public class MoneyInPiggyBank
{
    public Money_Data[] moneys;
}

public class PiggyBankControl : MonoBehaviour
{
    public static PiggyBankControl instance;

    public GameObject Money_Model;
    public Transform pos_init;

    public MoneyInPiggyBank mipbs;
    public List<GameObject> moneys;

    public Coroutine coroutine_Spawn;

    IEnumerator coroutine_freeSpawn;
    
    bool isRun = false;
    bool isPress = false;
    
    private void Awake()
    {
        instance = this;
        //load_and_init();
        coroutine_freeSpawn = I_FreeSpawnMoney(0.2f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isRun && !isPress)
        {
            //SpawnMoney(5, 0.2f);
            isRun = true;
            isPress = true;
            StartCoroutine(coroutine_freeSpawn);
        }
        if (Input.GetKeyDown(KeyCode.Space) && isRun && !isPress)
        {
            //SpawnMoney(5, 0.2f);
            isRun = false;
            isPress = true;
            StopCoroutine(coroutine_freeSpawn);
        }
        isPress = false;

        if (Input.GetKeyDown(KeyCode.S))
        {
            save();
            Debug.Log("save data");
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            load_and_init();
            Debug.Log("Load & init");
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log(PlayerPrefs.GetString("Money"));
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            DeleteData();
            Debug.Log("delete data");
        }
    }

    #region SpawnMoney
    public void SpawnMoney(float timeSpawn,float interval)//gọi hàm này để spawn money ra - timeSpawn : thời gian spam - interval : thời gian chờ giữa các lần spawn ---(5f,0.2f)
    {
        coroutine_Spawn = StartCoroutine(I_SpawnMoney(timeSpawn, interval));
    }
    IEnumerator I_SpawnMoney(float timeSpawn,float interval)
    {
        float time = timeSpawn;
        while (time > 0)
        {
            GameObject obj = Instantiate(Money_Model, pos_init.position, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)), pos_init);
            moneys.Add(obj);
            
            yield return new WaitForSeconds(interval);
            time -= interval;
        }
        save();
        yield return new WaitForSeconds(3f);
        save();
    }
    
    IEnumerator I_FreeSpawnMoney(float interval)
    {
        bool a = true;
        while (a)
        {
            GameObject obj = Instantiate(Money_Model, pos_init.position, Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)), pos_init);
            moneys.Add(obj);

            yield return new WaitForSeconds(interval);
        }
        save();
        yield return new WaitForSeconds(1.5f);
        save();
        yield return new WaitForSeconds(3f);
        save();
        yield return new WaitForSeconds(5);
        save();
    }
    #endregion

    #region save/load data piggy bank
    public void save()
    {
        int i = 0;
        Money_Data[] m = new Money_Data[moneys.Count];
        
        foreach (GameObject money in moneys)
        {
            Money_Data md = new Money_Data();
            md.pos = money.transform.position;
            md.euler = money.transform.eulerAngles;

            m[i] = md; 
            i++;
        }

        mipbs.moneys = m;
        PlayerPrefs.SetString("Money",JsonUtility.ToJson(mipbs));
    }
    public void load_and_init()
    {
        if (PlayerPrefs.GetString("Money") == "")
            return;

        mipbs = JsonUtility.FromJson<MoneyInPiggyBank>(PlayerPrefs.GetString("Money"));
        foreach (Money_Data md in mipbs.moneys)
        {
            GameObject obj = Instantiate(Money_Model, md.pos, Quaternion.Euler(md.euler), pos_init);
            moneys.Add(obj);
        }
    }
    public int Money_Count()
    {
        if (PlayerPrefs.GetString("Money") == "")
            return 0;

        mipbs = JsonUtility.FromJson<MoneyInPiggyBank>(PlayerPrefs.GetString("Money"));
        int count = mipbs.moneys.Length;

        return count;

    }
    public void DeleteData()
    {
        PlayerPrefs.SetString("Money","");
        mipbs = null;
        
        foreach (GameObject money in moneys)
        {
            Destroy(money);
        }
        moneys.Clear();
    }
    private void OnApplicationQuit()
    {
        //save();
    }
    #endregion

}
