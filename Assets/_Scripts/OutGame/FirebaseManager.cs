﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;
using System;
using System.Globalization;

public class FirebaseManager : MonoBehaviour
{
    public static FirebaseManager ins = null;
    private bool fireBaseReady = false;//Firebase đã Init thành công
    private bool configChecked = false;//Firebase đã có kết quả Init
    private bool firebaseIniting = false;//Firebase đang Init
    public bool is_remote_config_done = false;//Quá trình RemoteConfig đã xong
    public bool is_remote_config_success = false;//RemoteConfig thành công

    public bool is_result_fetch_result, is_fetch_with_firebase;

    void Awake()
    {
        if (ins != null)
        {
            Destroy(this.gameObject);
            return;
        }
        ins = this;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start() {
        StartCoroutine(Last_Start());
    }

    private IEnumerator Last_Start() {
        yield return new WaitForEndOfFrame();
        fetch((bool is_fetch_result) => {

        });
    }
    /*
    private IEnumerator Start()
    {
        firebaseIniting = true;
        CheckFireBase();
        yield return new WaitUntil(() => !firebaseIniting);
        if (fireBaseReady)
        {
            Firebase.FirebaseApp.LogLevel = Firebase.LogLevel.Debug;
        }
        else
        {
            Debug.LogError("Ko khởi tạo đc Firebase");
        }
        yield return new WaitUntil(() => configChecked);
    }*/



    private void CheckFireBase()
    {
        try
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                firebaseIniting = false;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    fireBaseReady = true;
                }
                else
                {
                    configChecked = true;
                    Debug.LogError(System.String.Format("Lỗi dependencies của Firebase: {0}", dependencyStatus));
                }
            });
        }
        catch (System.Exception ex)
        {
            configChecked = true;
            firebaseIniting = false;
            Debug.LogError("Lỗi khởi tạo Firebase:" + ex.ToString());
        }
    }

    public void OnSetUserProperty()
    {
        //Nếu là bản DevelopmentBuild hoặc UnityEditor thì ko bắn UserProperty lên
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            //retentType
            DateTime timeInstall = DateTime.ParseExact(DataManager.ins.gameSave.timeInstall, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime time = DateTime.ParseExact(DataManager.ins.gameSave.timeLastOpen, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            //Retention của user, 0 tương ứng D0, 7 tương ứng D7
            OnSetProperty("retent_type", (time - timeInstall).Days);
            //Số ngày user đã chơi, khác với Retention.Nếu user cài ở D0 và 7 ngày sau mới chơi thì retention là D7 còn days_played là 2
            OnSetProperty("days_played", DataManager.ins.gameSave.daysPlayed);
            //Số tiền user đã pay, với game casual thì chỉ tính các mốc 2, 5, 10, 20, 50, tức là nếu đã tiêu 3$ thì paying_type = 2, tiêu 6$ thì paying_type = 5, lấy mốc cận dưới gần nhất
            OnSetProperty("paying_type", 0);
            //update sau cùng các event liên quan tới level
            OnSetProperty("winMiniGameCount", DataManager.ins.gameSave.winMiniGameCount);
        }
    }

    private void OnSetProperty(string key, object value)
    {
        try
        {
            FirebaseAnalytics.SetUserProperty(key.ToString(), value.ToString());
        }
        catch
        {
            Debug.LogError("Lỗi UserProperty của Firebase: " + key);
        }
    }


    #region Events
    public void check_point(int check_point, string name_game_cur, string name_game_max)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            if (check_point < 10){
                FirebaseAnalytics.LogEvent("check_point_0" + check_point, new Parameter[]
                    {
                        new Parameter("name_game_cur", name_game_cur),
                        new Parameter("name_game_max", name_game_max)
                    });
            }else if(check_point < 30){
                FirebaseAnalytics.LogEvent("check_point_" + check_point, new Parameter[]
                    {
                        new Parameter("name_game_cur", name_game_cur),
                        new Parameter("name_game_max", name_game_max)
                    });
            }
        }
    }

    public void level_start(string nameGame)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("level_start", new Parameter[]
{
                new Parameter("name_game", nameGame)
});
        }
    }

    public void level_complete(string nameGame, int dieCount, int winMiniGameCount)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("level_complete", new Parameter[]
{
                new Parameter("name_game", nameGame),
                new Parameter("dieCount", dieCount),
                new Parameter("winMiniGameCount", winMiniGameCount)
});
        }
    }

    public void level_fail(string nameGame, int winMiniGameCount)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("level_fail", new Parameter[]
{
                new Parameter("name_game", nameGame),
                new Parameter("winMiniGameCount", winMiniGameCount)
});
        }
    }

    public void earn_virtual_currency(string virtual_currency_name, int amount, string source)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("earn_virtual_currency", new Parameter[]
            {
                new Parameter("virtual_currency_name", virtual_currency_name),
                new Parameter("value", amount),
                new Parameter("source", source)
               });
        }
    }

    public void spend_virtual_currency(string virtual_currency_name, int amount, string item_name)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("spend_virtual_currency", new Parameter[]
            {
                new Parameter("virtual_currency_name", virtual_currency_name),
                new Parameter("value", amount),
                new Parameter("item_name", item_name)
               });
        }
    }



    public void ads_reward_offer(string placement)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_reward_offer", new Parameter[]
            {
                new Parameter("placement", placement)
               });
        }
    }
    public void ads_reward_click(string placement)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_reward_click", new Parameter[]
            {
                new Parameter("placement", placement)
               });
        }
    }
    public void ads_reward_show(string placement)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_reward_show", new Parameter[]
            {
                new Parameter("placement", placement)
               });
        }
    }
    public void ads_reward_fail(string placement, string errormsg)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_reward_fail", new Parameter[]
            {
                new Parameter("placement", placement),
                new Parameter("errormsg", errormsg)
               });
        }
    }
    public void ads_reward_complete(string placement)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_reward_complete", new Parameter[]
            {
                new Parameter("placement", placement),
               });
        }
    }

    public void ads_inter_load()
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_inter_load");
        }
    }
    public void ads_inter_click()
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_inter_click");
        }
    }
    public void ads_inter_show()
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_inter_show");
        }
    }
    public void ads_inter_fail(string errormsg)
    {
        if (!Debug.isDebugBuild && !Application.isEditor)
        {
            FirebaseAnalytics.LogEvent("ads_inter_fail", new Parameter[]
            {
                new Parameter("errormsg", errormsg)
               });
        }
    }
    #endregion


    #region Remote Config

    public void fetch(Action<bool> completionHandler) {
        // TODO: RELEASE時にここを外す
        try {
            var settings = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ConfigSettings;
            settings.IsDeveloperMode = true;
            Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.SetConfigSettingsAsync(settings);

            System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAsync(new System.TimeSpan(0));
            string config_name = "asset_bundles_config_" + Application.version.Replace(".", "");
#if UNITY_ANDROID
            config_name = "android_asset_bundles_config_" + Application.version.Replace(".", "");
#elif UNITY_IOS
             config_name = "ios_asset_bundles_config_" + Application.version.Replace(".", "");
#endif
            fetchTask.ContinueWith(task => {
                if(task.IsCanceled || task.IsFaulted) {
                    //if(PlayerPrefs.HasKey(config_name))
                    //{
                    //    string content_asset_bundles_config = PlayerPrefs.GetString(config_name);
                    //    _asset_bundles_config = JsonConvert.DeserializeObject<List<AssetBundlesConfig>>(content_asset_bundles_config);
                    //}
                    Debug.LogWarning("fetchTask Firebase Fail");
                    is_result_fetch_result = true;
                } else {
                    Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ActivateAsync();
                    RefrectProperties(config_name);
                    is_result_fetch_result = true;
                }
            });
        } catch(Exception ex) {
            Debug.Log(ex.ToString());
        }
        // StartCoroutine(ie_save_value(config_name));
    }

    IEnumerator ie_save_value(string config_name) {
        yield return new WaitUntil(() => is_result_fetch_result == true);
        if(is_fetch_with_firebase) {
            PlayerPrefs.SetString(config_name, Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue(config_name).StringValue);
            PlayerPrefs.Save();
        }
    }

    private void RefrectProperties(string config_name) {
        try {
            bool keyVer = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue("Enable_AppOpenAd").BooleanValue;
            Debug.LogWarning("??? Enable_AppOpenAd = " + keyVer);
            DataManager.ins.enable_AppOpenAds = keyVer;//Mặc định là bật AppOpenAds

            long keyVerB = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue("Time_Loading").LongValue;
            Debug.LogWarning("??? Time_Loading = " + keyVerB);
            DataManager.ins.time_Loading = keyVerB;//Mặc định là 3
            /*
            string keyVerB = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("tutorial_BattleIO").StringValue;
            Debug.LogWarning("???" + keyVerB);
            DataLoader.isHaveTutorial = true;
            
            if (keyVerB.ToLower().Contains("0")) {
                DataLoader.isHaveTutorial = false;//Bản A là no tut
            } else if(keyVerB.ToLower().Contains("1")) {
                DataLoader.isHaveTutorial =  true;//Bản B là có tut
            } else {
                Debug.LogError("Lỗi A B test");
            }
            

            DataLoader.moveSpeed = 3.1f;
            try {
                double keyMoveSpeed = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("moveSpeed").DoubleValue;
                Debug.LogWarning("???" + keyMoveSpeed);
                DataLoader.moveSpeed = keyMoveSpeed;
            } catch (Exception e) {
                Debug.LogError("Lỗi A B test MoveSpeed " + e.ToString());
            }
            
            //AB test thêm cả Survival
            string keyDefaultMode = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("defaultMode").StringValue;
            if (keyDefaultMode.ToLower().Contains("1")) {
                DataLoader.isDefaultMode_ZombieCity = false;//Bản A là Classic
            } else if (keyDefaultMode.ToLower().Contains("2")) {
                DataLoader.isDefaultMode_ZombieCity = true;//Bản B là Survival
            } else {
                Debug.LogError("Lỗi A B test 1");
            }
            Debug.LogWarning("??? DefaultMode:" + keyDefaultMode);
            */
            /*
            //AB test ability đầu tiên
            long idAbility = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ability_FirstGame").LongValue;
            if (idAbility >= GameManager.instance.listAbility.Length || idAbility == 0) {
                Debug.LogError("Lỗi A B test 2");
            } else {
                DataLoader.ability_ZombieCity_FirstGame = idAbility;
            }
            Debug.LogWarning("??? Ability:" + idAbility);

            //AB test ngày mở DayZ
            long dayOpen_DayZ = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("open_DayZ").LongValue;
            if (dayOpen_DayZ <= 0) {
                Debug.LogError("Lỗi A B test 3");
            } else {
                DataLoader.dayOpen_DayZ = dayOpen_DayZ;
            }
            Debug.LogWarning("??? DayZ open:" + dayOpen_DayZ);

            //AB test độ khó Survival
            long keyAI_Survival = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AI_Survival").LongValue;
            DataLoader.AI_Survival = keyAI_Survival;
            Debug.LogWarning("??? AI_Survival:" + keyAI_Survival);

            //AB test độ khó ZombieCity
            long keyAI_ZombieCity = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AI_ZombieCity").LongValue;
            DataLoader.AI_ZombieCity = keyAI_ZombieCity;
            Debug.LogWarning("??? AI_ZombieCity:" + keyAI_ZombieCity);

            //AB test vào game thì chơi luôn
            long play_FirstOpen = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("play_FirstOpen").LongValue;
            DataLoader.play_FirstOpen = play_FirstOpen;
            Debug.LogWarning("??? play_FirstOpen:" + play_FirstOpen);
            */
            /*
            //AB test chỉ cho User xem VideoAds của CharSkin 1 lần mỗi đồ
           string keyVideoAds_CharSkin_1Time = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("videoAds_CharSkin_1Time").StringValue;
           Debug.LogWarning("??? unlockSkin_10Minutes:" + keyVideoAds_CharSkin_1Time);
           if (keyVideoAds_CharSkin_1Time.ToLower().Contains("0")) {
               DataLoader.videoAds_CharSkin_1Time = false;//Bản A là cho User unlock bằng VideoAds liên tục
           } else if(keyVideoAds_CharSkin_1Time.ToLower().Contains("1")) {
               DataLoader.videoAds_CharSkin_1Time =  true;//Bản B là chir cho User unlock bằng VideoAds 1 lần mỗi bộ
            } else {
               Debug.LogError("Lỗi A B test");
           }*/
            /*
            //AB test thêm cả sound
            string keySoundOn = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("soundOn").StringValue;
            if (keySoundOn.ToLower().Contains("0")) {
                DataLoader.isSoundOn = false;//Bản A là Sound Off
            } else if (keySoundOn.ToLower().Contains("1")) {
                DataLoader.isSoundOn = true;//Bản B là Sound On
            } else {
                Debug.LogError("Lỗi A B test 2");
            }
            Debug.LogWarning("??? SoundOn:" + keySoundOn);

            //AB test thêm có CharacterSkin ko?
            string keyCharSkin = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("characterSkin").StringValue;
            if (keyCharSkin.ToLower().Contains("0"))
            {
                DataLoader.isHaveCharSkin = false;//Bản A là Ko có
            }
            else if (keyCharSkin.ToLower().Contains("1"))
            {
                DataLoader.isHaveCharSkin = true;//Bản B là Có
            }
            else
            {
                Debug.LogError("Lỗi A B test 3");
            }
            Debug.LogWarning("??? CharSkin:" + keyCharSkin);

            //AB test ZombieCity mở lúc nào
            //long keyTimeOpen_ZombieCity = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("timeOpen_ZombieCity").LongValue;
            //DataLoader.timeOpen_ZombieCity = keyTimeOpen_ZombieCity;
            //Debug.LogWarning("??? timeOpen_ZombieCity:" + keyTimeOpen_ZombieCity);

            //AB test ZombieCity mở lúc nào
            long keyTimeOpen_ZombieCity_Minutes = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("timeOpen_ZombieCity_Minutes").LongValue;
            DataLoader.timeOpen_ZombieCity_Minutes = keyTimeOpen_ZombieCity_Minutes;
            Debug.LogWarning("??? timeOpen_ZombieCity:" + keyTimeOpen_ZombieCity_Minutes);
            */
            string content_asset_bundles_config = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetValue(config_name).StringValue;
            //_asset_bundles_config = JsonConvert.DeserializeObject<List<AssetBundlesConfig>>(content_asset_bundles_config);
            is_fetch_with_firebase = true;
            is_result_fetch_result = true;
        } catch(Exception ex) {
            Debug.Log("xxxxxxxxxxxxxxxxxxx: " + ex.Message);
        }
    }


    #endregion
}
