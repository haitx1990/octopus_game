﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxManager : MonoBehaviour
{
    public static MaxManager Ins;

    private const string MaxSdkKey = "ZoNyqu_piUmpl33-qkoIfRp6MTZGW9M5xk1mb1ZIWK6FN9EBu0TXSHeprC3LMPQI7S3kTc1-x7DJGSV8S-gvFJ";
    private const string InterstitialAdUnitId = "3f0b2d46fdf4b60b";
    private const string RewardedAdUnitId = "2484bbd96abde889";
    private const string BannerAdUnitId = "57bc52c808044aae";
    private const string InterstitialAdUnitId_IOS = "6fbf05ae0f5de6af";
    private const string RewardedAdUnitId_IOS = "0b9f33c1a3f00b58";
    private const string BannerAdUnitId_IOS = "bf7fb4b6bdea9ad2";

    private bool isBannerShowing;
    private bool isMRecShowing;

    private int interstitialRetryAttempt;
    private int rewardedRetryAttempt;
    private int rewardedInterstitialRetryAttempt;

    public float timeWatchAds;
    public float timeWatchAdsInter;
    Action OnRewardAds_Finish;
    Action OnInter_Finish, OnInter_Showed;
    bool ShowingVideoAds = true;
    public bool isIOS = false;

    private void Awake()
    {
        if (Ins != null)
        {
            Destroy(this.gameObject);
            return;
        }
        Ins = this;
        DontDestroyOnLoad(this.gameObject);

#if UNITY_IOS || UNITY_IPHONE
        isIOS = true;
#endif
    }

    public void OnStart()
    {
        timeWatchAds = 30;
        timeWatchAdsInter = 30;
        MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration =>
        {
            // AppLovin SDK is initialized, configure and start loading ads.
            //Debug.Log("MAX SDK Initialized");
            //MaxSdk.ShowMediationDebugger();

            InitializeInterstitialAds();
            InitializeRewardedAds();
            InitializeBannerAds();
        };

        MaxSdk.SetSdkKey(MaxSdkKey);
        MaxSdk.InitializeSdk();
    }

    private void Update()
    {
        timeWatchAds += Time.deltaTime;
        timeWatchAdsInter += Time.deltaTime;
    }

    #region Interstitial Ad Methods

    public void InitializeInterstitialAds()
    {
        // Attach callbacks
        MaxSdkCallbacks.OnInterstitialLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.OnInterstitialLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.OnInterstitialDisplayedEvent += OnInterstitialDisplayedEvent;
        MaxSdkCallbacks.OnInterstitialHiddenEvent += OnInterstitialDismissedEvent;

        // Load the first interstitial
        LoadInterstitial();
    }

    public void LoadInterstitial()
    {
        if (DataManager.ins.gameSave.isNoAds) return;

        if (Application.internetReachability == NetworkReachability.NotReachable) return;

        MaxSdk.LoadInterstitial(isIOS ? InterstitialAdUnitId_IOS : InterstitialAdUnitId);
    }

    public void ShowInterstitial(string placement = "", Action OnFinish = null, Action OnShowed = null)
    {
        try
        {
            if (Application.isEditor || GameManager.ins.isSkipAds || DataManager.ins.gameSave.isNoAds || timeWatchAds < 15 || timeWatchAdsInter < 15)
            {
                OnFinish?.Invoke();
                return;
            }
            OnInter_Finish = OnFinish;
            OnInter_Showed = OnShowed;
            FirebaseManager.ins.ads_inter_click();
            if (MaxSdk.IsInterstitialReady(isIOS ? InterstitialAdUnitId_IOS : InterstitialAdUnitId))
            {
                Dictionary<string, string> value = new Dictionary<string, string>();
                AppsFlyer.trackRichEvent("show_inter", value);
                FirebaseManager.ins.ads_inter_show();
                timeWatchAdsInter = 0;
                MaxSdk.ShowInterstitial(isIOS ? InterstitialAdUnitId_IOS : InterstitialAdUnitId);
            }
            else
            {
                Debug.LogError("Lỗi chưa load đc Inter");
                LoadInterstitial();
                OnInter_Finish?.Invoke();
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Lỗi Inter: " + e);
            OnInter_Finish?.Invoke();
        }
    }

    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(isIOS ? InterstitialAdUnitId_IOS : InterstitialAdUnitId) will now return 'true'
        Debug.Log("Interstitial loaded");

    }

    private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
    {
        // Interstitial ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        double retryDelay = 5;

        Debug.Log("Interstitial failed to load with error code: " + errorCode);

        Invoke("LoadInterstitial", (float)retryDelay);
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        // Interstitial ad failed to display. We recommend loading the next ad
        Debug.Log("Interstitial failed to display with error code: " + errorCode);
        FirebaseManager.ins.ads_inter_fail("Interstitial failed to display with error code: " + errorCode);
        LoadInterstitial();
        OnInter_Finish?.Invoke();
    }
    private void OnInterstitialDisplayedEvent(string adUnitId)
    {
        // Fired when an interstitial ad is displayed (may not be received by Unity until the interstitial closes)
        Debug.Log("Interstitial Displayed");
        OnInter_Showed?.Invoke();
    }

    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        // Interstitial ad is hidden. Pre-load the next ad
        Debug.Log("Interstitial dismissed");
        OnInter_Finish?.Invoke();
        LoadInterstitial();
        timeWatchAdsInter = 0;
    }

    #endregion

    #region Rewarded Ad Methods

    private void InitializeRewardedAds()
    {
        // Attach callbacks
        MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
        MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
        MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
        MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
        MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

        // Load the first RewardedAd
        LoadRewardedAd();
    }

    public bool isRewardedVideoAvailable()
    {
        return MaxSdk.IsRewardedAdReady(isIOS ? RewardedAdUnitId_IOS : RewardedAdUnitId);
    }

    private void LoadRewardedAd()
    {
        FirebaseManager.ins.ads_inter_load();
        MaxSdk.LoadRewardedAd(isIOS ? RewardedAdUnitId_IOS : RewardedAdUnitId);
    }

    public bool ShowRewardedAd(string nameEvent = "", Action OnFinish = null)
    {
        try
        {
            if (Application.isEditor || GameManager.ins.isSkipAds)
            {
                OnFinish?.Invoke();
                return true;
            }
            OnRewardAds_Finish = OnFinish;
            ShowingVideoAds = true;
            FirebaseManager.ins.ads_reward_click(nameEvent);
            if (MaxSdk.IsRewardedAdReady(isIOS ? RewardedAdUnitId_IOS : RewardedAdUnitId))
            {
                Dictionary<string, string> value = new Dictionary<string, string>();
                AppsFlyer.trackRichEvent("show_rewarded", value);
                FirebaseManager.ins.ads_reward_show(nameEvent);
                MaxSdk.ShowRewardedAd(isIOS ? RewardedAdUnitId_IOS : RewardedAdUnitId);
                return true;
            }
            else
            {
                Debug.Log("Lỗi chưa load đc Video Ads");
                LoadRewardedAd();
                return false;
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Lỗi VideoAds: " + e);
            return false;
        }
    }

    private void OnRewardedAdLoadedEvent(string adUnitId)
    {
        // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(isIOS ? RewardedAdUnitId_IOS : RewardedAdUnitId) will now return 'true'
        Debug.Log("Rewarded ad loaded");
    }

    private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
    {
        // Rewarded ad failed to load. We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds).
        double retryDelay = 5;

        Debug.Log("Rewarded ad failed to load with error code: " + errorCode);

        Invoke("LoadRewardedAd", (float)retryDelay);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        // Rewarded ad failed to display. We recommend loading the next ad
        Debug.Log("Rewarded ad failed to display with error code: " + errorCode);
        FirebaseManager.ins.ads_reward_fail("", "Rewarded ad failed to display with error code: " + errorCode);
        LoadRewardedAd();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId)
    {
        Debug.Log("Rewarded ad displayed");
    }

    private void OnRewardedAdClickedEvent(string adUnitId)
    {
        Debug.Log("Rewarded ad clicked");
    }

    private void OnRewardedAdDismissedEvent(string adUnitId)
    {
        // Rewarded ad is hidden. Pre-load the next ad
        Debug.Log("Rewarded ad dismissed");
        LoadRewardedAd();
        if (!ShowingVideoAds)
        {
            //FirebaseManager.ins.SendAdsRewardComplete();
            //isWatchAds = true;
            timeWatchAds = 0;
            //Manager.Instance.UpdateUserAds();
            //AppsFlyerObjectScript.Instance.TrackAppflyerAds();
            OnRewardAds_Finish();
        }
    }

    private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
    {
        // Rewarded ad was displayed and user should receive the reward
        Debug.Log("Rewarded ad received reward");
        ShowingVideoAds = false;
    }
    #endregion

    #region Banner Ad Methods

    public void InitializeBannerAds()
    {
        if (!DataManager.ins.gameSave.isNoAds)
        {
            // Banners are automatically sized to 320x50 on phones and 728x90 on tablets.
            // You may use the utility method `MaxSdkUtils.isTablet()` to help with view sizing adjustments.
            MaxSdk.CreateBanner(isIOS ? BannerAdUnitId_IOS : BannerAdUnitId, MaxSdkBase.BannerPosition.BottomCenter);

            // Set background or background color for banners to be fully functional.
            MaxSdk.SetBannerBackgroundColor(isIOS ? BannerAdUnitId_IOS : BannerAdUnitId, Color.black);
        }
    }

    public void ShowBanner()
    {
#if UNITY_EDITOR
        return;
#endif
        if (!DataManager.ins.gameSave.isNoAds)
        {
            MaxSdk.ShowBanner(isIOS ? BannerAdUnitId_IOS : BannerAdUnitId);
            isBannerShowing = true;
        }
    }

    public void HideBanner()
    {
        MaxSdk.HideBanner(isIOS ? BannerAdUnitId_IOS : BannerAdUnitId);
        isBannerShowing = false;
    }

    #endregion

}