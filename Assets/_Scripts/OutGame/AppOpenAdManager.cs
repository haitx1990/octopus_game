﻿using System;
using GoogleMobileAds.Api;
using UnityEngine;

public class AppOpenAdManager : MonoBehaviour{
#if UNITY_ANDROID
    //private const string AD_UNIT_ID = "ca-app-pub-3940256099942544/3419835294";//Key Test 
    private const string AD_UNIT_ID = "ca-app-pub-9819920607806935/2521877165";
#elif UNITY_IOS
    private const string AD_UNIT_ID = "ca-app-pub-3940256099942544/5662855259";
#else
    private const string AD_UNIT_ID = "unexpected_platform";
#endif

    //private static AppOpenAdManager instance;

    private AppOpenAd ad;

    private bool isShowingAd = false;

    private DateTime loadTime;

    public static AppOpenAdManager Instance;

    public bool IsAdAvailable {
        get {
            return ad != null;
            //&& (System.DateTime.UtcNow - loadTime).TotalHours < 4;
        }
    }

    private void Awake() {
        if(Instance != null) {
            Destroy(gameObject);
            return;
        } else {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    public void Start() {
        MobileAds.Initialize(initStatus => {
            // Load an app open ad when the scene starts
            AppOpenAdManager.Instance.LoadAd();
        });
    }

    public void OnApplicationPause(bool paused) {
        // Display the app open ad when the app is foregrounded
        if(!paused) {
            
            //AppOpenAdManager.Instance.ShowAdIfAvailable();
        }
    }

    public void LoadAd() {
        AdRequest request = new AdRequest.Builder().Build();
        // Load an app open ad for portrait orientation
        AppOpenAd.LoadAd(AD_UNIT_ID, ScreenOrientation.Portrait, request, ((appOpenAd, error) => {
            if(error != null) {
                // Handle the error.
                Debug.LogFormat("Failed to load the ad. (reason: {0})", error.LoadAdError.GetMessage());
                return;
            }
            // App open ad is loaded.
            ad = appOpenAd;
            loadTime = DateTime.UtcNow;
        }));
    }

    public void ShowAdIfAvailable() {
        if(Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) {
            return;
        }
        if(!IsAdAvailable || isShowingAd) {
            return;
        }
        ad.OnAdDidDismissFullScreenContent += HandleAdDidDismissFullScreenContent;
        ad.OnAdFailedToPresentFullScreenContent += HandleAdFailedToPresentFullScreenContent;
        ad.OnAdDidPresentFullScreenContent += HandleAdDidPresentFullScreenContent;
        ad.OnAdDidRecordImpression += HandleAdDidRecordImpression;
        ad.OnPaidEvent += HandlePaidEvent;

        ad.Show();
    }

    private void HandleAdDidDismissFullScreenContent(object sender, EventArgs args) {
        Debug.Log("Closed app open ad");
        // Set the ad to null to indicate that AppOpenAdManager no longer has another ad to show.
        ad = null;
        isShowingAd = false;
        //LoadAd();//Vì chỉ hiển thị 1 lần lúc đầu game -> Ko cần LoadAd sau lần hiện đầu tiên
    }

    private void HandleAdFailedToPresentFullScreenContent(object sender, AdErrorEventArgs args) {
        Debug.LogFormat("Failed to present the ad (reason: {0})", args.AdError.GetMessage());
        // Set the ad to null to indicate that AppOpenAdManager no longer has another ad to show.
        ad = null;
        LoadAd();
    }

    private void HandleAdDidPresentFullScreenContent(object sender, EventArgs args) {
        Debug.Log("Displayed app open ad");
        isShowingAd = true;
    }

    private void HandleAdDidRecordImpression(object sender, EventArgs args) {
        Debug.Log("Recorded ad impression");
    }

    private void HandlePaidEvent(object sender, AdValueEventArgs args) {
        Debug.LogFormat("Received paid event. (currency: {0}, value: {1}",
                args.AdValue.CurrencyCode, args.AdValue.Value);
    }
}