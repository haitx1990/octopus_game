﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class IapServices : MonoBehaviour, IStoreListener
{
    private IStoreController m_store_controller;
    private IExtensionProvider m_store_extension_provider;
    private IAppleExtensions m_AppleExtensions;

    private Action callBackSuccess, callBackFailed;
    static IapServices _me;
    public static IapServices me
    {
        get
        {
            if (_me == null)
            {
                GameObject gob = new GameObject("IapServices");
                _me = gob.AddComponent<IapServices>();
                _me.Init();
                DontDestroyOnLoad(gob);
            }
            return _me;
        }
    }


    void Awake()
    {

    }


    void Start()
    {
        if (_me == null)
        {
            _me = this;
            DontDestroyOnLoad(gameObject);
            //Tạm thời tắt trên IOS

            Init();

        }
    }

    void Init()
    {

        try
        {
            if (IsInitialized())
            {
                return;
            }
            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            builder.AddProduct("move_no_ads", ProductType.NonConsumable, new IDs
            {
                {"move_no_ads",GooglePlay.Name},
            });/*
            builder.AddProduct(Definition.product_id.gold_pack_10.ToString(), ProductType.Consumable, new IDs
            {
                {Definition.product_id.gold_pack_10.ToString(),GooglePlay.Name},
            });
            builder.AddProduct(Definition.product_id.skin_pack_5.ToString(), ProductType.NonConsumable, new IDs
            {
                {Definition.product_id.skin_pack_5.ToString(),GooglePlay.Name},
            });*/

            UnityPurchasing.Initialize(this, builder);
            Debug.Log("==========================UnityPurchasing successful !");
        }
        catch (Exception Ex)
        {
            Debug.Log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx :" + Ex.Message);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_store_controller = controller;
        m_store_extension_provider = extensions;
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_store_extension_provider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                if (result)
                {
                    for (int i = 0; i < m_store_controller.products.all.Length; i++)
                    {
                        if (m_store_controller.products.all[i].hasReceipt)
                        {
                            Debug.Log("Receipt " + m_store_controller.products.all[i].receipt);
                            /*
                            if (string.Equals(m_store_controller.products.all[i].definition.id, Definition.product_id.move_no_ads.ToString(), StringComparison.Ordinal)) {
                                DataManager.ins.gameSave.isRemoveAds = true;
                                GameManager.ins.CheckBanner();
                                DataManager.ins.SaveGame();
                                if(SceneManager.ins.popup_Shop != null) SceneManager.ins.popup_Shop.obj_NoAds.SetActive(false);
                            } else if (string.Equals(m_store_controller.products.all[i].definition.id, Definition.product_id.skin_pack_5.ToString(), StringComparison.Ordinal)) {

                                for (int j = 0; j < DataManager.ins.gameSave.listCharSkin.Length; j++) {
                                    DataManager.ins.gameSave.listCharSkin[j].isUnlock = true;
                                    DataManager.ins.gameSave.listCharSkin[j].isBuy = true;
                                }
                                DataManager.ins.gameSave.isUnlockAllSkin = true;
                                DataManager.ins.SaveGame();
                                if (SceneManager.ins.popup_Shop != null) SceneManager.ins.popup_Shop.obj_skin.SetActive(false);
                            }
                            //ShowPopup RestorePurchase thành công
                            SceneManager.ins.ShowPopup_RestorePurchases();*/
                        }
                    }
                }
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    public bool IsInitialized()
    {
        Debug.Log("m_store_controller" + (m_store_controller != null));
        Debug.Log("m_store_extension_provider" + (m_store_extension_provider != null));
        // Only say we are initialized if both the Purchasing references are set.
        return m_store_controller != null && m_store_extension_provider != null;
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
#if UNITY_ANDROID || UNITY_IOS
        string producid_purchased = args.purchasedProduct.definition.id;

        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
        var result = validator.Validate(args.purchasedProduct.receipt);

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (callBackSuccess == null)
            {
                Debug.LogError("===================================c callback success ==null");
                return PurchaseProcessingResult.Complete;
            }

            try
            {
                foreach (IPurchaseReceipt productReceipt in result)
                {
                    Debug.Log(productReceipt.productID);
                    Debug.Log(productReceipt.purchaseDate);
                    Debug.Log(productReceipt.transactionID);
                }
                Debug.Log("====================purchased success: " + producid_purchased);
                this.callBackSuccess();
                this.callBackSuccess = null;


            }
            catch (Exception ex)
            {
                Debug.LogError("============================================ errorpurchaser:  " + ex.ToString());
            }
        }
        return PurchaseProcessingResult.Complete;
#endif
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason p)
    {

        if (p == PurchaseFailureReason.DuplicateTransaction)
        {
            callBackSuccess();
            return;
        }

        callBackFailed();
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, p));
    }


    public void BuyIAP(string product_id, Action callBackSuccess, Action callBackFailed)
    {
        this.callBackSuccess = callBackSuccess;
        this.callBackFailed = callBackFailed;

        if (Application.platform == RuntimePlatform.WindowsEditor)
        {
            callBackSuccess();
            return;
        }

        try
        {
            if (IsInitialized())
            {

                Product product = m_store_controller.products.WithID(product_id.ToString());
                if (product != null && product.availableToPurchase)
                {
                    Debug.LogError(string.Format("1Purchasing product asychronously: '{0}'", product.definition.id));
                    m_store_controller.InitiatePurchase(product);
                }
                else
                {
                    Debug.LogError("1BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                }
            }
            else
            {
                Debug.LogError("1BuyProductID FAIL. Not initialized.");
                Init();
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            Debug.Log(product_id + "------------------- Error");
        }
    }

    /*
    public string get_price_localize(Definition.product_id product_id)
    {
        try
        {
            return m_store_controller.products.WithID(product_id.ToString()).metadata.localizedPriceString;
        }
        catch (Exception ex)
        {
            string prize = 
                //product_id == Definition.product_id.starter ? "3.99$" :
               //product_id == Definition.product_id.super ? "11.99$" :
               //product_id == Definition.product_id.mega ? "24.99$" :
               //product_id == Definition.product_id.ultimate ? "49.99$" :
               product_id == Definition.product_id.gem_pack_2 ? "1.99$" :
               product_id == Definition.product_id.gem_pack_4 ? "3.99$" :
               product_id == Definition.product_id.gem_pack_10 ? "9.99$" :
               product_id == Definition.product_id.gem_pack_20 ? "19.99$" :
               product_id == Definition.product_id.gem_pack_40 ? "39.99$" : "3.99$";        
            return prize;
        }
    }*/



}
