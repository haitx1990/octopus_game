﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DataManager : MonoBehaviour
{
    public static DataManager ins;

    public bool isLoaded = false;
    public bool enable_AppOpenAds = true;
    public long time_Loading = 8;

    public GameSave gameSave;
    private GameSave gameSave_BackUp;//Luôn giống Data gốc, nhưng sẽ check để ko thể bị lỗi

    #region Unity
    private void Awake()
    {
        if (ins != null)
        {
            Destroy(this.gameObject);
            return;
        }
        ins = this;
        //DontDestroyOnLoad(this.gameObject);
    }

    private void OnApplicationPause(bool pause) { SaveGame(); }
    private void OnApplicationQuit() { SaveGame(); }
    #endregion

    public void LoadData()
    {
        //Load Data mới
        if (isLoaded == false)
        {
            //Load data cũ
            if (PlayerPrefs.HasKey("GameSave")) gameSave = JsonUtility.FromJson<GameSave>(PlayerPrefs.GetString("GameSave"));
            //Tạo Data mới
            if (gameSave.isNew) {
                gameSave = new GameSave();
            }
            else
            {
                gameSave.sessionCount++;
                DateTime time = DateTime.ParseExact(gameSave.timeLastOpen, "dd/MM/yyyy HH:mm:ss", null);
                if ((time.Year * 366 + DateTime.Now.Month * 31 + DateTime.Now.Day) > (time.Year * 366 + time.Month * 31 + time.Day))//Nếu sang ngày mới thì ??
                {
                    gameSave.daysPlayed++;
                    gameSave.playTime_Session = 0;
                }
                gameSave.timeLastOpen = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            }
            //Cheat
            if (GameManager.ins.isSkipAds) gameSave.isNoAds = true;
            MaxManager.Ins.OnStart();

            //Load skin char
            LoadListCharSkin();

            isLoaded = true;
            SaveGame();
        }
    }
     
    public void SaveGame()
    {
        if (!isLoaded) return;

        if (gameSave == null)
        {
            if (gameSave_BackUp != null)
            {
                gameSave = gameSave_BackUp;
                Debug.LogError("gameSave bị null, backup thành công");
            }
            else
            {
                gameSave = new GameSave();
                Debug.LogError("gameSave bị null, backup ko thành công. Reset data");
            }
        }
        gameSave_BackUp = gameSave;
        
        PlayerPrefs.SetString("GameSave", JsonUtility.ToJson(gameSave));
        PlayerPrefs.Save();
    }

    public void ChangeGold(int amount, string nameEvent, bool isFirebase = true)
    {
        if (amount == 0) return;
        gameSave.gold += amount;
        if(SceneManager.ins.form_Home != null) SceneManager.ins.form_Home.txt_Gold.text = gameSave.gold + "";
        if (isFirebase)
        {
            if (amount > 0)
            {
                FirebaseManager.ins.earn_virtual_currency("money", Mathf.Abs(amount), nameEvent);
            }
            else
            {
                FirebaseManager.ins.spend_virtual_currency("money", Mathf.Abs(amount), nameEvent);
            }
        }
        SaveGame();
    }

    public void LoadListCharSkin()
    {
        //Load lại data của skin tóc
        Skin_Save[] listSkins_Hair_Case = new Skin_Save[GameManager.ins.listSkins_Hair.Length];
        for (int i = 0; i < GameManager.ins.listSkins_Hair.Length; i++)
        {
            listSkins_Hair_Case[i] = new Skin_Save(i, GameManager.ins.listSkins_Hair[i].keyID);
            //Nếu có data cũ thì truyền vào
            for (int j = 0; gameSave.listSkins_Hair != null && j < gameSave.listSkins_Hair.Length; j++)
            {
                if (listSkins_Hair_Case[i].keyID == gameSave.listSkins_Hair[j].keyID)
                {
                    listSkins_Hair_Case[i].isUnlock = gameSave.listSkins_Hair[j].isUnlock;
                    listSkins_Hair_Case[i].amountVideAds = gameSave.listSkins_Hair[j].amountVideAds;
                }
            }
            if (i == 0)
            {
                listSkins_Hair_Case[i].isUnlock = true;
            }
        }
        gameSave.listSkins_Hair = listSkins_Hair_Case;

        //Load lại data của skin Body
        Skin_Save[] listSkins_Body_Case = new Skin_Save[GameManager.ins.listSkins_Body.Length];
        for (int i = 0; i < GameManager.ins.listSkins_Body.Length; i++)
        {
            listSkins_Body_Case[i] = new Skin_Save(i, GameManager.ins.listSkins_Body[i].keyID);
            //Nếu có data cũ thì truyền vào
            for (int j = 0; gameSave.listSkins_Body != null && j < gameSave.listSkins_Body.Length; j++)
            {
                if (listSkins_Body_Case[i].keyID == gameSave.listSkins_Body[j].keyID)
                {
                    listSkins_Body_Case[i].isUnlock = gameSave.listSkins_Body[j].isUnlock;
                    listSkins_Body_Case[i].amountVideAds = gameSave.listSkins_Body[j].amountVideAds;
                }
            }
            if (i == 0)
            {
                listSkins_Body_Case[i].isUnlock = true;
            }
        }
        gameSave.listSkins_Body = listSkins_Body_Case;
    }
}
