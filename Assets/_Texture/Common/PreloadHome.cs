﻿using UnityEngine;

namespace ZombieIdle.MenuScripts
{
    public class PreloadHome : MonoBehaviour
    {
        private static PreloadHome instance = null;
        public static PreloadHome Instance => instance;
        ~PreloadHome() => Debug.Log("OnDispose");
        public static void Dispose() => instance = null;
        private void Awake() => instance = this;
            
        public Sprite iconCoin;
        public Sprite iconE99;
        public Sprite iconPart;

        public Sprite[] machineGunSprites;
        public Sprite[] pistolGunSpirtes;
        public Sprite[] berserkerWeaponSprite;
        public Sprite[] heavyGunSprites;
        
    }
}