﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace ZombieIdle.Common
{
    public class ParticleSystemCallback : MonoBehaviour
    {
        public bool IsRoot = false;
        public GameObject rootObject;

        private UnityAction actionCallback;
        public bool IsDontDespawnOnDone = false;

        public ParticleSystemCallback SetCallback(UnityAction callback)
        {
            this.actionCallback = callback;
            return this;
        }

        public ParticleSystemStopAction GetStopAction()
        {
            var _main = GetComponent<ParticleSystem>().main;
            return _main.stopAction;
        }

        public void SetRootObject()
        {
            rootObject = gameObject;
        }

        protected virtual void OnParticleSystemStopped()
        {
            if (actionCallback != null)
            {
                actionCallback.Invoke();
            }
            if (!IsDontDespawnOnDone)
            {
                //rootObject.GetComponent<PoolObject>().TurnOff();
                if (rootObject != null)
                    SimplePool.Despawn(rootObject);
                else
                    SimplePool.Despawn(gameObject);
            }
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ParticleSystemCallback), true)]
    [CanEditMultipleObjects]
    public class ParticleSystemCallbackEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var myScript = target as ParticleSystemCallback;
            serializedObject.Update();

            if (myScript.GetStopAction() != ParticleSystemStopAction.Callback)
            {
                EditorGUILayout.HelpBox("Stop Action of main ParticleSystem not callback!", MessageType.Error);
            }

            myScript.IsDontDespawnOnDone = GUILayout.Toggle(myScript.IsDontDespawnOnDone, "Dont Despawn On Done");

            EditorGUI.BeginDisabledGroup(myScript.IsDontDespawnOnDone);

            if (myScript.IsDontDespawnOnDone)
            {
                myScript.rootObject = null;
                EditorGUILayout.HelpBox("Need to Set Callback on Play in code!", MessageType.Info);
            }
            else
            {
                myScript.IsRoot = GUILayout.Toggle(myScript.IsRoot, "Is Root");

                EditorGUI.BeginDisabledGroup(myScript.IsRoot);
                myScript.rootObject = (GameObject)EditorGUILayout.ObjectField("Root Object", myScript.rootObject, typeof(GameObject), true);
                EditorGUI.EndDisabledGroup();

                if (!myScript.IsRoot)
                {
                    if (myScript.rootObject == null)
                        EditorGUILayout.HelpBox("Require Root Object not null!", MessageType.Warning);
                    else if (myScript.rootObject.Equals(myScript.gameObject))
                    {
                        myScript.rootObject = null;
                    }
                }
                else
                {
                    myScript.SetRootObject();
                }
                EditorGUI.EndDisabledGroup();
            }
            if (GUI.changed)
            {
                EditorUtility.SetDirty(myScript);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
#endif
}