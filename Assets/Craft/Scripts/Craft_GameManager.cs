﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Craft_GameManager : MonoBehaviour
{
    public static Craft_GameManager Ins;

    public GameObject panelStart;
    //public GameObject panelAnimStart;

    public GameObject EfxBlood;

    [Header("Game Status")]
    private bool isStartGame;
    public bool isGamePlaying;

    [Header("UI")]
    public CanvasGroup TutObj;

    [Header("Time")]
    public Text txtTime;
    public float timeValue;
    string strTime;

    [Header("BOT")]
    public Transform transSpawn;
    public GameObject botPrefab;

    private void Awake()
    {
        Ins = this;
    }

    #region State
    public void Anim_Start_Complete()
    {
        isStartGame = true;
        isGamePlaying = true;
    }
    public void Game_Start()
    {
        isStartGame = true;
        isGamePlaying = true;
    }

    public void Game_Finish()
    {
        isStartGame = false;
        Craft_Camera.Instance.Set_Play();
        Debug.LogWarning("End game");
        StartCoroutine(Endgame(true));
    }
    public void Game_TimeUp()
    {
        isStartGame = false;
        isGamePlaying = false;
        Debug.LogWarning("Time Up");
        ShowPopup_EndGame(true);
    }
    public void Game_Dead()
    {
        Debug.LogWarning("Dead");
        isStartGame = false;
        isGamePlaying = false;
        StartCoroutine(ShowRevive());
    }

    IEnumerator Endgame(bool isWin)
    {
        yield return Yielders.Get(3f);
        //PanelEndgame.SetActive(true);
        ShowPopup_EndGame(isWin);
    }

    public void ShowPopup_EndGame(bool isWin = false)
    {
        if (isWin)
        {
            //Nếu đã chơi đủ số ngày rồi thì mới show WinAll
            if (DataManager.ins.gameSave.level >= GameManager.ins.listMiniGame.Length - 1)
            {
                SceneManager.ins.ShowPopup_WinAll();
            }
            else
            {//Nếu ko thì chỉ show WinMinigame
                SceneManager.ins.ShowPopup_EndGame(true);
            }
        }
        else SceneManager.ins.ShowPopup_EndGame(isWin);
    }

    public IEnumerator ShowRevive()
    {
        yield return Yielders.Get(1f);
        SceneManager.ins.ShowPopup_Revive(Game_Revive);
    }

    public void Game_Revive()
    {
        Craft_PlayerController.Ins.Game_Revive();
        isStartGame = true;
        isGamePlaying = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Game_Start();
        }
        if (isStartGame)
        {
            timeValue -= Time.deltaTime;
            
            if (timeValue <= 10)
            {
                strTime = "00:0" + (int)timeValue;
            }
            else strTime = "00:" + (int)timeValue;
            if (timeValue <= 0)
            {
                Game_TimeUp();
                strTime = "00:00";
            }
        }
        txtTime.text = strTime;
    }

    void IE_BotSpawn()
    {
        var r = Random.Range(10, 15);
        for (int i = 0; i < r; i++)
        {
            GameObject _map = Instantiate(botPrefab);
            _map.transform.localPosition = transSpawn.GetChild(i).transform.position;
            _map.transform.localRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            //_map.GetComponent<Craft_Character>().OnStart();

        }
    }

    #endregion



    #region StartGame

    public void Start()
    {
        //panelStart.SetActive(true);
        //Move_CameraFollow.Ins.state = 0;
        //OnStart(15);
        //IE_BotSpawn();
        TutObj.alpha = 1;
    }

    public void StartGame()
    {
        if (isStartGame) return;
        Game_Start();
        HideTut();
    }
    #endregion

    #region Support
    #region Tutorial
    public void ShowTut()
    {
        StartCoroutine(ie_ShowTut());
    }

    public void HideTut()
    {
        StartCoroutine(ie_HideTut());
    }

    IEnumerator ie_HideTut()
    {
        var t = 1f;
        TutObj.alpha = t;
        while (t >= 0)
        {
            t -= Time.deltaTime * 2;
            TutObj.alpha = t;
            yield return Yielders.FixedUpdate;
        }
    }

    IEnumerator ie_ShowTut()
    {
        var t = 0f;
        TutObj.alpha = t;
        while (t >= 0)
        {
            t -= Time.deltaTime * 2;
            TutObj.alpha = t;
            yield return Yielders.FixedUpdate;
        }
    }
    #endregion

    public Transform GetRandomPos()
    {
        var t = Move_MapConfig.Ins.List_AI.Count;
        return Move_MapConfig.Ins.List_AI[Random.Range(0, t)];
    }

    public List<Transform> DeepClone(List<Transform> l)
    {
        var res = new List<Transform>();
        foreach (var i in l) res.Add(i);
        return res;
    }
    #endregion

}