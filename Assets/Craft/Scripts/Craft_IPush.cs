﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Craft_IPush
{
    void AddImpact(Vector3 dir, float force);
}
