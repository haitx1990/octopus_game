﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craft_PushTrap : MonoBehaviour
{
    public Vector3 dir;
    public float force;

    private void OnTriggerEnter(Collider collision)
    {
        var t = collision.gameObject.GetComponent<Craft_IPush>();
        if (t != null)
        {
            Debug.Log(collision.gameObject.name + "_");
            t.AddImpact(dir, force);
        }
    }
}
