﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craft_TrapSaw : MonoBehaviour
{
    public bool Idle;
    public bool Move;

    public Transform Trans_Rotate;
    public float OffsetMin = 0.7f;
    public float OffsetMax = 0.7f;
    public float speed;

    private int _dir;
    private void OnEnable()
    {
        Trans_Rotate.DORotate(Vector3.forward * 360, 1f, RotateMode.FastBeyond360)
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Restart);
        _dir = Random.Range(0, 2) == 0 ? -1 : 1;
    }

    private void Update()
    {
        if (!Move) return;
        if(_dir == -1)
        {
            if (Trans_Rotate.localPosition.x >= OffsetMin)
            {
                Trans_Rotate.localPosition += Vector3.left * speed * Time.deltaTime;
            }
            else _dir = 1;
        }
        else
        {
            if (Trans_Rotate.localPosition.x <= OffsetMax)
            {
                Trans_Rotate.localPosition += Vector3.right * speed * Time.deltaTime;
            }
            else _dir = -1;
        }
    }

}
