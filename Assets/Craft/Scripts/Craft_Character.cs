﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craft_Character : MonoBehaviour, Craft_IPush
{
    public CharacterController controller;
    public Transform tran_Rotate;
    public Transform groundCheck;
    public LayerMask groundMask;
    bool isInGround;
    public float speed;
    public bool isCanMove = false;
    private bool _isDead;
    private bool _isJump;
    private float gravity = -9.8f;
    public float jumpHeight = 1.0f;
    Vector3 velocity;
    public Animator anim;
    public int Hp;
    RaycastHit hit;
    Vector3 _direction;

    public bool isSee = false;
    //public GameObject objHair;
    Vector3 impact = Vector3.zero;

    public void Start()
    {
        _direction = transform.TransformDirection(Vector3.forward);
        speed = Random.Range(4f, 8f);
        //if (speed > 6f) isSee = false;
        //objHair.SetActive(Random.Range(0,2) == 0 ? true : false);
        isCanMove = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "JumpBox")
        {
            Debug.Log("Call Jump");
            Jump();
        }
        else if (other.tag == "DieBox")
        {
            Debug.Log("Die");
            SetDied();
        }
        else if (other.tag == "Finishs")
        {
            isCanMove = false;
            controller.enabled = false;
            ani = Craft_Ani_State.Win; 
            ReloadAnimation();
        }
    }
    public void Jump()
    {
        _isJump = true;
        
        velocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravity);
    }

    public void SetDied()
    {
        if (_isDead) return;
        controller.enabled = false;
        //Move_GameManager.Ins.isGamePlaying = false;
        //Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.pDie);
        ani = Craft_Ani_State.Dead;
        _isDead = true;
        ReloadAnimation();
    }
    void LateUpdate()
    {
        if (_isDead) return;
        ReloadAnimation();
    }

    public Craft_Ani_State ani;
    public Craft_Ani_State ani_Apply;
    public void ReloadAnimation()
    {

        //Nếu trạng thái animation thay đổi
        if (ani_Apply == ani)
            return;
        //Bật animation sau mỗi frame
        ani_Apply = ani;
        switch (ani)
        {
            case Craft_Ani_State.Idle:
                anim.SetBool("IsIdle", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                anim.SetBool("IsDance", false);
                break;
            case Craft_Ani_State.Dance:
                anim.SetBool("IsIdle", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                anim.SetBool("IsDance", true);
                break;
            case Craft_Ani_State.Run:
                anim.SetBool("IsIdle", false);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                break;
            case Craft_Ani_State.Attack:
                anim.SetBool("IsAttack", true);
                anim.SetBool("IsUlti", false);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", false);
                break;
            case Craft_Ani_State.Ulti:
                anim.SetBool("IsAttack", true);
                anim.SetBool("IsUlti", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", false);
                break;
            case Craft_Ani_State.Win:
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", true);
                break;
            case Craft_Ani_State.Dead:
                anim.SetBool("IsDead", true);
                break;
            default:
                Debug.LogError("Lỗi animationState Player" + ani.ToString());
                break;
        }
    }

    public void FixedUpdate()
    {
        if (groundCheck != null) isInGround = Physics.CheckSphere(groundCheck.position, 0.4f, groundMask);
        if (isInGround && velocity.y < 0)
        { 
            _isJump = false;
            velocity.y = 0f;
        }

        if (_isDead || !Craft_GameManager.Ins.isGamePlaying || !isCanMove) return;
        //if (_isDead ) return;
        CheckFoward();


        ani = Craft_Ani_State.Run;
        
        controller.SimpleMove(_direction * speed);
        //velocity.z = speed * Time.deltaTime;
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (impact.magnitude > 0.2F) controller.Move(impact * Time.deltaTime);
        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);

    }
    float moveLeft;
    void CheckFoward()
    {
        int layerMask = 1 << 8;
        layerMask = ~layerMask; 
        if (Physics.Raycast(transform.position , transform.TransformDirection(Vector3.forward), out hit, 9f, layerMask))
        {

            if (hit.collider.tag == "JumpBox")
            {
                Debug.Log("see jumpbox");
                
            }
            else if (hit.collider.tag == "DieBox")
            {
                //if (isSee)
                {
                    Debug.Log("see DieBox");
                    if (hit.transform.position.x > 0) moveLeft = -1f;
                    else moveLeft = 1f;
                    tran_Rotate.eulerAngles += Vector3.up * moveLeft;
                    _direction = tran_Rotate.TransformDirection(Vector3.forward);
                }
            }
            else 
            {
                
                if ( transform.position.x <= -2.8f)
                {
                    tran_Rotate.eulerAngles = Vector3.zero;
                    _direction = tran_Rotate.TransformDirection(Vector3.forward);
                }
                if (transform.position.x >= 2.8f)
                {
                    tran_Rotate.eulerAngles = Vector3.zero;
                    _direction = tran_Rotate.TransformDirection(Vector3.forward);
                }
            }

        }
    }

    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        Debug.Log(dir + " " + force);
        impact += dir.normalized * force / 3; 
    }
}
