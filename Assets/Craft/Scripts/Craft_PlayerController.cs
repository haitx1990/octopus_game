﻿using System.Collections;
using UnityEngine;

public class Craft_PlayerController : MonoBehaviour, Craft_IPush, Craft_IHealth
{
    public static Craft_PlayerController Ins;

    public Transform groundCheck;
    public LayerMask groundMask;
    bool isInGround;
    Vector3 velocity;

    private void Awake()
    {
        Ins = this;
    }

    public Transform tran_Rotate;
    public CharacterController controller;
    public Animator anim;
    public GameObject TargetObj;

    public int Hp;
    public float speed;
    public bool isCanMove = false;

    private float gravity = -9.8f;
    private Vector3 moveDirection = Vector3.zero;
    private RaycastHit[] hit;
    private bool _isDead;
    private bool _isJump;
    Vector3 impact = Vector3.zero;
    Vector3 checkPoint;

    void Start()
    {
        checkPoint = transform.position;
    }

    public void FixedUpdate()
    {
        if (groundCheck != null) isInGround = Physics.CheckSphere(groundCheck.position, 0.4f, groundMask);
        if (isInGround && velocity.y < 0)
        {
            TargetObj.SetActive(true);
            _isJump = false;
            velocity.y = -2f;
        }

        //if (_isDead || !Craft_GameManager.Ins.isGamePlaying) return;
        if (_isDead) return;
        if (Hp > 0)
        {
            if (_isJump)
            {
                ani = Craft_Ani_State.Run;
                controller.Move(Vector3.forward * speed * Time.deltaTime);
                tran_Rotate.localRotation = Quaternion.LookRotation(Vector3.zero);
            }
            else
            {
                // Đi thẳng về phía trước mặt
                if (Input.GetMouseButton(0) && isCanMove)
                {
                    ani = Craft_Ani_State.Run;

                    //Tính vận tốc và hướng của MainChar
                    moveDirection = new Vector3(0.0f, 0.0f, 1);
                    moveDirection = tran_Rotate.TransformDirection(moveDirection);//Di chuyển theo hướng quay của nhân vật
                    if (_isJump) moveDirection = Vector3.forward;
                    moveDirection = moveDirection * speed;

                    // Đi thẳng về phía trước mặt
                    if (Input.GetMouseButton(0) && isCanMove)
                    {
                        controller.Move(moveDirection * Time.deltaTime);
                    }
                    //Khi ko trong chế độ tự động tấn công thì Quay mặt theo hướng controll đang điều khiển
                    if (Move_TouchRotateSingle.eulerRotation != Vector3.zero)
                    {
                        tran_Rotate.localRotation = Quaternion.LookRotation(Move_TouchRotateSingle.eulerRotation);
                    }
                }
                else
                {
                    //Trạng thái đứng đợi hoặc tấn công Enemy
                    if (ani == Craft_Ani_State.Run) ani = Craft_Ani_State.Idle;
                }
            }
            //if (transform.position.y != 0.0f) transform.position = new Vector3(transform.position.x, transform.position.x, transform.position.z);
        }
        else
        {
            isCanMove = false;
        }

        velocity.y += gravity * Time.deltaTime * 2;
        controller.Move(velocity * Time.deltaTime);

        if (impact.magnitude > 0.2F) controller.Move(impact * Time.deltaTime);
        // consumes the impact energy each cycle:
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
    }

    [ContextMenu("Jump")]
    public void Jump()
    {
        TargetObj.SetActive(false);
        _isJump = true;
        float distance = 1f;
        var t = distance / speed;
        var h = -gravity * t * t * 0.5f;
        Debug.Log(h);
        velocity.y = Mathf.Sqrt(8 * -2f * gravity);
    }

    public void SetDied()
    {
        //Move_GameManager.Ins.isGamePlaying = false;
        //Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.pDie);
        ani = Craft_Ani_State.Dead;
        _isDead = true;
        ReloadAnimation();
        //StartCoroutine(ie_Die());
        Craft_GameManager.Ins.Game_Dead();
    }

    IEnumerator ie_Die()
    {
        yield return new WaitForSeconds(1f);
        //Move_SoundController.PlaySoundEffect(Move_SoundController.Instance.loseBg);
        //Move_GameManager.Ins.Endgame();
        //yield return new WaitForSeconds(1f);
        //ani = Ani_State.Idle;
        //ReloadAnimation();
        
    }

    void LateUpdate()
    {
        if (_isDead) return;
        ReloadAnimation();
    }

    public Craft_Ani_State ani;
    public Craft_Ani_State ani_Apply;
    public void ReloadAnimation()
    {

        //Nếu trạng thái animation thay đổi
        if (ani_Apply == ani)
            return;
        //Bật animation sau mỗi frame
        ani_Apply = ani;
        switch (ani)
        {
            case Craft_Ani_State.Idle:
                anim.SetBool("IsIdle", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                anim.SetBool("IsDance", false);
                break;
            case Craft_Ani_State.Dance:
                anim.SetBool("IsIdle", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                anim.SetBool("IsDance", true);
                break;
            case Craft_Ani_State.Run:
                anim.SetBool("IsIdle", false);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsAttack", false);
                anim.SetBool("IsWin", false);
                break;
            case Craft_Ani_State.Attack:
                anim.SetBool("IsAttack", true);
                anim.SetBool("IsUlti", false);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", false);
                break;
            case Craft_Ani_State.Ulti:
                anim.SetBool("IsAttack", true);
                anim.SetBool("IsUlti", true);
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", false);
                break;
            case Craft_Ani_State.Win:
                anim.SetBool("IsDead", false);
                anim.SetBool("IsWin", true);
                break;
            case Craft_Ani_State.Dead:
                anim.SetBool("IsDead", true);
                break;
            default:
                Debug.LogError("Lỗi animationState Player" + ani.ToString());
                break;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (_isDead) return;
        if(other.tag == "JumpBox")
        {
            Debug.Log("Call Jump");
            Jump();
        }
        else if (other.tag == "DieBox")
        {
            Debug.Log("Die");
            SetDied();
        }
        else if (other.tag == "Finishs")
        {
            isCanMove = false;
            controller.enabled = false;
            ani = Craft_Ani_State.Win; 
            ReloadAnimation();
            Craft_GameManager.Ins.Game_Finish();
        }
        else if (other.tag == "Respawn")
        {
            checkPoint = other.transform.position;
            other.enabled = false;
        }
    }

    public void AddImpact(Vector3 dir, float force)
    {
        dir.Normalize();
        if (dir.y < 0) dir.y = -dir.y; // reflect down force on the ground
        Debug.Log(dir + " " + force);
        impact += dir.normalized * force / 3;
        Debug.Log("impact: " + impact);
    }

    public void TakeDame()
    {
        SetDied();
    }

    public void Game_Revive()
    {
        gameObject.SetActive(false);
        _isDead = false;
        transform.position = checkPoint;
        gameObject.SetActive(true);
        ani = Craft_Ani_State.Idle;
        ReloadAnimation();

    }
}

public enum Craft_Ani_State
{
    None,
    Idle,
    Walk,
    Run,
    Attack,
    Dance,
    Dead,
    Win,
    OutGame_Dead,
    OutGame_Idle,
    Free_Run,
    Free_Idle,
    Attack_RunToEnemy,
    Attack_IdleStartAutoAttack,
    Attack_IdleDelayAttack,
    Attack_IdleWaitAttack,
    Attack_Attack,
    Attack_IdleAfterAttack,
    Attack_RunToDodge,
    Dead_Dead,
    Ulti,
    Attack_Ulti,
    Attack_Zombie
}