﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craft_AnimStart : MonoBehaviour
{
    public Craft_GameManager gameManager;

    public void Event_Start()
    {
        gameManager.Anim_Start_Complete();
        gameObject.SetActive(false);
    }
}
