﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craft_Camera : MonoBehaviour
{
    public static Craft_Camera Instance;
    public float speed = 4f;
    Vector3 posCur;
    Vector3 rotCur;
    private Vector3 posPlay = new Vector3(0, -6.4f, 5f);
    private Vector3 rotPlay = new Vector3(24f, 0, 0);
    private Vector3 posStart = Vector3.zero;
    private Vector3 rotStart = new Vector3(24f, 0, 0);

    Transform trans;

    bool isComplete = false;
    private void Awake()
    {
        trans = transform;
        Instance = this;
    }
    private void Start()
    {
        Set_Start();
    }
    public void Set_Start()
    {
        isComplete = false;
        posCur = posStart;
        rotCur = rotStart;
        trans.localPosition = posStart;
        trans.localEulerAngles = rotStart;
    }
    public void Set_Play()
    {
        posCur = posPlay;
        rotCur = rotPlay;
        //trans.localPosition = posPlay;
        //trans.localEulerAngles = rotPlay;
        isComplete = true;
    }
    private void LateUpdate()
    {
        if (isComplete)
        {
            if (trans.localPosition != posCur) trans.localPosition = Vector3.Lerp(trans.localPosition, posCur, Time.deltaTime * speed);
            if (trans.localEulerAngles != rotCur) trans.localEulerAngles = Vector3.Lerp(trans.localEulerAngles, rotCur, Time.deltaTime * speed);
        }
    }
}
