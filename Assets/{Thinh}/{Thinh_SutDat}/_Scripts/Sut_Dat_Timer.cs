﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Sut_Dat_Timer : MonoBehaviour
{
    public static Sut_Dat_Timer instance;

    public int playTime; //seconds
    IEnumerator I_Timer;

    public Text txtTimer;
    public Text txtTimer1;
    public Text txtStart;
    private void Awake()
    {
        instance = this;
    }
    
    public IEnumerator I_TimerCountDown(int time, Text txtTimer,bool isTime ,Action onComplete)
    {
        bool a = true;
        while (a)
        {
            playTime = time;
            time -= 1;
            if ((time % 60) < 10 && isTime)
            {
                txtTimer.text = "0" + (time / 60) + ":0" + (time % 60);
            }
            else if((time % 60) >= 10 && isTime)
            {
                txtTimer.text = "0" + (time / 60) + ":" + (time % 60);
            }
            else if (!isTime)
            {
                txtTimer.text = time.ToString();
            }
            if (time == 0)
            {
                a = false;
                onComplete();
                break;
            }
            yield return new WaitForSeconds(1f);
        }

    }
}
