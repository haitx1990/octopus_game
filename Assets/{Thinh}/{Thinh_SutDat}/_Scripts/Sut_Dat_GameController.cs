﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/*
 * Fire = bẫy 
 * Respawn = snowball
 * Wall = bricks;
 */
public class Sut_Dat_GameController : MonoBehaviour
{

    public static Sut_Dat_GameController instance;

    public bool isPlay = false; // biến check chơi game

    
    public float interval;// sau 1 khoảng t.g sẽ spawn object 
    public float interval_SpawnPl;
    public float interval_Increase;

    IEnumerator coroutineTimer; //coroutine trôi ngược thời gian

    public Sut_Dat_MyPlayer myPlayer;

    public GameObject popup_win;
    public GameObject popup_revive;

    bool oneTime = false;

    IEnumerator SpawnObjectDestroy_New;
    IEnumerator SpawnToPlayer;
    IEnumerator IncreaseObjDestroy;
    private void Update()
    {
        if (isPlay&&!oneTime)
        {
            if (Sut_Dat_PlayerController.instance.isDead)
            {
                Debug.Log("lose");
                Lose();
                oneTime = true;
            }
        }
    }
    private void Awake()
    {
        instance = this;

        SpawnObjectDestroy_New = I_SpawnObjectDestroy_New();
        SpawnToPlayer = I_SpawnToPlayer();
        IncreaseObjDestroy = I_IncreaseObjectDestroy(interval_Increase);
        coroutineTimer = Sut_Dat_Timer.instance.I_TimerCountDown(60, Sut_Dat_Timer.instance.txtTimer, true, ()=> {
            if (Sut_Dat_PlayerController.instance.isDead)//khi đồng hồ đếm hết thời gian
            {
                Lose();
            }
            else
            {
                Win();
            }
        });
    }
    #region win/lose
    private void Win()
    {
        //popup win nhảy ra
        StopCoroutine(SpawnObjectDestroy_New);//bắt đầu chơi - SPAWN OBJECT ĐỂ NÉ
        StopCoroutine(SpawnToPlayer);//spawn object thẳng mặt người chơi
        StopCoroutine(coroutineTimer);//- đếm ngược thời gian
        StopCoroutine(IncreaseObjDestroy);// tăng tốc độ và mật độ của object destroy theo thời gian 

        ShowPopup_EndGame(true);
        
    }
    private void Lose()
    {
        //popup revive nhảy ra

        StopCoroutine(SpawnObjectDestroy_New);//bắt đầu chơi - SPAWN OBJECT ĐỂ NÉ
        StopCoroutine(SpawnToPlayer);//spawn object thẳng mặt người chơi
        StopCoroutine(coroutineTimer);//- đếm ngược thời gian
        StopCoroutine(IncreaseObjDestroy);// tăng tốc độ và mật độ của object destroy theo thời gian 

        StartCoroutine(ie_Die());
        
    }
    IEnumerator ie_Die()
    {
        yield return new WaitForSeconds(1f);
        ShowPopup_Revive(Revive);
    }
    void Revive()
    {
        
        
        Sut_Dat_PlayerController.instance.SMF.enabled = true; // bật lại camera follow
        myPlayer.Reborn();
        Sut_Dat_PlayerController.instance.BringPlayerToLive();//bật lại các biến check winlose + control
        oneTime = false;
        isPlay = true;

        Sut_Dat_PlayerController.instance.reloadAnimation();//show lại animation cho nó
        Sut_Dat_PlayerController.instance.backToLastCheckPoint(Sut_Dat_PlayerController.instance.isFall);//đưa nó về vị trí cũ khi nó rơi xuống
        
        //startcoroutine tiếp để chơi
        StartAllCoroutine();
    }


    public void ShowPopup_EndGame(bool isWin = false)
    {
        SceneManager.ins.ShowPopup_EndGame(isWin);
    }
    public void ShowPopup_Revive(System.Action c)
    {
        SceneManager.ins.ShowPopup_Revive(c);
    }
    #endregion
    private void Start()
    {
        StartCoroutine(Sut_Dat_Timer.instance.I_TimerCountDown(3,Sut_Dat_Timer.instance.txtTimer1,false,()=> {
            //sau khi đồng hồ chạy xuống 0 sẽ thực hiện khối lệnh sau
            //vào chơi : random 1 vị trí bất kỳ sau 10s 
            Sut_Dat_Timer.instance.txtTimer1.enabled = false;
            Sut_Dat_Timer.instance.txtStart.enabled = true;
            Timer.Schedule(this, 0.5f, () => {
                Sut_Dat_Timer.instance.txtStart.enabled = false;
                isPlay = true;
                Sut_Dat_Timer.instance.playTime = 60;
                //START ALL COROUTINE
                StartAllCoroutine();

                myPlayer.isCanControl = true; // bật mode cho điều khiển người chơi
            });
        }));
        
    }

    //STARTCOROUTINE
    
    void StartAllCoroutine()
    {
        coroutineTimer = Sut_Dat_Timer.instance.I_TimerCountDown(Sut_Dat_Timer.instance.playTime, Sut_Dat_Timer.instance.txtTimer, true, () => {
            //khi đồng hồ đếm hết thời gian
            if (Sut_Dat_PlayerController.instance.isDead)
            {
                Lose();
                Debug.Log("TimeOut");
            }
            else
            {
                Win();
            }
        });
        
        StartCoroutine(SpawnObjectDestroy_New);//bắt đầu chơi - SPAWN OBJECT ĐỂ NÉ
        StartCoroutine(SpawnToPlayer);//spawn object thẳng mặt người chơi
        StartCoroutine(coroutineTimer);//- đếm ngược thời gian
        StartCoroutine(IncreaseObjDestroy);// tăng tốc độ và mật độ của object destroy theo thời gian 
    }
    #region spawn object destroy ngẫu nhiên
    public GameObject SnowBall;
    public float timeMove;
    public GameObject shadow;
    IEnumerator I_SpawnObjectDestroy_New()
    {
        bool a = true;
        
        while (a)
        {
            SpawnSnowBall();
            float _interval = interval;
            yield return new WaitForSeconds(_interval);
        }
    }
    public void SpawnSnowBall()
    {
        Vector3 posInit = new Vector3(Random.Range(-14.3f, 14.3f), 23f, Random.Range(-25.4f, 15.1f));
        GameObject obj = Instantiate(SnowBall, posInit, Quaternion.identity, null);
        GameObject shad = Instantiate(shadow, posInit + Vector3.down * 22.9f, Quaternion.identity, null);
        
        
        shad.transform.localScale = Vector3.zero;
        
        shad.transform.DOScale(new Vector3(13.5f, 0, 13.5f), timeMove).SetEase(Ease.Linear);
        shad.transform.GetChild(0).GetComponent<SpriteRenderer>().DOFade(1,timeMove).SetEase(Ease.Linear);
        obj.transform.DOMoveY(0,timeMove).SetEase(Ease.InBack).OnComplete(()=> {
            Destroy(shad);
        });
        //init 1 cái sprite 2D bóng đổ ra đúng tại bị trí posinit đó nhưng y = 1
        //kích thước của cái sprite bóng đổ đúng bằng kích thước của snowball
        //kích thước sẽ scale từ 0-kích thước của snowball trong vòng đúng timeMove


    }
    #endregion
    public void UI_Button()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(5);
    }

    //cách 1 khoảng 1 t.g sẽ spawn 1 object destroy xuống thẳng người chơi
    //bắt người chơi phải né
    //code nằm ở đây
    #region spawn object destroy vào thẳng player
    IEnumerator I_SpawnToPlayer()
    {
        bool a = true;
        
        while (a)
        {
            yield return new WaitForSeconds(2f);
            SpawnSnowBallToPlayer();
            float interval = interval_SpawnPl;
            yield return new WaitForSeconds(interval-2f);
        }
    }

    public void SpawnSnowBallToPlayer()
    {
        Vector3 posInit = myPlayer.transform.position + Vector3.up * 23;
        GameObject obj = Instantiate(SnowBall, posInit, Quaternion.identity, null);
        GameObject shad = Instantiate(shadow, posInit + Vector3.down * 22.9f, Quaternion.identity, null);


        shad.transform.localScale = Vector3.zero;

        shad.transform.DOScale(new Vector3(13.5f, 0, 13.5f), timeMove).SetEase(Ease.Linear);
        shad.transform.GetChild(0).GetComponent<SpriteRenderer>().DOFade(1, timeMove).SetEase(Ease.Linear);
        obj.transform.DOMoveY(0, timeMove).SetEase(Ease.InBack).OnComplete(() => {
            Destroy(shad);
        });
    }
    #endregion
    #region tăng dần tốc độ và mật độ của Object destroy
    IEnumerator I_IncreaseObjectDestroy(float interval)
    {
        bool a = true;
        while (a)
        {
            yield return new WaitForSeconds(interval);
            IncreaseObjectDestroy();
        }
    }

    public void IncreaseObjectDestroy()
    {
        //mật độ
        interval = interval * 90 /100; //khoảng t.g spawn object
        interval_SpawnPl = interval_SpawnPl * 90 /100; //khoảng t.g spawn vào player
        timeMove = timeMove * 90 / 100; //thời gian object phi xuống mặt đất
        Debug.Log(interval);
        Debug.Log(interval_SpawnPl);
        Debug.Log(timeMove);
    }
    #endregion
}
