﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sut_Dat_SmoothCameraFollow : MonoBehaviour
{
    public Vector3 offset;

    GameObject mainCamera;
    public GameObject player;
    private void Awake()
    {
        mainCamera = gameObject;
    }
    private void LateUpdate()
    {
        mainCamera.transform.position = player.transform.position - offset;
        mainCamera.transform.LookAt(player.transform.position + Vector3.up);
    }
}
