﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sut_Dat_ObjectDestroy : MonoBehaviour
{
    public Rigidbody rb;
    float power;

    private void Awake()
    {
        power = 1000f;
    }
    public void Fall()
    {
        rb.isKinematic = false;
        rb.AddForce(Vector3.down * power);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            Destroy(this.gameObject);
            collision.gameObject.GetComponent<Sut_Dat_GroundControl>().Fall();
        }
    }
}
