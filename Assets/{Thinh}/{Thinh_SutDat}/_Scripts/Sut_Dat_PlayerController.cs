﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Sut_Dat_PlayerController : MonoBehaviour
{
    public static Sut_Dat_PlayerController instance;
    public float spdMove;

    public VariableJoystick joystick;

    public bool isDead;
    public bool isControl;
    public bool isFall;

    public Sut_Dat_MyPlayer myPlayer;
    public Sut_Dat_SmoothCameraFollow SMF;

    Animator animator;
    Vector3 checkPoint;
    private void Awake()
    {
        instance = this;
        isControl = false;
        animator = GetComponent<Sut_Dat_MyPlayer>().animator;
    }

    private void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x,-14.3f,14.3f)
            ,transform.position.y
            ,Mathf.Clamp(transform.position.z,-25.4f,15.1f));


        
    }

    public void Dead()
    {
        Debug.Log("DEAD!!!");
        isDead = true;
        isControl = false;
    }

    public void Fall(Collision collision)
    {
        collision.gameObject.GetComponent<MeshCollider>().enabled = false;
        Debug.Log("Fall");

        isFall = true;
        //myPlayer.isDie = true;//animation
        AnimDie();
        isDead = true;
        isControl = false;
        
        gameObject.transform.DOMove(collision.gameObject.transform.position + Vector3.down *2, 0.2f).SetEase(Ease.Linear).OnComplete(()=> {
            //gameObject.GetComponent<Rigidbody>().AddForce(Vector3.down * 500 );
            
        });

    }
    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Fire"))// xử lý va chạm rơi
        {
            Fall(collision);
            SMF.enabled = false;
        }

        
        if (collision.gameObject.CompareTag("Wall"))//save checkPoint
        {
            checkPoint = collision.gameObject.transform.position;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("WallEndMap")) //TRIGGER
        {
            Dead();
        }
        if (other.CompareTag("EditorOnly")) //object destroy 
            //bị rơi vào PL
        {
            SMF.enabled = false;
            Dead();
            AnimDie();
            //myPlayer.isDie = true;
        }
    }

    public void reloadAnimation()
    {
        animator.SetBool(Ani_State.Idle.ToString(), true);
        animator.SetBool(Ani_State.Run.ToString(), false);
        animator.SetBool(Ani_State.Dead.ToString(), false);
        animator.SetBool(Ani_State.Win.ToString(), false);
        animator.SetBool(Ani_State.Dance.ToString(), false);
        animator.SetBool(Ani_State.Attack.ToString(), false);
        animator.SetBool(Ani_State.Ulti.ToString(), false);
        animator.SetBool(Ani_State.Walk_Up.ToString(), false);
        animator.SetBool(Ani_State.Idle_InGame.ToString(), false);
    }
    public void AnimDie()
    {
        animator.SetBool(Ani_State.Idle.ToString(), false);
        animator.SetBool(Ani_State.Run.ToString(), false);
        animator.SetBool(Ani_State.Dead.ToString(), true);
        animator.SetBool(Ani_State.Win.ToString(), false);
        animator.SetBool(Ani_State.Dance.ToString(), false);
        animator.SetBool(Ani_State.Attack.ToString(), false);
        animator.SetBool(Ani_State.Ulti.ToString(), false);
        animator.SetBool(Ani_State.Walk_Up.ToString(), false);
        animator.SetBool(Ani_State.Idle_InGame.ToString(), false);
    }

    public void backToLastCheckPoint(bool isFall)
    {
        if (isFall)
        {
            transform.position = checkPoint;
        }
        else
        {
            // tìm cái gần nhất với player để revive
            GameObject[] objs = GameObject.FindGameObjectsWithTag("Wall");
            float distance = Vector3.Distance(transform.position, objs[0].transform.position);
            GameObject checkPointObj = objs[0] ;
            foreach (GameObject obj in objs )
            {
                float temp_distance = Vector3.Distance(transform.position, obj.transform.position);
                if (temp_distance <= distance)
                {
                    distance = temp_distance;
                    checkPointObj = obj;
                }
            }
            //set transform
            transform.position = checkPointObj.transform.position;
        }
        
    }

    public void BringPlayerToLive()
    {
        isFall = false;
        myPlayer.isDie = false;
        isDead = false;
        myPlayer.isCanControl = true;
    }
    
}
