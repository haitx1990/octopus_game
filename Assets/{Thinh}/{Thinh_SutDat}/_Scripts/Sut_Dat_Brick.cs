﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sut_Dat_Brick : MonoBehaviour
{
    public GameObject graphicBrick;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EditorOnly"))
        {
            tag = "Fire";
            graphicBrick.SetActive(false);
        }
    }
}
