﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loading2_PathManager : MonoBehaviour
{
    public GameObject minionPrefabs;
    [Header("---path 1")]
    public GameObject path1;
    public int numberOfPlayer_path1;

    private void Awake()
    {
        int temp1 = numberOfPlayer_path1;
        while (temp1 > 0)
        {
            //tính tổng đoạn đường xem dài bao nhiêu m? = tổng khoảng cách các đoạn đường
            //lấy tổng đó chia đều cho numberofplayer => ta có được khoảng cách đều giữa mỗi player
            //path1 ta có 5 đoạn đường cho path1 => 0-1 1-2 2-3 3-4 4-5

            temp1 -= 1;
        }
    }
}
