﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class L2_AnimationManager : MonoBehaviour
{
    public Animator animator;
    public bool isMinion;

    public PlayerSkinManager PlayerSkinManager;

    private void Awake()
    {
        animator.SetBool(Ani_State.Idle.ToString(), !isMinion);
        animator.SetBool(Ani_State.Run.ToString(), false);
        animator.SetBool(Ani_State.Dead.ToString(), false);
        animator.SetBool(Ani_State.Win.ToString(), false);
        animator.SetBool(Ani_State.Dance.ToString(), false);
        animator.SetBool(Ani_State.Attack.ToString(), false);
        animator.SetBool(Ani_State.Ulti.ToString(), false);
        animator.SetBool(Ani_State.Walk_Up.ToString(), isMinion);
        animator.SetBool(Ani_State.Idle_InGame.ToString(), false);

        PlayerSkinManager.isUser = false;
    }
    

    
}
