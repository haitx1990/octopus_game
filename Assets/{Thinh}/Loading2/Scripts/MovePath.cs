﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum e_path
{
    path1,
    path2,
    path3,
    path4,
    pathBox
}

public class MovePath : MonoBehaviour
{
    [Header("***SETTINGS***")]
    public e_path id;

    public Vector3[] path;
    public float spdMove;

    [Header("***GLOBAL***")]

    [Header("***Path1***")]
    public Vector3 start_pos1;
    public Vector3[] fullPath1;

    [Header("***Path2***")]
    public Vector3 start_pos2;
    public Vector3[] fullPath2;

    [Header("***Path3***")]
    public Vector3 start_pos3;
    public Vector3[] fullPath3;

    [Header("***Path4***")]
    public Vector3 start_pos4;
    public Vector3[] fullPath4;

    [Header("***Path_Box***")]
    public Vector3 start_pos_box;
    public Vector3[] fullPathBox;

    public float lookat;
    private void Awake()
    {
        spdMove = 1.5f;
        lookat = 0.035f;
        Move();
        
    }

    private void Update()
    {
        if (id == e_path.pathBox)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,-10f);
            return;
        }
            
        transform.localRotation = Quaternion.Euler(0,transform.localEulerAngles.y,0);
    }

    public void Move()
    {
        transform.DOLocalPath(path,DistancePath()/spdMove, PathType.Linear).SetEase(Ease.Linear).OnComplete(()=> {
            switch (id)
            {
                case e_path.path1:
                    transform.position = start_pos1;
                    transform.DOPath(fullPath1,DistanceFullPath(start_pos1,fullPath1)/spdMove, PathType.Linear).SetEase(Ease.Linear).SetLoops(-1).SetLookAt(lookat);
                    break;
                case e_path.path2:
                    transform.position = start_pos2;
                    transform.DOPath(fullPath2, DistanceFullPath(start_pos2, fullPath2) / spdMove, PathType.Linear).SetEase(Ease.Linear).SetLoops(-1).SetLookAt(lookat);
                    break;
                case e_path.path3:
                    transform.position = start_pos3;
                    transform.DOPath(fullPath3, DistanceFullPath(start_pos3, fullPath3) / spdMove, PathType.Linear).SetEase(Ease.Linear).SetLoops(-1).SetLookAt(lookat);
                    break;
                case e_path.path4:
                    transform.position = start_pos4;
                    transform.DOPath(fullPath4, DistanceFullPath(start_pos4, fullPath4) / spdMove, PathType.Linear).SetEase(Ease.Linear).SetLoops(-1).SetLookAt(lookat);
                    break;
                case e_path.pathBox:
                    transform.position = start_pos_box;
                    transform.DOPath(fullPathBox, DistanceFullPath(start_pos_box, fullPathBox) / spdMove, PathType.Linear).SetEase(Ease.Linear).SetLoops(-1).SetLookAt(lookat);
                    break;
            }
        }).SetLookAt(lookat);
    }

    public float DistancePath()
    {
        float distance = Vector3.Distance(transform.position, path[0]);
        for (int i = 0; i < path.Length -1; i++)
        {
            float tempDistance = Vector3.Distance(path[i],path[i+1]);
            distance += tempDistance;
        }
        return distance;
    }

    public float DistanceFullPath(Vector3 startPos, Vector3[] fullPath)
    {
        float distance = Vector3.Distance(startPos,fullPath[0]);
        for (int i =0; i < fullPath.Length -1; i++)
        {
            float tempDistance = Vector3.Distance(fullPath[i], fullPath[i + 1]);
            distance += tempDistance;
        }
        return distance;
    }
}
