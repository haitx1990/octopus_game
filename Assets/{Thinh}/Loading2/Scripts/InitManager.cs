﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitManager : MonoBehaviour
{
    //public List<Transform> tranfs;
    public List<Vector3> points;
    public List<Vector3> directions;
    public List<float> distances;
    float allDistance = 0;

    public GameObject prefab;
    public int soLuong_prefab;

    private void Awake()
    {
        Init();
    }
    void Init()
    {
        //new
        //setup
        //foreach (Transform tf in tranfs)
        //{
        //    points.Add(tf.position);
        //}//có 1 list points
        int y = 1;

        foreach (Vector3 vt in points)
        {
            if (y > points.Count - 1)
                break;
            distances.Add(Vector3.Distance(vt, points[y]));

            directions.Add((points[y] - vt).normalized);
            y += 1;
        } //có tổng độ dài của path và các directions

        foreach (float dis in distances)
        {
            allDistance += dis;
        }

        //bh mình muốn có x số lượng trên đường path này thì mình sẽ phải làm gì nhỉ?
        //cứ chia ra trước đi đã rồi tính tiếp
        float d = allDistance / soLuong_prefab;
        int p = 0;
        Vector3 lastObjPos = points[0];
        //bh mình đã có đoạn chia đều rồi bh sao?
        for (int i = 0; i < soLuong_prefab; i++)
        {
            GameObject obj = Instantiate(prefab, lastObjPos + directions[p] * d, Quaternion.identity, null);
            //nếu obj có vị trí đến cái điểm gần nhất dài hơn thì sẽ next directions
            float temp = Vector3.Distance(obj.transform.position, points[p]);
            Debug.Log(temp);
            if (temp > distances[p])
            {
                obj.transform.position = points[p + 1];
                float temp1 = d - Vector3.Distance(points[p + 1], lastObjPos);
                obj.transform.position += directions[p + 1] * temp1;
                p += 1;
            }
            lastObjPos = obj.transform.position;
        }
    }
}
