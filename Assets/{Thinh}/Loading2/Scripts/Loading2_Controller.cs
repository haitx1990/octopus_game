﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

/*
 * -sử dụng dotween để đi qua các điểm trên 1 đường
 */
public class Loading2_Controller : MonoBehaviour
{
    public GameObject minion;
    public GameObject guard;

    public Transform parent_Path1;
    public Transform parent_Path2;
    public Transform parent_Path3;
    public Transform parent_Path4;

    [Header("***PATH_1***")]
    public Vector3 start_Pos_Path1;
    public Vector3[] list;

    [Header("***PATH_2***")]
    public Vector3 start_Pos_Path2;
    public Vector3[] path2;

    [Header("***PATH_3***")]
    public Vector3 start_Pos_Path3;
    public Vector3[] path3;

    [Header("***PATH_4***")]
    public Vector3 start_Pos_Path4;
    public Vector3[] path4;

    [Header("***Position cho từng object***")]
    public List<Vector3> pos_Path_1;
    public List<Vector3> pos_Path_2;
    public List<Vector3> pos_Path_3;
    public List<Vector3> pos_Path_4;
    

    private void Start()
    {
        //StartCoroutine(I_setTime());

        //StartCoroutine(I_Spawn(start_Pos_Path1,list,9f,0.5f,parent_Path1)); // 1
        //StartCoroutine(I_Spawn(start_Pos_Path2,path2,7f,0.5f,parent_Path2)); //2
        //StartCoroutine(I_Spawn(start_Pos_Path3, path3, 15f, 0.5f,parent_Path3)); //3 - 2.93f length
        //StartCoroutine(I_Spawn(start_Pos_Path4, path4, 7f, 0.5f,parent_Path4)); //4 -1.22 length

        
        
    }

    IEnumerator I_setTime()
    {
        Time.timeScale = 100f;
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 1;
    }
    IEnumerator I_Spawn(Vector3 startPos, Vector3[] path,float duration,float interval,Transform parent)
    {
        bool a = true;
        while (a)
        {
            Spawn(startPos,path,duration,parent);
            yield return new WaitForSeconds(interval);
        }
    }

    private void Spawn(Vector3 startPos,Vector3[] path,float duration,Transform parent)
    {
        GameObject obj = Instantiate(minion, startPos, Quaternion.identity, parent);
        obj.transform.DOPath(path, duration, PathType.Linear).OnComplete(() => {
            Destroy(obj);
        });
    }

    private void SpawnInPos (Vector3 startPos, Vector3[] path, float duration, Transform parent,Vector3 posInit)
    {
        GameObject obj = Instantiate(minion, startPos, Quaternion.identity, parent);
        
        obj.transform.DOPath(path, duration, PathType.Linear).SetLoops(-1).OnComplete(() => {
            
        });
        obj.transform.localPosition = posInit;
    }
}
